﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ricimi;
public class SignUpPopUP : MonoBehaviour {
	public InputField input_userName;
	public InputField input_Email;
	public InputField input_password;
	public Dropdown input_country;
	public GameObject prefPopupSignError;
	public GameObject txt_warning;
	GameObject popupSignError;
	public Transform mCanvas;
	public static SignUpPopUP instance;

	public static SignUpPopUP Instance() {
		if (!instance) {
			instance = FindObjectOfType (typeof(SignUpPopUP)) as SignUpPopUP;
		}
		return instance;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
//		if (input_Email.isFocused == true)
//			txt_warning.SetActive (true);
//		else
//			txt_warning.SetActive (false);
	}

	public void ActivateWarningText() {
		if (input_Email.text != "")
			txt_warning.SetActive (true);
		else 
			txt_warning.SetActive (false);
	}

	bool CheckRegisterData() {
		bool bCheckValid = true;
		if (input_userName.text == "") {
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (0);
			//popupSignError.SetActive (true);
			bCheckValid = false;
		} else if (input_Email.text == "") {
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (2);
			//popupSignError.SetActive (true);
			bCheckValid = false;
		} else if (input_password.text == "") {
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (4);
			//popupSignError.SetActive (true);
			bCheckValid = false;
		} else if (input_password.text.Length < 5) {
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (5);
			//popupSignError.SetActive (true);
			bCheckValid = false;
		}
		return bCheckValid;
	}

	public void CheckNSendRequest() {
		if (!CheckRegisterData ())
			return;
		string username = input_userName.text;
		string email = input_Email.text;
		string password = input_password.text;
		int countryCode = input_country.value;
		GameFlags.userName = username;
		GameFlags.email = email;
		GameFlags.password = password;
		ApiHandler.Instance ().PostRegisterData (username, email, password, countryCode);
	}

	public void ShowErrorPopup(int nError) {
		switch (nError) {
		case 1:
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (6);
			//popupSignError.SetActive (true);
			break;
		case 2: //user exsit
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (1);
			//popupSignError.SetActive (true);
			break;
		case 3://email exist
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (3);
			//popupSignError.SetActive (true);
			break;
		case 4: // email format
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (7);
			//popupSignError.SetActive (true);
			break;
		case 5:
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (5);
			//popupSignError.SetActive (true);
			break;
		case 6: 
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (9);
			break;
		}
	}


}
