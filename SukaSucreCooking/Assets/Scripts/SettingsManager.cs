﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingsManager : MonoBehaviour {
	public GameObject[] imgDisables;
	public GameObject prefPopupAlert;
	// Use this for initialization
	void Start () {
		CheckSettings ();
	}
	
	void CheckSettings() {
		if (!PlayerPrefs.HasKey ("SoundOff")) {
			imgDisables [0].SetActive (false);
		} else {
			if (PlayerPrefs.GetInt("SoundOff", 0) == 1)
				imgDisables [0].SetActive (true);
			else
				imgDisables [0].SetActive (false);
		}

		if (!PlayerPrefs.HasKey ("MusicOff")) {
			imgDisables [1].SetActive (false);
		} else {
			if (PlayerPrefs.GetInt ("MusicOff", 0) == 1)
				imgDisables [1].SetActive (true);
			else 
				imgDisables [1].SetActive (false);
		}

		if (!PlayerPrefs.HasKey ("Notification")) {
			imgDisables [2].SetActive (false);
		} else {
			if (PlayerPrefs.GetInt ("Notification", 0) == 1)
				imgDisables [2].SetActive (true);
			else 
				imgDisables [2].SetActive (false);
		}
	}

	public void OnClickSoundButton () {
		bool bDisabled = false;
//		if (!PlayerPrefs.HasKey ("SoundOff")) {
//			PlayerPrefs.SetInt ("SoundOff", 1);
//			bDisabled = true;
//		} else {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 1) {
			PlayerPrefs.SetInt ("SoundOff", 0);
			imgDisables [0].SetActive (false);
		} else {
			PlayerPrefs.SetInt ("SoundOff", 1);
			imgDisables [0].SetActive (true);
		}

		//}
	}

	public void OnClickMusicButton () {

		if (PlayerPrefs.GetInt ("MusicOff") == 1) {
			PlayerPrefs.SetInt ("MusicOff", 0);
			imgDisables [1].SetActive (false);
		} else {
			PlayerPrefs.SetInt ("MusicOff", 1);
			imgDisables [1].SetActive (true);
		}
		BGSoundManager.Instance ().CheckMuteSounds ();	
	}

	public void OnClickNotificationButton () {

		if (PlayerPrefs.GetInt ("Notification") == 1) {
			PlayerPrefs.SetInt ("Notification", 0);
			imgDisables [2].SetActive (false);
		} else {
			PlayerPrefs.SetInt ("Notification", 1);
			imgDisables [2].SetActive (true);
		}


	}

	public void OnClickPrivacyButton() {
		Application.OpenURL (Config.URL_PRIVACY);
	}

	public void OnClickRestorePurchases() {
		CoinPurchaseScript.Instance ().RestorePurchases ();
		GameObject popupAlert = Instantiate (prefPopupAlert) as GameObject;
		popupAlert.transform.parent = GameObject.Find ("Canvas").transform;
		popupAlert.transform.localScale = 0.8f * Vector3.one;
		popupAlert.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -238f);
		popupAlert.GetComponent<PopupSignError> ().ShowPopUp (10);
	}
}
