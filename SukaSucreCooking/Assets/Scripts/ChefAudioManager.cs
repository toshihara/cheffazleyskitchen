﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChefAudioManager : MonoBehaviour {
	public AudioSource[] chefAudioChipList;
	public static ChefAudioManager chefAudioManager;

	public static ChefAudioManager Instance() {
		if (!chefAudioManager) {
			chefAudioManager = FindObjectOfType (typeof (ChefAudioManager)) as ChefAudioManager;
		}
		return chefAudioManager;
	}

	public void PickupRawFood() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PickupReadyFood() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PickupBurntFood() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PickupReadyDrink() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PickupReadyIce() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PutFood2Table() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [0].Play ();
	}

	public void PutRaw2Oven() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [1].Play ();
	}

	public void PutRaw2Stove() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [2].Play ();
	}

	public void Throw2DustBin() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [3].Play ();
	}

	public void PressDrinkMachine() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			chefAudioChipList [4].Play ();
	}
		
}
