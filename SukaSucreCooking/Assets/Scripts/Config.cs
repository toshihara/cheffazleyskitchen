﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config {

	public static readonly string API_VERSION_URL = "";
	public static readonly string API_URL = "http://kometdigital.com/games/chef/backend/user/index.php";
	public static readonly string API_RANK_URL = "http://kometdigital.com/games/chef/backend/ranking/index.php";
	public static readonly string API_EVENT_URL = "";
	public static readonly string IMG_URL = "http://kometdigital.com/games/chef/user_profiles/";
	public static readonly string FLAG_URL = "http://kometdigital.com/games/chef/flags/";

	public static readonly string URL_GOOGLE_STORE = "";
	public static readonly string URL_ITUNE_STORE = "";
	public static readonly string URL_RULE = "http://kometdigital.com/games/chef/rules.php";
	public static readonly string URL_WINNER = "http://kometdigital.com/games/chef/winners.php";
	public static readonly string URL_SHARE_IMG = "http://kometdigital.com/games/chef/shared.png";
	public static readonly string URL_PRIVACY = "http://kometdigital.com/games/chef/privacy_policy.html";
													
	public static string version = "1.0.0";

	public static int Oven1stFreeStage = 1;
	public static int Oven2ndFreeStage = 10;
	public static int Stove1stFreeStage = 5;
	public static int Stove2ndFreeStage = 15;
	public static int Warming1stFreeStage = 9;
	public static int Warming2ndFreeStage = 19;

}

public struct UserRankInfo {
	public int user_rank;
	public string user_id;
	public string user_name;
	public long chips;
	public UserRankInfo(int rank, string id, string name, long chip) {
		this.user_rank = rank;
		this.user_name = name;
		this.user_id = id;
		this.chips = chip;
	}
}