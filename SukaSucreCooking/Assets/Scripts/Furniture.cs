﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;
public class Furniture : MonoBehaviour {
	public Image img_Event;
	public Color[] color_list;
	public Transform target_pos;
	public TargetMover targetMover;
	string currentFood = "";
	//public bool bCookDone = false;
	//public SpriteRenderer img_progress;
//	public Sprite[] sprite_progress_list;
	public Image circle_progress;
	public GameObject canvasCookProgress;
	public Image circle_burn;
	public GameObject canvasBurnProgress;
	float fTimeTick = 0;
	public float fCookTime = 3f;
	public float fBurnTime = 8f;
	public UtensilType utensilType;
	public int utensilIndex;
	public UtensilCookingStatus utensilStatus = UtensilCookingStatus.Empty;
	public Animator aniCook;
	public GameObject drinkMeter;
	public SpriteRenderer sprite_cook;
	bool bTutorial = false;
	// Use this for initialization
	void Start () {
		EmptyUtensil ();
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameFlags.bPauseGame)
			CookFood ();
	}

	public void OnMouseUp() {

	}

	public void OnMouseDown() {
		if (!GameFlags.bTutorial) {
			targetMover.PushTargetPosition2List (target_pos.position);
			CharacterAniController.Instance ().destinationFurniture = this;
			CharacterAniController.Instance ().destinationFood = null;
			CharacterAniController.Instance ().destinationTable = null;
			CharacterAniController.Instance ().bReachDestination = false;
			CharacterAniController.Instance ().targetCategories.Add ("Furniture");
			CharacterAniController.Instance ().targetFurnitureList.Add (this);
		} else {
			if (utensilType == UtensilType.Oven && utensilIndex == 0 && 
				(TutorialManager.Instance().nTutorialIndex == 3 || TutorialManager.Instance().nTutorialIndex == 4)) {
				targetMover.PushTargetPosition2List (target_pos.position);
				CharacterAniController.Instance ().destinationFurniture = this;
				CharacterAniController.Instance ().destinationFood = null;
				CharacterAniController.Instance ().destinationTable = null;
				CharacterAniController.Instance ().bReachDestination = false;
				CharacterAniController.Instance ().targetCategories.Add ("Furniture");
				CharacterAniController.Instance ().targetFurnitureList.Add (this);
				TutorialManager.Instance ().HideArrow ();
			}
		}
	}

	public void OnMouseExit() {

	}

	bool SetCookingStatus(string newCookName) {
		currentFood = newCookName;
		if (canvasCookProgress != null)
			canvasCookProgress.SetActive (true);
		if (canvasBurnProgress != null)
			canvasBurnProgress.SetActive (false);
		
		utensilStatus = UtensilCookingStatus.Cooking;
		aniCook.SetTrigger ("Cooking");
		aniCook.ResetTrigger ("CookEmpty");
		this.fTimeTick = 0;
		return true;
	}

	public bool ReceiveFoodMaterials(List<FoodMaterial> foodMatList) {
		switch (utensilType) {
		case UtensilType.Oven:
			if (foodMatList.Count > 0) {
				if (foodMatList [0] == FoodMaterial.Macaron) {
					if (foodMatList.Count == 1) {
						 return SetCookingStatus("Plain Macaron");
					} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Chocolate) {
						return SetCookingStatus("Chocolate Macaron");
					} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Chocolate && foodMatList [2] == FoodMaterial.Caramel) {
						 return SetCookingStatus("Chocolate Caramel Macaron");
					}
				} else if (foodMatList [0] == FoodMaterial.Cake) {
					if (foodMatList.Count == 1) {
						return SetCookingStatus("Plain Cake");
					} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Chocolate) {
						return SetCookingStatus("Chocolate Cake");
					} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Chocolate && foodMatList [2] == FoodMaterial.Caramel) {
						return SetCookingStatus("Chocolate Caramel Cake");
					}
				} else if (foodMatList [0] == FoodMaterial.Rice) {
					if (foodMatList.Count == 1) {
						return SetCookingStatus("French Butter Rice");
					} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Meat) {
						return SetCookingStatus("French Butter Rice With Rendang & Vegetable Pickles");
					} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Meat && foodMatList [2] == FoodMaterial.Turmeric) {
						return SetCookingStatus("French Butter Rice With Daging Salai Lemak Cili Api");
					}
				} else if (foodMatList [0] == FoodMaterial.PizzaCrust) {
					if (foodMatList.Count == 1) {
						return SetCookingStatus("Ratatouille Pizza");
					} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Meat) {
						return SetCookingStatus("Rendang Pizza");
					} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Meat && foodMatList [2] == FoodMaterial.Tomato) {
						return SetCookingStatus("Heavy Duty Meat Pizza");
					}
				} else if (foodMatList [0] == FoodMaterial.Eclairs) {
					if (foodMatList.Count == 1) {
						return SetCookingStatus("Toasted Eclair");
					} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Meat) {
						return SetCookingStatus("Toasted Eclair Pulled Beef Rendang");
					} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Meat && foodMatList [2] == FoodMaterial.Turmeric) {
						return SetCookingStatus("Toasted Eclair Daging Salai");
					}
				}
			}
			break;
		case UtensilType.Stove:
			if (foodMatList [0] == FoodMaterial.Churros) {
				if (foodMatList.Count == 1) {
					return SetCookingStatus("Churros Plain");
				} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Chocolate) {
					return SetCookingStatus("Churros Chocolate");
				} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Chocolate && foodMatList [2] == FoodMaterial.Caramel) {
					return SetCookingStatus("Churros Chocolate Caramel");
				}
			} else if (foodMatList [0] == FoodMaterial.Spaghetti) {
				if (foodMatList.Count == 1) {
					return SetCookingStatus("Pasta Piyash");
				} else if (foodMatList.Count == 2 && foodMatList [1] == FoodMaterial.Tomato) {
					return SetCookingStatus("Pasta Payish");
				} else if (foodMatList.Count == 3 && foodMatList [1] == FoodMaterial.Tomato && foodMatList [2] == FoodMaterial.Turmeric) {
					return SetCookingStatus("Pasta Daging Salai Lemak Cili Api");
				}
			}
			break;
		case UtensilType.CoffeeMachine:
			if (foodMatList.Count == 0) {
				if (utensilIndex == 0) {
					return SetCookingStatus ("Americano");
				} else if (utensilIndex == 1) {
					return SetCookingStatus ("Caramel Latte");
				} else if (utensilIndex == 2) {
					return SetCookingStatus ("Hot Chocolate");
				}
			}
			break;
		case UtensilType.ColdMachine:
			if (foodMatList.Count == 0) {
				if (utensilIndex == 0) {
					return SetCookingStatus ("Frozen Grapes Mint Soda no_ice");
				} else if (utensilIndex == 1) {
					return SetCookingStatus ("Soda Bandung no_ice");
				} else if (utensilIndex == 2) {
					return SetCookingStatus ("Sucre Tea no_ice");
				}
			}
			break;
		}
		return false;
	}



	void CookFood() {
		if (utensilStatus == UtensilCookingStatus.Cooking && currentFood != "") {
			if (fTimeTick < fCookTime) {
				fTimeTick += Time.deltaTime;
				int nSpriteIndex = (int)(fTimeTick * 10 / 3);
				if (circle_progress != null)
					circle_progress.fillAmount = fTimeTick / fCookTime;
				if (drinkMeter != null) {
					Vector3 currentScale = drinkMeter.transform.localScale;
					drinkMeter.transform.localScale = new Vector3 (currentScale.x, 0.9f - 0.5f * fTimeTick / fCookTime, currentScale.z);
				} 
				if (GameFlags.bTutorial) {
					if (fTimeTick > fCookTime * 0.7f && !bTutorial) {
						if (utensilType == UtensilType.Oven && utensilIndex == 0 && TutorialManager.Instance ().nTutorialIndex == 3) {
							TutorialManager.Instance ().ShowTutorial (3);
							bTutorial = true;
						}
					}
				}

			} else {
				
				if (circle_progress != null)
					circle_progress.fillAmount = 1f;
				utensilStatus = UtensilCookingStatus.CookDone;
				if (utensilType != UtensilType.Oven && utensilType != UtensilType.Stove) {
					aniCook.SetTrigger ("CookDone");
					aniCook.ResetTrigger ("CookEmpty");
					SoundManager.Instance ().PlaySound (5);
				}
			}

		}

		if (utensilType == UtensilType.Oven || utensilType == UtensilType.Stove) {
			if (utensilStatus == UtensilCookingStatus.CookDone && currentFood != "") {
				if (canvasCookProgress.activeInHierarchy) {
					canvasCookProgress.SetActive (false);
					Debug.Log ("--->current food: " + currentFood);
					int nCookId = CharacterAniController.Instance().GetCookIndex (currentFood);
					sprite_cook.sprite = CharacterAniController.Instance ().cookSpriteList [nCookId];
				}
				if (!canvasBurnProgress.activeSelf && fTimeTick < (fCookTime + fBurnTime)) {
					canvasBurnProgress.SetActive (true);
					SoundManager.Instance ().PlaySound (5);
//					if (GameFlags.bTutorial && utensilType == UtensilType.Oven && utensilIndex == 0 && TutorialManager.Instance().nTutorialIndex == 3)
//						
//						TutorialManager.Instance ().ShowTutorial (3);
				}
				
				if (fTimeTick < (fCookTime + fBurnTime)) {
					fTimeTick += Time.deltaTime;
					circle_burn.fillAmount = (fTimeTick - fCookTime) / fBurnTime;
				} else {
					if (!GameFlags.bTutorial) {
						if (currentFood != "Burn")
							SoundManager.Instance ().PlaySound (8);
						circle_burn.fillAmount = 1f;
						currentFood = "Burn";
						if (canvasBurnProgress.activeSelf) {
							canvasBurnProgress.SetActive (false);
							int nCookId = CharacterAniController.Instance ().GetCookIndex (currentFood);
							sprite_cook.sprite = CharacterAniController.Instance ().cookSpriteList [nCookId];
							aniCook.SetTrigger ("CookDone");
							aniCook.ResetTrigger ("CookEmpty");
						}
					}
				}
				//Debug.Log ("burnAmount:" + circle_burn.fillAmount);
			}
		}
		if (currentFood != "" && fTimeTick > fCookTime) {
			if (sprite_cook != null) {
				sprite_cook.gameObject.SetActive (true);
			}
		} else {
			if (sprite_cook != null) {
				sprite_cook.gameObject.SetActive (false);
			}
		}
	}

	public string GetFood() {
		return currentFood;
	}

	public void EmptyUtensil() {
		currentFood = "";
		utensilStatus = UtensilCookingStatus.Empty;
		if (circle_progress != null)
			circle_progress.fillAmount = 0;
		if (canvasCookProgress != null)
			canvasCookProgress.SetActive (false);
		if (circle_burn != null)
			circle_burn.fillAmount = 0;
		if (canvasBurnProgress != null)
			canvasBurnProgress.SetActive (false);
		if (drinkMeter != null) {
			Vector3 currentScale = drinkMeter.transform.localScale;
			drinkMeter.transform.localScale = new Vector3 (currentScale.x, 0.9f, currentScale.z);
		}
//		if (utensilStatus == UtensilCookingStatus.Cooking)
//			aniCook.SetTrigger ("CookDone");
//		aniCook.SetTrigger ("CookEmpty");

		if (!aniCook.GetCurrentAnimatorStateInfo(0).IsName("ovenEmpty"))
			aniCook.Play("ovenEmpty");
		if (!aniCook.GetCurrentAnimatorStateInfo(0).IsName("stoveEmpty"))
			aniCook.Play("stoveEmpty");
		if (!aniCook.GetCurrentAnimatorStateInfo(0).IsName("coffeeMachineEmpty"))
			aniCook.Play("coffeeMachineEmpty");
		if (!aniCook.GetCurrentAnimatorStateInfo(0).IsName("coldMachineEmpty"))
			aniCook.Play("coldMachineEmpty");
		
		Debug.Log ("CookEmpty");
		this.aniCook.ResetTrigger ("CookDone");
		fTimeTick = 0;
	}
		
}


