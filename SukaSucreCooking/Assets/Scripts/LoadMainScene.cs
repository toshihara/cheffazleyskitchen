﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Net;
using System.IO;

public class LoadMainScene : MonoBehaviour {
	public GameObject prefConnectionErrorPopup;
	// Use this for initialization
	void Start () {
		StartCoroutine (LoadNextScene ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public string GetHtmlFromUri(string resource)
	{
	     string html = string.Empty;
	     HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
	     try
	     {
	         using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
	         {
	             bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
	             if (isSuccess)
	             {
	                 using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
	                 {
	                     //We are limiting the array to 80 so we don't have
	                     //to parse the entire html document feel free to 
	                     //adjust (probably stay under 300)
	                     char[] cs = new char[80];
	                     reader.Read(cs, 0, cs.Length);
	                     foreach(char ch in cs)
	                     {
	                         html +=ch;
	                     }
	                 }
	             }
	         }
	     }
	     catch
	     {
	         return "";
	     }
	     return html;
	 }

	bool checkInternetConnection(){
		string HtmlText = GetHtmlFromUri("http://bing.com");
		if (HtmlText == "") {
			return false;
		} else 
		return true;
	}


	IEnumerator LoadNextScene() {
		yield return new WaitForSeconds (4f);
		if (checkInternetConnection ())
			SceneManager.LoadScene (1);
		else {
			GameObject objConnectionError = Instantiate (prefConnectionErrorPopup) as GameObject;
			objConnectionError.transform.parent = GameObject.Find ("Canvas").transform;
			objConnectionError.transform.localScale = 0.8f * Vector3.one;
			objConnectionError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -200f);
			objConnectionError.GetComponent<PopupSignError> ().Open ();
		}

	}
}
