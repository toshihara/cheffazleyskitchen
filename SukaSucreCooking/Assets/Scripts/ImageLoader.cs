﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageLoader : MonoBehaviour {
	public RawImage productImage;
	public RawImage zoomProductImage;
	public Animator aniZoomer;
	public Image pnl_zoom;
	//public string url = "http://images.earthcam.com/ec_metros/ourcams/fridays.jpg";

	IEnumerator Start()
	{
		// Start a download of the given URL
		WWW www = new WWW(GameFlags.productImageUrl);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			productImage.texture = www.texture;
			zoomProductImage.texture = www.texture;
		}

	}

	public void ZoomIn() {
		GameFlags.bPauseGame = true;
		pnl_zoom.gameObject.SetActive (true);
		aniZoomer.SetTrigger ("zoomin");
	}

	public void ZoomOut() {
		GameFlags.bPauseGame = false;
		pnl_zoom.gameObject.SetActive (false);
		aniZoomer.SetTrigger ("zoomout");
	}

	public void Go2Link() {
		Application.OpenURL (GameFlags.productTextUrl);
	}

}
