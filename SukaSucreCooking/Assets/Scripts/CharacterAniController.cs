﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;
using Spine.Unity;

public class CharacterAniController : MonoBehaviour {
	public static CharacterAniController chiefController;
	public Sprite[] characterSprites;
	public SpriteRenderer characterImg;
	public AILerp aiLerp;
	Vector3 currentDirection = Vector3.zero;
	CharacterMovementDirection movementDirection;
	public Sprite[] foodMatList;
	public Sprite[] cookSpriteList;
	public SpriteRenderer currentFoodMatSprite;

	public Furniture destinationFurniture;
	public Food destinationFood;
	public WaitingTableController destinationTable;

	public List<Furniture> targetFurnitureList;
	public List<Food> targetFoodList;
	public List<WaitingTableController> targetTableController;
	public List<int> targetWarmingStationList;
	public List<string> targetCategories;

	public List<FoodMaterial> currentFoodMaterialList = new List<FoodMaterial>();
//	public string foodName = "";
//	public bool bHoldCook;
	public bool bClickDustBin;
	public int nWarmerIndex = -1;
	public WarmerHandler[] warms;
	public Hand leftHand;
	public Hand rightHand;
	public SkeletonAnimation skeletonAni;
	public bool bReachDestination = false;
	public bool bTakeFood = false;
	TargetMover targetMover;

	public static CharacterAniController Instance() {
		if (chiefController == null) {
			chiefController = FindObjectOfType (typeof(CharacterAniController)) as CharacterAniController;
			if (!chiefController) {
				Debug.Log ("There should be one active Object");
			 }
		}
		return chiefController;
	}

	public void InitChief() {
		destinationFurniture = null;
		destinationFood = null;
		destinationTable = null;
//		foodName = "";
		currentFoodMaterialList.Clear ();
		//this.characterImg.sprite = characterSprites[0];
		skeletonAni.AnimationName = "Idle 2_HandsDown_2";
//		bHoldCook = false;
		nWarmerIndex = -1;
		leftHand.EmptyHand ();
		rightHand.EmptyHand ();
		if (targetMover == null)
			targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover> ();
		targetMover.InitTargetList ();
		targetCategories = new List<string> ();
		targetFoodList = new List<Food> ();
		targetFurnitureList = new List<Furniture> ();
		targetTableController = new List<WaitingTableController> ();
		targetWarmingStationList = new List<int> ();
	}

	public int GetCookIndex(string cookName) {
		int nCookId = -1;
		switch (cookName) {
		case "Plain Macaron":
			nCookId = 0;
			break;
		case "Chocolate Macaron":
			nCookId = 1;
			break;
		case "Chocolate Caramel Macaron":
			nCookId = 2;
			break;
		case "Churros Plain":
			nCookId = 3;
			break;
		case "Churros Chocolate":
			nCookId = 4;
			break;
		case "Churros Chocolate Caramel":
			nCookId = 5;
			break;
		case "Plain Cake":
			nCookId = 6;
			break;
		case "Chocolate Cake":
			nCookId = 7;
			break;
		case "Chocolate Caramel Cake":
			nCookId = 8;
			break;
		case "Pasta Piyash":
			nCookId = 9;
			break;
		case "Pasta Payish":
			nCookId = 10;
			break;
		case "Pasta Daging Salai Lemak Cili Api":
			nCookId = 11;
			break;
		case "French Butter Rice":
			nCookId = 12;
			break;
		case "French Butter Rice With Rendang & Vegetable Pickles":
			nCookId = 13;
			break;
		case "French Butter Rice With Daging Salai Lemak Cili Api":
			nCookId = 14;
			break;
		case "Ratatouille Pizza":
			nCookId = 15;
			break;
		case "Rendang Pizza":
			nCookId = 16;
			break;
		case "Heavy Duty Meat Pizza":
			nCookId = 17;
			break;
		case "Toasted Eclair":
			nCookId = 18;
			break;
		case "Toasted Eclair Pulled Beef Rendang":
			nCookId = 19;
			break;
		case "Toasted Eclair Daging Salai":
			nCookId = 20;
			break;
		case "Americano":
			nCookId = 21;
			break;
		case "Caramel Latte":
			nCookId = 22;
			break;
		case "Hot Chocolate":
			nCookId = 23;
			break;
		case "Frozen Grapes Mint Soda no_ice":
			nCookId = 24;
			break;
		case "Soda Bandung no_ice":
			nCookId = 25;
			break;
		case "Sucre Tea no_ice":
			nCookId = 26;
			break;
		case "Frozen Grapes Mint Soda":
			nCookId = 27;
			break;
		case "Soda Bandung":
			nCookId = 28;
			break;
		case "Sucre Tea":
			nCookId = 29;
			break;
		case "Burn":
			nCookId = 30;
			break;
		}
		return nCookId;
	}

	//Need to check and add logic if it choose 
	public void OnReachedDestination() {
		ReachPathEnd ();
//		if (destinationFurniture != null)
		if (targetCategories.Count == 0)
			return;
		if (targetCategories[0] == "Furniture" && targetFurnitureList.Count > 0)
		{
			if (targetFurnitureList[0] != null) {
				//targetFurnitureList [0] = destinationFurniture;
				destinationFurniture = targetFurnitureList[0];
				if (destinationFurniture.utensilStatus == UtensilCookingStatus.Empty) {
					bool bAccepted = false;
					if (rightHand.currentFoodMaterialList.Count > 0 || destinationFurniture.utensilType == UtensilType.CoffeeMachine || destinationFurniture.utensilType == UtensilType.ColdMachine) {
						bAccepted = destinationFurniture.ReceiveFoodMaterials (rightHand.currentFoodMaterialList);
						if (bAccepted) {
							rightHand.currentFoodMaterialList.Clear ();
							if (destinationFurniture.utensilType == UtensilType.Oven) {
								ChefAudioManager.Instance ().PutRaw2Oven ();
//								if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 3) {
//									TutorialManager.Instance ().ShowTutorial ();
//								}
							}
							else if (destinationFurniture.utensilType == UtensilType.Stove)
								ChefAudioManager.Instance ().PutRaw2Stove ();
							else if (destinationFurniture.utensilType == UtensilType.CoffeeMachine || destinationFurniture.utensilType == UtensilType.ColdMachine) {
								ChefAudioManager.Instance ().PressDrinkMachine ();
							}
						}
					}
					if (leftHand.currentFoodMaterialList.Count > 0 || destinationFurniture.utensilType == UtensilType.CoffeeMachine || destinationFurniture.utensilType == UtensilType.ColdMachine) {
						if (!bAccepted) {
							bAccepted = destinationFurniture.ReceiveFoodMaterials (leftHand.currentFoodMaterialList);
							if (bAccepted) {
								leftHand.currentFoodMaterialList.Clear ();
								if (destinationFurniture.utensilType == UtensilType.Oven) {
									ChefAudioManager.Instance ().PutRaw2Oven ();
//									if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 3) {
//										TutorialManager.Instance ().ShowTutorial ();
//									}
								}
								else if (destinationFurniture.utensilType == UtensilType.Stove)
									ChefAudioManager.Instance ().PutRaw2Stove ();
								else if (destinationFurniture.utensilType == UtensilType.CoffeeMachine || destinationFurniture.utensilType == UtensilType.ColdMachine) {
									ChefAudioManager.Instance ().PressDrinkMachine ();
								}
							}
						}
					}
					destinationFurniture = null;
				} else if (destinationFurniture.utensilStatus == UtensilCookingStatus.CookDone) {
					if (!rightHand.bHoldCook && rightHand.currentFoodMaterialList.Count == 0) {
						rightHand.GetCook ();
						ChefAudioManager.Instance ().PickupReadyFood ();
						if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 4) {
							TutorialManager.Instance ().ShowTutorial (4);
						}
					} else if (!leftHand.bHoldCook && leftHand.currentFoodMaterialList.Count == 0 && GameFlags.bTutorial == false) {
						leftHand.GetCook ();
						ChefAudioManager.Instance ().PickupReadyFood ();
						if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 4) {
							TutorialManager.Instance ().ShowTutorial (4);
						}
					}
				}
				targetFurnitureList [0] = null;
				targetFurnitureList.RemoveAt (0);
			}
		}
		//if (destinationFood != null) 
		if (targetCategories[0] == "Food" && targetFoodList.Count > 0)
		{
			if (targetFoodList[0] != null) {
			destinationFood = targetFoodList[0];
			if (!rightHand.bHoldCook || destinationFood.foodMaterial == FoodMaterial.Ice)
				rightHand.CheckNAddFoodMaterial (destinationFood.foodMaterial);
			if (destinationFood != null) {
				if (!leftHand.bHoldCook || destinationFood.foodMaterial == FoodMaterial.Ice)
					leftHand.CheckNAddFoodMaterial (destinationFood.foodMaterial);
			}
			if (destinationFood != null) {
				if (destinationFood.foodMaterial == FoodMaterial.Ice)
					destinationFood = null;
			}
			targetFoodList [0] = null;
			targetFoodList.RemoveAt (0);
			//targetCategories.RemoveAt (0);
			}
		}

		//if (destinationTable != null) 
		if (targetCategories[0] == "WaitingTable" && targetTableController.Count > 0)
		{
			if (targetTableController [0] != null) {
				destinationTable = targetTableController [0];
				bool result = destinationTable.CheckCustomerOrder ();
				destinationTable = null;
				if (result) {
					ChefAudioManager.Instance ().PutFood2Table ();
					if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 5) {
						TutorialManager.Instance ().ShowTutorial (5);
					}
				}
				targetTableController [0] = null;
				targetTableController.RemoveAt (0);
				//targetCategories.RemoveAt (0);
			}
		}
		//if (bClickDustBin) 
		if (targetCategories[0] == "Dustbin" && bClickDustBin){
			Throw2DustBin ();
			//targetCategories.RemoveAt (0);
		}
		//if (nWarmerIndex > -1) 
		if (targetCategories[0] == "Warmer" && targetWarmingStationList.Count > 0)
		{
			nWarmerIndex = targetWarmingStationList[0];
			int nHandId = -1;
			bool bEmptyLeft = false;
			bool bEmptyRight = false;
			bool bLockLeft = false;
			bool bLockRight = false;
			if (!rightHand.bHoldCook && rightHand.currentFoodMaterialList.Count == 0) {
				bEmptyRight = true;
			} else if (!leftHand.bHoldCook && leftHand.currentFoodMaterialList.Count == 0) {
				bEmptyLeft = true;
			}

			if (leftHand.currentFoodMaterialList.Count > 0 || !CheckValidCook(leftHand.foodName))
				bLockLeft = true;
			if (rightHand.currentFoodMaterialList.Count > 0 || !CheckValidCook(rightHand.foodName))
				bLockRight = true;
			
			if (warms [nWarmerIndex].cookName != "") {
				if (bEmptyLeft && bEmptyRight) {
					if (bLockLeft && bLockRight)
						nHandId = -1;
					else if (bLockLeft)
						nHandId = 0;
					else if (bLockRight)
						nHandId = 1;
					else
						nHandId = 0;
				
				} else if (bEmptyRight) {
					if (!bLockRight)
						nHandId = 0;
					else
						nHandId = -1;
				} else if (bEmptyLeft) {
					if (!bLockLeft)
						nHandId = 1;
					else
						nHandId = -1;
				} else {
					if (bLockLeft && bLockRight)
						nHandId = -1;
					else if (bLockLeft)
						nHandId = 0;
					else if (bLockRight)
						nHandId = 1;
					else
						nHandId = Random.Range (0, 2);
				}
			}
			else { // empty warm station case
				if (bEmptyLeft && bEmptyRight)
					nHandId = -1;
				else if (bEmptyLeft && !bLockRight)
					nHandId = 0;
				else if (bEmptyRight && !bLockLeft)
					nHandId = 1;
				else if (!bEmptyLeft && !bEmptyRight) {
					if (!bLockRight && !bLockLeft)
						nHandId = Random.Range (0, 2);
					else if (!bLockRight)
						nHandId = 0;
					else if (!bLockLeft)
						nHandId = 1;
				}
			}

			if (nHandId > -1) {
				if (nHandId == 0) {
					if (!rightHand.bHoldCook) {
						if (warms [nWarmerIndex].cookName != "") {
							rightHand.foodName = warms [nWarmerIndex].GiveBackCook ();
							if (rightHand.foodName != "") {
								int nCookId = GetCookIndex (rightHand.foodName);
								if (nCookId > -1) {
									rightHand.bHoldCook = true;
									rightHand.UpdateHandSprites (cookSpriteList [nCookId]);
								}
							}
						}
					} else {
						if (rightHand.foodName != "" && CheckValidCook(rightHand.foodName)) {
							string strWarmCook = "";
							if (warms [nWarmerIndex].cookName != "") {
								strWarmCook = warms [nWarmerIndex].GiveBackCook ();
							}
							if (warms [nWarmerIndex].ReceiveCook (rightHand.foodName)) {
								rightHand.foodName = strWarmCook;
								if (rightHand.foodName == "")
									rightHand.bHoldCook = false;
								else {
									int nCookId = GetCookIndex (rightHand.foodName);
									if (nCookId > -1) {
										rightHand.bHoldCook = true;
										rightHand.UpdateHandSprites (cookSpriteList [nCookId]);
									}
								}
							}
						}
					}
				} // right hand
				//lefthand
				else if (nHandId == 1) {
					if (!leftHand.bHoldCook) {
						if (warms [nWarmerIndex].cookName != "") {
							leftHand.foodName = warms [nWarmerIndex].GiveBackCook ();
							if (leftHand.foodName != "") {
								int nCookId = GetCookIndex (leftHand.foodName);
								if (nCookId > -1) {
									leftHand.bHoldCook = true;
									leftHand.UpdateHandSprites (cookSpriteList [nCookId]);
								}
							}
						}
					} else {
						if (leftHand.foodName != "" && CheckValidCook(leftHand.foodName)) {
							string strWarmCook = "";
							if (warms [nWarmerIndex].cookName != "") {
								strWarmCook = warms [nWarmerIndex].GiveBackCook ();
							}
							if (warms [nWarmerIndex].ReceiveCook (leftHand.foodName)) {
								leftHand.foodName = strWarmCook;
								if (leftHand.foodName == "")
									leftHand.bHoldCook = false;
								else {
									int nCookId = GetCookIndex (leftHand.foodName);
									if (nCookId > -1) {
										leftHand.bHoldCook = true;
										leftHand.UpdateHandSprites (cookSpriteList [nCookId]);
									}
								}
							}
						}
					}
				} // left hand
			}
			nWarmerIndex = -1;
			targetWarmingStationList.RemoveAt(0);
			//targetCategories.RemoveAt (0);
		}
		targetMover.PopTargetPositionFromList();
		targetCategories.RemoveAt (0);
	}

	bool CheckValidCook(string cookName) {
		if (cookName == "Frozen Grapes Mint Soda no_ice" || cookName == "Soda Bandung no_ice" || cookName == "Sucre Tea no_ice" || cookName == "Burn")
			return false;
		return true;
	}

	public void Go2DustBin() {
		bClickDustBin = true;
		this.destinationFood = null;
		this.destinationFurniture = null;
		this.destinationTable = null;
	}

	public void Throw2DustBin() {

		if (rightHand.foodName == "Burn") {
			rightHand.Throw2DustBin();
			bClickDustBin = false;
			return;
		}

		if (leftHand.foodName == "Burn") {
			leftHand.Throw2DustBin();
			bClickDustBin = false;
			return;
		}

		if (rightHand.foodName == "" && rightHand.currentFoodMaterialList.Count > 0) {
			rightHand.Throw2DustBin();
			bClickDustBin = false;
			return;
		} else if (leftHand.foodName == "" && leftHand.currentFoodMaterialList.Count > 0) {
			leftHand.Throw2DustBin();
			bClickDustBin = false;
			return;
		}

		if (!rightHand.Throw2DustBin ())
			leftHand.Throw2DustBin ();
		bClickDustBin = false;
	}

	// Use this for initialization
	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover> ();
	}
	
	// Update is called once per frame
	void Update () {
		CheckRotationValue ();
	}

	public void CheckRotationValue() {
//		if (!bReachDestination) {
			// if direction is changed??
			if (this.currentDirection != aiLerp.direction) {
				if (aiLerp.direction.x == -1f) {
					if (aiLerp.direction.y == 0)
						movementDirection = CharacterMovementDirection.Left;
					else if (aiLerp.direction.y == 1f)
						movementDirection = CharacterMovementDirection.TopLeft;
					else if (aiLerp.direction.y == -1f)
						movementDirection = CharacterMovementDirection.BottomLeft;
					
				} else if (aiLerp.direction.x == 0) {
					if (aiLerp.direction.y == -1f)
						movementDirection = CharacterMovementDirection.Down;
					else if (aiLerp.direction.y == 1f)
						movementDirection = CharacterMovementDirection.Up;
				} else if (aiLerp.direction.x == 1f) {
					if (aiLerp.direction.y == -1f)
						movementDirection = CharacterMovementDirection.BottomRight;
					else if (aiLerp.direction.y == 0)
						movementDirection = CharacterMovementDirection.Right;
					else if (aiLerp.direction.y == 1f)
						movementDirection = CharacterMovementDirection.TopRight;
				} 
			}
//			Debug.Log("+++ bReachDestination: " + bReachDestination);
//			Debug.Log("Movement Direction: " + movementDirection);

			bool bRightHand = rightHand.IsHandEmpty ();
			bool bLeftHand = leftHand.IsHandEmpty ();

			if (bReachDestination) {
				if (bRightHand && bLeftHand) {
					GameFlags.currentPlayerPose = PlayerPoseSituation.IdleHandsDown;
					if (skeletonAni.AnimationName != "Idle 2_HandsDown_2")
						skeletonAni.AnimationName = "Idle 2_HandsDown_2";
				} else if (bRightHand && !bLeftHand) {
					GameFlags.currentPlayerPose = PlayerPoseSituation.IdleLeftHandUp;
					if (skeletonAni.AnimationName != "Idle 2_L_HandUp_2")
						skeletonAni.AnimationName = "Idle 2_L_HandUp_2";
				} else if (!bRightHand && bLeftHand) {
					GameFlags.currentPlayerPose = PlayerPoseSituation.IdleRightHandUp;
					if (skeletonAni.AnimationName != "Idle 2_R_HandUp_2")
						skeletonAni.AnimationName = "Idle 2_R_HandUp_2";
				} else if (!bRightHand && !bLeftHand) {
					GameFlags.currentPlayerPose = PlayerPoseSituation.IdleHandsUp;
					if (skeletonAni.AnimationName != "Idle 2_HandsUp_2")
						skeletonAni.AnimationName = "Idle 2_HandsUp_2";
				}
			} else {
			Debug.Log ("movement Direction: " + movementDirection);
			switch (movementDirection) {
				case CharacterMovementDirection.Left:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsDown;
						if (skeletonAni.AnimationName != "Walk_Left_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Left_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_L_Hand Up_2")
							skeletonAni.AnimationName = "Walk_Left_L_Hand Up_2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_R_HandUp_2")
							skeletonAni.AnimationName = "Walk_Left_R_HandUp_2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsUp;
						if (skeletonAni.AnimationName != "Walk_Left_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Left_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.Right:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsDown;
						if (skeletonAni.AnimationName != "Walk_Right_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Right_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_L_Hand Up-2")
							skeletonAni.AnimationName = "Walk_Right_L_Hand Up-2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_R_HandUp-2")
							skeletonAni.AnimationName = "Walk_Right_R_HandUp-2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsUp;
						if (skeletonAni.AnimationName != "Walk_Right_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Right_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.Up:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkBackHandsDown;
						if (skeletonAni.AnimationName != "Walk__HandsDown-2")
							skeletonAni.AnimationName = "Walk__HandsDown-2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkBackLeftHandUp;
						if (skeletonAni.AnimationName != "Walk__LHandUp-2")
							skeletonAni.AnimationName = "Walk__LHandUp-2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkBackRightHandUp;
						if (skeletonAni.AnimationName != "Walk__RHandUp-2")
							skeletonAni.AnimationName = "Walk__RHandUp-2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkBackHandsUp;
						if (skeletonAni.AnimationName != "Walk__HandsUp-2")
							skeletonAni.AnimationName = "Walk__HandsUp-2";
					}
					break;
				case CharacterMovementDirection.Down:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkFrontHandsDown;
						if (skeletonAni.AnimationName != "Walk_Front_HandsDown-2")
							skeletonAni.AnimationName = "Walk_Front_HandsDown-2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkFrontLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Front_L_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Front_L_HandsUp-2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkFrontRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Front_R_HandUp-2")
							skeletonAni.AnimationName = "Walk_Front_R_HandUp-2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkFrontHandsUp;
						if (skeletonAni.AnimationName != "Walk_Front_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Front_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.BottomLeft:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsDown;
						if (skeletonAni.AnimationName != "Walk_Left_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Left_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_L_Hand Up_2")
							skeletonAni.AnimationName = "Walk_Left_L_Hand Up_2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_R_HandUp_2")
							skeletonAni.AnimationName = "Walk_Left_R_HandUp_2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsUp;
						if (skeletonAni.AnimationName != "Walk_Left_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Left_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.BottomRight:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsDown;
						if (skeletonAni.AnimationName != "Walk_Right_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Right_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_L_Hand Up-2")
							skeletonAni.AnimationName = "Walk_Right_L_Hand Up-2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_R_HandUp-2")
							skeletonAni.AnimationName = "Walk_Right_R_HandUp-2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsUp;
						if (skeletonAni.AnimationName != "Walk_Right_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Right_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.TopLeft:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsDown;
						if (skeletonAni.AnimationName != "Walk_Left_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Left_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_L_Hand Up_2")
							skeletonAni.AnimationName = "Walk_Left_L_Hand Up_2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Left_R_HandUp_2")
							skeletonAni.AnimationName = "Walk_Left_R_HandUp_2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkLeftHandsUp;
						if (skeletonAni.AnimationName != "Walk_Left_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Left_HandsUp-2";
					}
					break;
				case CharacterMovementDirection.TopRight:
					if (bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsDown;
						if (skeletonAni.AnimationName != "Walk_Right_HandsDown_2")
							skeletonAni.AnimationName = "Walk_Right_HandsDown_2";
					} else if (bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightLeftHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_L_Hand Up-2")
							skeletonAni.AnimationName = "Walk_Right_L_Hand Up-2";
					} else if (!bRightHand && bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightRightHandUp;
						if (skeletonAni.AnimationName != "Walk_Right_R_HandUp-2")
							skeletonAni.AnimationName = "Walk_Right_R_HandUp-2";
					} else if (!bRightHand && !bLeftHand) {
						GameFlags.currentPlayerPose = PlayerPoseSituation.WalkRightHandsUp;
						if (skeletonAni.AnimationName != "Walk_Right_HandsUp-2")
							skeletonAni.AnimationName = "Walk_Right_HandsUp-2";
					}
					break;
				}

			}
			Debug.Log("AniName: " + skeletonAni.AnimationName);
//		}
	}

	public void ReachPathEnd() {
	//	Debug.Log ("ReachPathEnd");
	//	targetMover.PopTargetPositionFromList();
		bReachDestination = true;
		bool bRightHand = rightHand.IsHandEmpty ();
		bool bLeftHand = leftHand.IsHandEmpty ();

		if (bRightHand && bLeftHand) {
			GameFlags.currentPlayerPose = PlayerPoseSituation.IdleHandsDown;
			if (skeletonAni.AnimationName != "Idle 2_HandsDown_2")
				skeletonAni.AnimationName = "Idle 2_HandsDown_2";
		} else if (bRightHand && !bLeftHand) {
			GameFlags.currentPlayerPose = PlayerPoseSituation.IdleLeftHandUp;
			if (skeletonAni.AnimationName != "Idle 2_L_HandUp_2")
				skeletonAni.AnimationName = "Idle 2_L_HandUp_2";
		} else if (!bRightHand && bLeftHand) {
			GameFlags.currentPlayerPose = PlayerPoseSituation.IdleRightHandUp;
			if (skeletonAni.AnimationName != "Idle 2_R_HandUp_2")
				skeletonAni.AnimationName = "Idle 2_R_HandUp_2";
		} else if (!bRightHand && !bLeftHand) {
			GameFlags.currentPlayerPose = PlayerPoseSituation.IdleHandsUp;
			if (skeletonAni.AnimationName != "Idle 2_HandsUp_2")
				skeletonAni.AnimationName = "Idle 2_HandsUp_2";
		}
	//	Debug.Log ("hand state: " + bRightHand + "/" + bLeftHand );
	}


}

public enum CharacterMovementDirection {
	Left,
	Right,
	Up,
	Down,
	BottomLeft,
	BottomRight,
	TopLeft,
	TopRight
}

public enum CharacterStatus {
	Idle,
	Moving,
	Handling
}
	