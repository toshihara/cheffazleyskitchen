﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class PurchaseManager : MonoBehaviour, IStoreListener
{
	private static IStoreController m_StoreController;         
	private static IExtensionProvider m_StoreExtensionProvider; 

	private static string kProductID4kChips = "coins.4000";
	private static string kProductID12kChips = "coins.12000";
	private static string kProductID20kChips = "coins.20000";
	private static string kProductIDSuperChips = "superchef.2x";
	private static string kProductIDAdsChips = "removeAds";

	public static PurchaseManager instance;

	public static PurchaseManager Instance() {
		if (!instance) {
			instance = FindObjectOfType (typeof(PurchaseManager)) as PurchaseManager;
		}
		return instance;
	}

	void Start()
	{
		if (m_StoreController == null)
		{
			InitializePurchasing();
		}
	}

	public void InitializePurchasing() 
	{
		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		builder.AddProduct (kProductID4kChips, ProductType.Consumable);
		builder.AddProduct (kProductID12kChips, ProductType.Consumable);
		builder.AddProduct (kProductID20kChips, ProductType.Consumable);
		builder.AddProduct (kProductIDSuperChips, ProductType.Consumable);
		builder.AddProduct (kProductIDAdsChips, ProductType.Consumable);

		UnityPurchasing.Initialize(this, builder);
	}


	private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void Buy4kChips()
	{
		BuyProductID (kProductID4kChips);
	}

	public void Buy12kChips()
	{
		BuyProductID (kProductID12kChips);
	}

	public void Buy20kChips()
	{
		BuyProductID (kProductID20kChips);
	}

	public void BuySuperChips()
	{
		BuyProductID (kProductIDSuperChips);
	}

	public void BuyAdsChips()
	{
		BuyProductID (kProductIDAdsChips);
	}


	void BuyProductID(string productId)
	{
		if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productId);

			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				m_StoreController.InitiatePurchase(product);
			}
			else
			{
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else
		{
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}


	//  
	// --- IStoreListener
	//

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("OnInitialized: PASS");

		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		int coinCount = PlayerPrefs.GetInt ("Coin", 0);

		if (string.Equals (args.purchasedProduct.definition.id, kProductID4kChips, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin",coinCount + 4000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text>().text = string.Format("{0:#,#}",PlayerPrefs.GetString ("Coin"));
		}

		else if (string.Equals (args.purchasedProduct.definition.id, kProductID12kChips, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin",coinCount + 12000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text>().text = string.Format("{0:#,#}",PlayerPrefs.GetString ("Coin"));
		}

		else if (string.Equals (args.purchasedProduct.definition.id, kProductID20kChips, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin",coinCount + 20000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text>().text = string.Format("{0:#,#}",PlayerPrefs.GetString ("Coin"));
		}

		else if (string.Equals (args.purchasedProduct.definition.id, kProductIDSuperChips, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("SpeedUp", 1);
			GameObject.Find ("ChefSpeedBooster").SetActive (false);
		}

		else if (string.Equals (args.purchasedProduct.definition.id, kProductIDAdsChips, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Ads", 1);
		}



		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}
}