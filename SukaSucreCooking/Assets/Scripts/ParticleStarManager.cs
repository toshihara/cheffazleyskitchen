﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleStarManager : MonoBehaviour {
	public starFxController starFx;
	public GameObject sunShine;
	public GameObject backPanel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetStarFx(int nStarNum) {
		
		starFx.Reset();
		starFx.ea = nStarNum;
		sunShine.SetActive (true);
		backPanel.SetActive (true);
	}

	public void ResetStarFx() {
		
		starFx.Reset();
		sunShine.SetActive (false);
		backPanel.SetActive (false);

	}
}
