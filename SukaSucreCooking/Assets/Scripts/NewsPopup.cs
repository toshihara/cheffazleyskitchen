﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.UI;
using System;
using Ricimi;

public class NewsPopup : MonoBehaviour {
	public Text txt_feed_title;
	public Text txt_feed_content;
	public RawImage img_feed_img;
	public GameObject btn_link;
	public Text txt_link;
	public GameObject prefPopupRules;
	int CurrentItemPlacement = 0;
	// Use this for initialization
	public void InitPopup(int nFeedIndex) {
		ChangeAndDisplayText (nFeedIndex);
	}


	private void ChangeAndDisplayText (int currentItem) {
		// Make sure we aren't looking for other images, hide the blank image unless we do.
		StopAllCoroutines ();
		//ArticleImage.SetActive (false);

		// Set our current place and get all applicable items.
		CurrentItemPlacement = currentItem;
		txt_feed_title.text = GameFlags.RSSItems[CurrentItemPlacement].Title;
		txt_feed_content.text = GameFlags.RSSItems[CurrentItemPlacement].Description;
		if (GameFlags.RSSItems[CurrentItemPlacement].Image != "") {
//			if (GameFlags.RSSItems[CurrentItemPlacement].Image.Contains("youtube")) {
//				StartCoroutine (LoadCurrentImage (GameFlags.RSSItems[CurrentItemPlacement].Image));
//			} else {
//				StartCoroutine (LoadCurrentImage (GameFlags.RSSItems[CurrentItemPlacement].Image));
//			}
			StartCoroutine(LoadCurrentImage(GameFlags.RSSItems[CurrentItemPlacement].Image));
		}
		if (GameFlags.RSSItems [CurrentItemPlacement].ButtonLabel != "") {
			btn_link.SetActive (true);
			txt_link.text = GameFlags.RSSItems [CurrentItemPlacement].ButtonLabel;
		} else {
			btn_link.SetActive (false);
		}
	}

	private IEnumerator LoadCurrentImage (string imageText) {
		Debug.Log ("Image Loading");
		WWW imageLink = new WWW (imageText);
		yield return imageLink;
		if (string.IsNullOrEmpty (imageLink.error)) {
			Debug.Log ("Image Loaded");
			img_feed_img.texture = imageLink.texture;
			int texHeight = imageLink.texture.height;
			int texWidth = imageLink.texture.width;
			Debug.Log ("width/height: " + texWidth + "/" +texHeight);
			float scaleX = 1f;
			float scaleY = 1f;
			if (texHeight > texWidth) {
				scaleX = texWidth * 1f / texHeight;
			} else if (texHeight < texWidth) {
				scaleY = texHeight * 1f / texWidth;
			}
			img_feed_img.transform.localScale = new Vector3 (scaleX, scaleY, 1f);
		}
	}

	public void ClickLinkButton() {
		if (GameFlags.RSSItems [CurrentItemPlacement].Link != "") {
			if (!GameFlags.RSSItems [CurrentItemPlacement].ButtonLabel.ToLower().Contains("contest rules"))
				Application.OpenURL (GameFlags.RSSItems [CurrentItemPlacement].Link);
			else {
				GameObject popupRules = Instantiate (prefPopupRules) as GameObject;
				popupRules.transform.parent = GameObject.Find ("Canvas").transform;
				popupRules.transform.localScale = 0.8f * Vector3.one;
				popupRules.GetComponent<RectTransform> ().anchoredPosition = new Vector2(0, -100f);
				popupRules.GetComponent<Popup> ().Open ();
			}
		}
	}
}
