﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlags {
	
	public ClickedObjectType clickedObjectType = ClickedObjectType.None;
	public static float fTimeRate = 8f;
	public static float fGameRound = 90f;
	public static int 	nScoreLimit = 150;
	public static int nPlainMacaronScore = 15;
	public static bool bEndRound = false;
	public static bool bPauseGame = false;
	public static bool bEmptySeat = false;
	public static bool bTimesup = false;
	public static int nCurrentRound = 0;
	public static List<RoundInfomation> roundInfoList = new List<RoundInfomation>();
	public static string CMPT = "COMPLETE";
	public static string FAIL = "FAILED";
	public static float fProgressTarget = 0.35f;
	public static float fProgressBonus = 0.15f;
	public static float fEmojiGuage = 400f;
	public static string userName = "";
	public static string email = "";
	public static string password = "";
	public static string locale = "";
	public static int nRankingLimit = 30;
	public static bool bSoundMute = false;
	public static bool bMusicMute = false;
	public static int nStars = 0;

	public static PlayerPoseSituation currentPlayerPose = PlayerPoseSituation.IdleHandsDown;
	public static string productImageUrl = "http://kometdigital.com/games/chef/product.png";
	public static string productTextUrl = "http://kometdigital.com/games/chef/product.php";
	public static string testImageUrl = "http://rasamalaysia.com/wp-content/uploads/2016/03/chicken-rendang.jpg";
	public static int MAX_PLAYING_COUNT = 3;
	public static Vector3 CoinController = new Vector3 (-18.11f, 11.75f, -0.3f);
	public static List<RSSContentItem> RSSItems = new List<RSSContentItem>();
	public static bool bFBLogin = false;
	public static bool bTutorial = false;

}

public enum CookType {
	Food,
	Drink
}

public enum UtensilType {
	None,
	Warm,
	Oven,
	Stove,
	Dustbin,
	IceMachine,
	CoffeeMachine,
	ColdMachine,
	RecipeBook
}

public enum ClickedObjectType {
	None,
	Utensil,
	FoodMaterial,
	Customer
}

public enum FoodMaterial {
	Macaron,
	Chocolate,
	Churros,
	Eclairs,
	Cake,
	Caramel,
	Meat,
	Turmeric,
	Tomato,
	Spaghetti,
	Rice,
	PizzaCrust, 
	Ice,
	None
}

public struct CustomerProperty {
	public float fTimeReadMenu;
	public float fTimeWaitingFood;
	public float fTimeEatFood;
	public float fTimeAwayNewGen;
	public CustomerProperty (float fReadTime, float fWaitTime, float fEat, float fAway) {
		this.fTimeReadMenu 		= fReadTime;
		this.fTimeWaitingFood 	= fWaitTime;
		this.fTimeEatFood 		= fEat;
		this.fTimeAwayNewGen 	= fAway;
	}
}

public enum CustomerType {
	MalayMale,
	IndianMale,
	MalayFemale,
	ChineseFemale
}

public enum CustomerStatus {
	ReadingMenu,
	WaitingFood,
	EatingFood,
	Going,
	Gone
}

public enum UtensilCookingStatus {
	Empty,
	Cooking,
	CookDone,
	Burn
}

public enum PlayerPoseSituation {
	
	IdleHandsDown,
	IdleHandsUp,
	IdleRightHandUp,
	IdleLeftHandUp,

	WalkFrontHandsDown,
	WalkFrontHandsUp,
	WalkFrontRightHandUp,
	WalkFrontLeftHandUp,

	WalkLeftHandsDown,
	WalkLeftHandsUp,
	WalkLeftRightHandUp,
	WalkLeftLeftHandUp,

	WalkRightHandsDown,
	WalkRightHandsUp,
	WalkRightRightHandUp,
	WalkRightLeftHandUp,

	WalkBackHandsDown,
	WalkBackHandsUp,
	WalkBackRightHandUp,
	WalkBackLeftHandUp

}

//food material category with main materials such as rice and macaron and extra materials such as chocolate, 
public enum FoodMatCat {
	Main,
	Extra,
	PostProcess
}

public enum HandKind {
	Right,
	Left
}

public struct RoundInfomation {
	public int nLevelId;
	public int nRoundId;
	public int nTargetScore;
	public int nBonusPoint;
	public float fTimeDuration;
	public float fInterval;
	public List<string> menuList;
	public RoundInfomation(int nLevelId, int nRoundId, int nTargetScore, int nBonusPoint, float fTimeDuration, float fInterval) {
		this.nTargetScore = nTargetScore;
		this.nLevelId = nLevelId;
		this.nRoundId = nRoundId;
		this.nBonusPoint = nBonusPoint;
		this.fTimeDuration = fTimeDuration;
		this.fInterval = fInterval;
		menuList = new List<string> ();
	}
}





