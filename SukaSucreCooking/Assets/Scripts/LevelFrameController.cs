﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelFrameController : MonoBehaviour {
	public GameObject activePanel;
	public Animator animFrame;
	public Toggle toggleSelection;
	public GameObject starBackground;
	public GameObject[] stars;
	public GameObject imgLock;
	public Text txt_Round;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitLevelFrame(bool bActivatePanel, bool bAni, bool bToggle, bool bStarBack, bool bLock, int nStars, int nRound) {
		activePanel.SetActive (bActivatePanel);
		animFrame.enabled = bAni;
		toggleSelection.enabled = bToggle;
		starBackground.SetActive (bStarBack);
		imgLock.SetActive (bLock);
		for (int i = 0; i < nStars; i++) {
			stars[i].SetActive (true);
		}
		txt_Round.text = nRound + "";
	}



}
