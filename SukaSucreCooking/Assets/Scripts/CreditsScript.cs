﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScript : MonoBehaviour {

	public void ClickBackButton() {
		VoiceManager.Instance ().PlayButtonSound ();
		GameManager.Instance ().LoadLevelSceneFromCredits (gameObject);
	}
}
