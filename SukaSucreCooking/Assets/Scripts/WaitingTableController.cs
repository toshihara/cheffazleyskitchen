﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;
public class WaitingTableController : MonoBehaviour {
	public Transform target_pos;
	TargetMover targetMover;
	public string orderedFood = "";
	public Customer customer;
	public Image img_food;
	public int nTableIndex;
	// Use this for initialization
	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover>();
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnMouseUp() {

	}

	public void OnMouseDown() {
		if (!GameFlags.bTutorial) {
			targetMover.PushTargetPosition2List (target_pos.position);
			CharacterAniController.Instance ().destinationFurniture = null;
			CharacterAniController.Instance ().destinationFood = null;
			CharacterAniController.Instance ().destinationTable = this;
			CharacterAniController.Instance ().bReachDestination = false;

			CharacterAniController.Instance ().targetCategories.Add ("WaitingTable");
			CharacterAniController.Instance ().targetTableController.Add (this);
		} else {
			if (nTableIndex == 0 && TutorialManager.Instance ().nTutorialIndex == 5) {
				targetMover.PushTargetPosition2List (target_pos.position);
				CharacterAniController.Instance ().destinationFurniture = null;
				CharacterAniController.Instance ().destinationFood = null;
				CharacterAniController.Instance ().destinationTable = this;
				CharacterAniController.Instance ().bReachDestination = false;

				CharacterAniController.Instance ().targetCategories.Add ("WaitingTable");
				CharacterAniController.Instance ().targetTableController.Add (this);
				TutorialManager.Instance ().HideArrow ();
			}
		}
	}

	public bool CheckCustomerOrder() {
		if (customer != null) {
			if (customer.orderedFood != "" && customer.customerStatus == CustomerStatus.WaitingFood) {
				if (customer.orderedFood == CharacterAniController.Instance ().rightHand.foodName) {
					CharacterAniController.Instance ().rightHand.foodName = "";
					CharacterAniController.Instance ().rightHand.bHoldCook = false;
					customer.customerStatus = CustomerStatus.EatingFood;
					img_food.sprite = customer.orderedCookImage.sprite;
					img_food.gameObject.SetActive (true);
					customer.DeactivateWaitingTimeBar ();
					customer.dialogFood.SetActive (false);

					return true;
				}
				else if (customer.orderedFood == CharacterAniController.Instance ().leftHand.foodName) {
					CharacterAniController.Instance ().leftHand.foodName = "";
					CharacterAniController.Instance ().leftHand.bHoldCook = false;
					img_food.sprite = customer.orderedCookImage.sprite;
					img_food.gameObject.SetActive (true);
					customer.customerStatus = CustomerStatus.EatingFood;
					customer.DeactivateWaitingTimeBar ();
					customer.dialogFood.SetActive (false);
;
					return true;
				}
			}
		}
		return false;
	}

	public void OnMouseExit() {

	}

	public void HideEatingdish() {
		img_food.gameObject.SetActive (false);
	}
}
