﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using SimpleJSON;
using System;
using System.Text;
using System.IO;
using UnityEngine.UI;
using System.Net;

public class ApiHandler : MonoBehaviour {

	private bool isConnected;
	public InputField userName;
	public InputField password;
	public InputField rePassword;
	public InputField emailId;
	public GameObject loginFailed;
	public GameObject insertData;
	public GameObject mainPanel;
	public GameObject inputPanel;
	public GameObject loadingPanel;
	public Text errorText;

	public GameObject RankController;
	//public ProfileEditor profileEditor;
	public GameObject shopManager;
	//public DiaryChipManager diaryChipManager;
	public GameObject forgotPassword;
	// Use this for initialization
	public GameObject prefConnectionError;

	public static ApiHandler apiHandler;

	public static ApiHandler Instance() {
		if (!apiHandler) {
			apiHandler = FindObjectOfType (typeof (ApiHandler)) as ApiHandler;
		}
		return apiHandler;
	}

	void Start () {
	
	}

	public void LoginGameAccount(string user_email, string user_password) {
		StartCoroutine (sendingLoginData (user_email, user_password));
	}

	public void LoginPostData(){
		string uname = userName.text;
		string pass = password.text;
		PlayerPrefs.SetInt ("LoginSuccess",0);
		if (uname.Equals ("")) {
			//errorText.text = "Please Enter Username";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_USER_NAME_EMPTY");
			StartCoroutine (fillData ());
			return;
		} else if(pass.Equals ("")){
			//errorText.text = "Please Enter Password";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_PASS_EMPTY_ERR");
			StartCoroutine (fillData ());
			return;
		}

		Debug.Log ("Data fatched from inputFields userName = " + uname + " and password = " + pass);

		if (checkInternetConnection ()) {
			StartCoroutine (sendingLoginData (uname, pass));
		} else {
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_REQUEST_FAIL");
			StartCoroutine (fillData ());
		}
	}


	// POST DATA TO SERVER FOR REGISTRATION

	public void RegistrationPostData() {

		string uname = userName.text;
		string email = emailId.text;
		string pass = password.text;
		string rePass = rePassword.text;

		if (uname.Equals ("")) {
			//errorText.text = "UserName should not be empty";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_USER_NAME_EMPTY");
			StartCoroutine (fillData ());
			return;
		} else if (email.Equals ("")) {
			//errorText.text = "Email should not be empty";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_USER_NAME_EMPTY");
			StartCoroutine (fillData());
			return;
		} else if (pass.Equals ("")) {
			//errorText.text = "Password should not be empty";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_PASS_LENGTH_ERR");
			StartCoroutine (fillData());
			return;
		} else if (rePass.Equals ("")) {
			//errorText.text = "RePassword should not be empty";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_PASS_CONFIRM_ERR");
			StartCoroutine (fillData());
			return;
		} else if (!pass.Equals (rePass)) {
			//errorText.text = "Password doesn't match";
			//errorText.text = LocalizationManager.Instance().GetMessage("MSG_PASS_MATCH_ERR");
			StartCoroutine (fillData());
			return;
		}

		if (pass.Equals (rePass)) {
			StartCoroutine (sendingRegistrationData (uname, email, pass, 112));
		}

	}

	void ShowConnectionErrorMsg() {
		GameObject objConnectionErr = Instantiate (prefConnectionError) as GameObject;
		objConnectionErr.transform.parent = GameObject.Find ("Canvas").transform;
		objConnectionErr.transform.localScale = Vector3.one;
		objConnectionErr.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -200f);
		objConnectionErr.GetComponent<PopupSignError> ().Open ();
	}

	public void PostRegisterData(string username, string email, string password, int countryCode) {
		if (checkInternetConnection ()) {
			StartCoroutine (sendingRegistrationData (username, email, password, countryCode));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	public void PostUpdateProfileData(string username, string email, int countryCode) {
		if (checkInternetConnection ()) {
			StartCoroutine (SendProfileUpdate (username, email, countryCode));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	public void GetRankingData(string tabName) {
		if (checkInternetConnection ()) {
			StartCoroutine (RequireRankingData (tabName));
		} else {
			ShowConnectionErrorMsg ();
		}
	}
	// POST EMAIL ADDRESS TO SERVER TO SEND NEW PASSWORD

	public void ForgetPasswordPostData(string email){

		//string email = emailId.text;

		if (email.Equals ("")) {
			//errorText.text = "Email should not be empty";
			//forgotPassword.GetComponent<ForgotPassword>().ShowAlertDlg (4);
			return;
		}

//		if (!checkInternetConnection())
			//forgotPassword.GetComponent<ForgotPassword>().ShowAlertDlg (3);

		StartCoroutine (sendingForgetPasswordData (email));

	}

	private string CreateRequest_test(string postData, int url_kind=0) {
		string api_url = Config.API_URL;
		if (url_kind == 1)
			api_url = Config.API_RANK_URL;
		else if (url_kind == 2) {
			api_url = Config.API_VERSION_URL;
		} else if (url_kind == 3) {
			api_url = Config.API_EVENT_URL;
		}
		Debug.Log (postData);
		var request = (HttpWebRequest)WebRequest.Create (api_url);
		var data = Encoding.UTF8.GetBytes (postData);
		request.Method = "POST";
		request.ContentType = "application/x-www-form-urlencoded";
		request.ContentLength = data.Length;

		using (var stream = request.GetRequestStream ()) {
			stream.Write (data, 0, data.Length);
		}

		var response = (HttpWebResponse)request.GetResponse ();
		var responseString = "";

		responseString = new StreamReader (response.GetResponseStream ()).ReadToEnd ();
		Debug.Log ("<<<CreateRequest>>>");
		return responseString;
	}

	byte[] GetMultipartFormData(string boundary, byte[] bytes) {
		Stream formDataStream = new System.IO.MemoryStream ();
		bool needsCLRF = false;
		string userId = PlayerPrefs.GetInt ("user_id") + "";
		string postData1 = string.Format ("---{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
			boundary,
			"op",
			"UpdateImage"
		);
		formDataStream.Write (Encoding.UTF8.GetBytes(postData1), 0, Encoding.UTF8.GetByteCount(postData1));
		formDataStream.Write (Encoding.UTF8.GetBytes("\r\n"), 0, Encoding.UTF8.GetByteCount("\r\n"));
		string postData2 = string.Format ("---{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
			boundary,
			"user_id",
			userId
		);
		formDataStream.Write (Encoding.UTF8.GetBytes(postData2), 0, Encoding.UTF8.GetByteCount(postData2));
		formDataStream.Write (Encoding.UTF8.GetBytes("\r\n"), 0, Encoding.UTF8.GetByteCount("\r\n"));
		string header = string.Format ("---{0}\r\nContent-disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type:{3}\r\n\r\n",
			boundary,
			"file",
			"avatar.png",
			"image/png"
		);
		formDataStream.Write (Encoding.UTF8.GetBytes(header), 0, Encoding.UTF8.GetByteCount(header));
		formDataStream.Write (bytes, 0, bytes.Length);

		string footer = "\r\n--" + boundary + "--\r\n";
		formDataStream.Write (Encoding.UTF8.GetBytes(footer), 0, Encoding.UTF8.GetByteCount(footer));
		formDataStream.Position = 0;
		byte[] formData = new byte[formDataStream.Length];
		formDataStream.Read (formData, 0, formData.Length);
		formDataStream.Close ();

		return formData;
	}

	private string CreateMultiPartRequest(string api_url, byte[] bytes) {
		

		string formDataBoundary = String.Format ("----------{0:N}", Guid.NewGuid());

		byte[] formData = GetMultipartFormData(formDataBoundary, bytes);
		HttpWebRequest request = WebRequest.Create(Config.API_URL) as HttpWebRequest;
		request.Method = "POST";
		request.ContentType = "multipart/form-data; boundary=" + formDataBoundary;
		request.UserAgent = "gameplayer";
		request.CookieContainer = new CookieContainer ();
		request.ContentLength = formData.Length;

		using (Stream requestSteam = request.GetRequestStream ()) {
			requestSteam.Write (formData, 0, formData.Length);
			//requestSteam.Close ();
		}

		var response = (HttpWebResponse)request.GetResponse ();
		var responseString = "";

		responseString = new StreamReader (response.GetResponseStream ()).ReadToEnd ();
		Debug.Log ("<<<CreateMultipartFormRequest>>>");
		return responseString;
	}

	private string CreateRequest(string postData, int url_kind=0) {
		string api_url = Config.API_URL;
		if (url_kind == 1)
			api_url = Config.API_RANK_URL;
		else if (url_kind == 2) {
			api_url = Config.API_VERSION_URL;
		} else if (url_kind == 3) {
			api_url = Config.API_EVENT_URL;
		}
		Debug.Log (postData);
		var request = (HttpWebRequest)WebRequest.Create (api_url);
		var data = Encoding.UTF8.GetBytes (postData);
		request.Method = "POST";
		request.ContentType = "application/x-www-form-urlencoded";
		request.ContentLength = data.Length;

		using (var stream = request.GetRequestStream ()) {
			stream.Write (data, 0, data.Length);
		}

		var response = (HttpWebResponse)request.GetResponse ();
		var responseString = "";

		responseString = new StreamReader (response.GetResponseStream ()).ReadToEnd ();
		Debug.Log ("<<<CreateRequest>>>");
		return responseString;
	}

	private IEnumerator sendingForgetPasswordData(string email){
		Debug.Log ("----------- Under Processing for forgetPassword -----------");
		var postData = "op=ForgotPassword&email=" + email;
		var responseString = "";
		CreateRequest(postData);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try {
			int error = int.Parse( dict["error_code"]);
			//forgotPassword.GetComponent<ForgotPassword>().ShowAlertDlg(error);
		}catch {
			Debug.Log ("exception?");
		} 
	}


	private IEnumerator fillData(){
		yield return new WaitForSeconds (1f);
		insertData.SetActive (true);
	}

	public void hideErrorPanel(){
		insertData.SetActive (false);
	}

	public string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
					{
						//We are limiting the array to 80 so we don't have
						//to parse the entire html document feel free to 
						//adjust (probably stay under 300)
						char[] cs = new char[80];
						reader.Read(cs, 0, cs.Length);
						foreach(char ch in cs)
						{
							html +=ch;
						}
					}
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}

	bool checkInternetConnection(){
		string HtmlText = GetHtmlFromUri("http://bing.com");
		if (HtmlText == "") {
			return false;
		} else 
			return true;
	}


	private IEnumerator checkInternetConnection__Old(Action<bool> action ){

		WWW www = new WWW ("http://google.com");
		yield return www;

		if (www.error != null) {
			action (false);
		} else {
			action (true);
		}

	}

	private IEnumerator sendingLoginData(string email, string pass){
		var postData = "op=Login&email=" + email + "&password=" + pass;
		var responseString = "";
		responseString = CreateRequest (postData);

		Debug.Log ("Responce from server for login == " + responseString);
		yield return responseString;
		Debug.Log ("response string: " + responseString);
		var dict = JSON.Parse (responseString);

		//try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) { // login success

				int nUser_id = int.Parse(dict["data"]["user_id"]);
				string encPass = dict["data"]["password"] + "";
				int nCoin = int.Parse(dict["data"]["chips"]);
				int countryCode = int.Parse(dict["data"]["country"]);

				string user_email = dict["data"]["email"] + "";
				string user_name = dict["data"]["username"] + "";

				string strPurchasedItems = dict["data"]["purchased_items"] + "";
				int nAvailstages_1 = int.Parse(dict["data"]["level1_stage"]);
			string avatarURL = dict["data"]["image"] + "";
			if (avatarURL != "") {
				avatarURL = Config.IMG_URL + avatarURL;
				PlayerPrefs.SetString ("AvatarURL", avatarURL);
			}
//			if (nAvailstages_1 > 1) {
//				PlayerPrefs.SetInt("Tutorial", 1);
//			}
				//int achievedStage = 0;
//				if (dict["data"]["achieved_stage"] != null)
//					achievedStage = int.Parse(dict["data"]["achieved_stage"]);

				Debug.Log("encPass:" + encPass);
				PlayerPrefs.SetInt("user_id", nUser_id);
				PlayerPrefs.SetString("encPass", encPass);
				PlayerPrefs.SetInt("country", countryCode);

				PlayerPrefs.SetString ("userName", user_name);
				PlayerPrefs.SetString ("Email", user_email);
				PlayerPrefs.SetInt ("AvailRound", nAvailstages_1);
				//PlayerPrefs.SetInt("AvailRound", achievedStage);

				if (nCoin > 0)
					PlayerPrefs.SetInt("Coin", nCoin);

				string[] parsed_item_strings = strPurchasedItems.Split('_');
				int nOven = 2 + int.Parse(parsed_item_strings[0]);
				PlayerPrefs.SetInt("Oven", nOven);

				int nStove = 2 + int.Parse(parsed_item_strings[1]);
				PlayerPrefs.SetInt("Stove", nStove);

				int nWarmStation = 2 + int.Parse(parsed_item_strings[2]);
				PlayerPrefs.SetInt("WarmStation", nWarmStation);

				int nSpeedUp = int.Parse(parsed_item_strings[3]);
				PlayerPrefs.SetInt("SpeedUp", nSpeedUp);

				int noAds =int.Parse(parsed_item_strings[4]);
				PlayerPrefs.SetInt("Ads", noAds);

				LevelSceneManager.Instance().StoreAccountInfo(true);
				LevelSceneManager.Instance ().RefreshRoundsList (0);

			}
			else{
				LevelSceneManager.Instance().ShowErrorPopup(error);
			}

//		} catch {
//			Debug.Log ("Catch Okay?");
//			try{
//				int error = int.Parse(dict["error_code"]);
//				if(error == 1){
//					StartCoroutine(FailedTOLogin());
//				}
//			}catch(Exception e){
//				Debug.Log (e.Message);
//			}
//		} 
	}

	// get the current event information : 
	public void CheckEventInfo() {
		//StartCoroutine (GetEventInfo());
	}

//	private IEnumerator GetEventInfo(bool isLogin=true) {
//		Debug.Log ("---------------- Event Info -------------------");
//
//		var responseString = CreateRequest("op=Info", 3);
//
//		responseString = responseString.Replace ("\\n","\n");
//		yield return responseString;
//
//		var dict = JSON.Parse (responseString);
//		try{
//			if (int.Parse(dict["error_code"]) == 0) {
//				Event.EventId = int.Parse(dict["data"]["id"]);
//				Event.EventName = dict["data"]["event_name"];
//				Event.EventMessage = dict["data"]["message"];
//				Event.EventStart = dict["data"]["start_at"];
//				Event.EventEnd = dict["data"]["end_at"];
//				Event.EventRankNum = int.Parse(dict["data"]["rank_num"]);
//				Event.Is_Season = (int.Parse(dict["data"]["is_season"]) == 1) ? true : false;
//				if (isLogin) {
//					if (!Event.Is_Season) {
//						//GetComponent<LoadScene>().LoadGameScene();
//					}
//					else {
//						//LoginEventChecker.Instance().ShowEventPopUp();
//					}
//					//GetComponent<LoadScene>().LoadLoadingScene();
//				}
//				else
//				{
//					//GetComponent<LoadScene>().LoadGameScene();
//				}
//			}
//			else if (int.Parse(dict["error_code"]) == 2) {
//				Event.EventId = -1;
//				Event.EventName = "";
//				Event.EventMessage = "";
//				Event.EventStart = "";
//				Event.EventEnd = "";
//				Event.EventRankNum = 0;
//				Event.Is_Season = false;
//			}
//		} catch{
//
//		}
//	}

	public void GetEventRankList() {
		//StartCoroutine (RequestEventRankList());
	}

//	private IEnumerator RequestEventRankList() {
//		Debug.Log ("---------------- Event Info -------------------");
//		Event.UserRankList.Clear ();
//		var responseString = CreateRequest("op=Ranking", 3);
//		responseString = responseString.Replace ("\\n","\n");
//		yield return responseString;
//
//		var dict = JSON.Parse (responseString);
//		try{
//			if (int.Parse(dict["error_code"]) == 0) {
//				Event.EventId = int.Parse(dict["data"]["id"]);
//				Event.EventName = dict["data"]["event_name"];
//				Event.EventMessage = dict["data"]["message"];
//				Event.EventStart = dict["data"]["start_at"];
//				Event.EventEnd = dict["data"]["end_at"];
//				Event.EventRankNum = int.Parse(dict["data"]["rank_num"]);
//				Event.Is_Season = (int.Parse(dict["data"]["is_season"]) == 1) ? true : false;
//				JSONArray rankList = (JSONArray) dict["data"]["ranking"];
//				for (int i = 0; i < rankList.Count; i++) {
//					string id = ((JSONNode)rankList[i])["user_id"];
//					string userName = ((JSONNode)rankList[i])["username"];
//					int userRank = int.Parse(((JSONNode)rankList[i])["display_rank"]);
//					long userChip = long.Parse(((JSONNode)rankList[i])["chips"]);
//					Event.UserRankList.Add (new UserRankInfo(userRank, id, userName, userChip));
//				}
//
//			}
//			//EventRankScript.Instance().ReceiveInfoCounter(0);
//			//GetPersonalEventRank();
//		} catch{
//
//		}
//	}

	public void GetPersonalEventRank() {
//		StartCoroutine (RequestPersonalEventRank());
	}

//	private IEnumerator RequestPersonalEventRank() {
//		Debug.Log ("---------------- Event Info -------------------");
//		int myId = PlayerPrefs.GetInt ("user_id");
//		string query = "op=UserRank&user_id=" + myId;
//		var responseString = CreateRequest(query, 3);
//
//		yield return responseString;
//
//		var dict = JSON.Parse (responseString);
//		try{
//			if (int.Parse(dict["error_code"]) == 0) {
//				string rank = dict["data"]["rank"] + "";
//				if (rank != "null")
//					Event.EventMyRank = int.Parse(dict["data"]["rank"]);
//				Event.EventMyNextRank = long.Parse(dict["data"]["next"]);
//				Event.EventMyRankChips = long.Parse(dict["data"]["score"]);
//			}
//			//EventRankScript.Instance().ReceiveInfoCounter(1);
//		} catch{
//
//		}
//	}

	private IEnumerator sendingRegistrationData(string userName, string email, string password, int countryCode){
		Debug.Log ("----------- Under Processing for registration -----------");
		var postData = "op=Register&username=" + userName + "&email=" + email + "&password=" + password + "&country=" + countryCode;
		var responseString = "";
		responseString = CreateRequest (postData);
		Debug.Log ("Responce From server for registration == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		Debug.Log ("dict: " + dict);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				Debug.Log ("Registration Successful.");

				int nUser_id = int.Parse(dict["data"]["user_id"]);
				string encPass = dict["data"]["encPass"] + "";
				int nCoin = int.Parse(dict["data"]["chips"]);


				Debug.Log("encPass:" + encPass);
				if (PlayerPrefs.HasKey("user_id")) {
					PlayerPrefs.SetInt ("AvailRound", 1);
					PlayerPrefs.SetInt("Oven", 2);
					PlayerPrefs.SetInt("Stove", 2);
					PlayerPrefs.SetInt("WarmStation", 2);
					PlayerPrefs.SetInt("SpeedUp", 0);
					PlayerPrefs.SetInt("Ads", 0);
					PlayerPrefs.SetInt("Coin", 0);
					PlayerPrefs.SetString ("FbShare", "");
				}
				PlayerPrefs.SetInt("user_id", nUser_id);
				PlayerPrefs.SetString("encPass", encPass);
				PlayerPrefs.SetInt("country", countryCode);
				PlayerPrefs.SetString ("userName", userName);
				PlayerPrefs.SetString ("Email", email);

				if (nCoin > 0)
					PlayerPrefs.SetInt("Coin", nCoin);

				///
//				if (dict["data"].Count > 3)
//					GameFlags.bFBLogin = false;

				if (dict["data"].Count > 3) {
					GameFlags.bFBLogin = false;
					string strPurchasedItems = dict["data"]["purchased_items"] + "";
					int nAvailstages_1 = int.Parse(dict["data"]["level1_stage"]);

					PlayerPrefs.SetInt ("AvailRound", nAvailstages_1);
					if (nAvailstages_1 > 1)
						PlayerPrefs.SetInt("Tutorial", 1);

					string[] parsed_item_strings = strPurchasedItems.Split('_');
					int nOven = 2 + int.Parse(parsed_item_strings[0]);
					PlayerPrefs.SetInt("Oven", nOven);

					int nStove = 2 + int.Parse(parsed_item_strings[1]);
					PlayerPrefs.SetInt("Stove", nStove);

					int nWarmStation = 2 + int.Parse(parsed_item_strings[2]);
					PlayerPrefs.SetInt("WarmStation", nWarmStation);

					int nSpeedUp = int.Parse(parsed_item_strings[3]);
					PlayerPrefs.SetInt("SpeedUp", nSpeedUp);

					int noAds =int.Parse(parsed_item_strings[4]);
					PlayerPrefs.SetInt("Ads", noAds);

					LevelSceneManager.Instance().StoreAccountInfo(true);
					//LevelSceneManager.Instance ().RefreshRoundsList (0);

				} else {
					if (PlayerPrefs.GetInt ("SpeedUp", 0) == 1) {
						ApiHandler.Instance ().LogPurchaseResult ("chefspeed2x", 1);
					}
					if (PlayerPrefs.GetInt("Ads", 0) == 1) {
						ApiHandler.Instance ().LogPurchaseResult ("removeads1", 1);
					}
					if (PlayerPrefs.GetInt("Oven", 2) > 2)
						ApiHandler.Instance ().LogPurchaseResult ("oven", PlayerPrefs.GetInt("Oven", 0) - 2);
					if (PlayerPrefs.GetInt("Stove", 2) > 2)
						ApiHandler.Instance ().LogPurchaseResult ("stove", PlayerPrefs.GetInt("Stove", 0) - 2);
					if (PlayerPrefs.GetInt("WarmStation", 2) > 2)
						ApiHandler.Instance ().LogPurchaseResult ("warmstation", PlayerPrefs.GetInt("WarmStation", 0) - 2);
					ApiHandler.Instance().SendStageAchievement(1, PlayerPrefs.GetInt("AvailRound", 1));
					if (PlayerPrefs.GetInt("Coin", 0) > 0) {
						ApiHandler.Instance ().SendGameLog ((long)(PlayerPrefs.GetInt("Coin", 0)), 1, 0);
					}
					LevelSceneManager.Instance().StoreAccountInfo(false);

				}
				LevelSceneManager.Instance ().RefreshRoundsList (0);

				////////////////
			} 
			else {
				string lang = PlayerPrefs.GetString ("DefaultLanguage","en");
				if (error == 1) {
					//insertData.SetActive(true);
					//errorText.text = LocalizationManager.Instance().GetMessage("MSG_REGISTER_FAIL");

				}
				else if (error == 2) {
					//insertData.SetActive(true);
					//errorText.text = LocalizationManager.Instance().GetMessage("MSG_USER_EXIST");
					SignUpPopUP.Instance().ShowErrorPopup(2);
				}	
				else if (error == 3){
					//insertData.SetActive(true);
					//errorText.text = LocalizationManager.Instance().GetMessage("MSG_EMAIL_EXIST");
					SignUpPopUP.Instance().ShowErrorPopup(3);
				}
				else if (error == 4){
					//insertData.SetActive(true);
					//errorText.text = LocalizationManager.Instance().GetMessage("MSG_INVALID_EMAIL");
					SignUpPopUP.Instance().ShowErrorPopup(4);
				}
				else if (error == 5){
					//errorText.text = LocalizationManager.Instance().GetMessage("MSG_PASS_LENGTH_ERR");
				}
				LevelSceneManager.Instance().HandleRegisterResult(error);
				Debug.Log("failed register");
			}

		}catch {
			try{
				int error = int.Parse(dict["error_code"]);
				if(error == 1){
					Debug.Log ("Registration failed check credentials and try to register again !!!");
				}
			}catch(Exception e){
				Debug.Log (e.Message);
			}
		}
	}

	//used for request the ranking data and fetch the info:
	private IEnumerator RequireRankingData(string tabName){
		Debug.Log ("----------- Under Processing for Fetching Ranking Data of tabName -----------");

		DateTime today = DateTime.Today;
		int year = today.Year;
		int month = today.Month;
		int day = today.Day;

		var postData = "op=Get&tab=" + tabName + "&year=" + year + "&month=" + month + "&day=" + day;
		var responseString = "";
		responseString = CreateRequest(postData, 1);
		Debug.Log ("Responce From server for Ranking Data == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				int nCountRank = dict["data"]["ranking"].Count;
				RankItemInfo[] rankItems = new RankItemInfo[nCountRank];
				for(int i = 0; i < nCountRank; i++) {
					string sRankNum = dict["data"]["ranking"][i]["display_rank"].Value;
					string sUserName = dict["data"]["ranking"][i]["username"].Value;
					string sCountry = dict["data"]["ranking"][i]["country"].Value;
					string sAvatar = dict["data"]["ranking"][i]["image"].Value;
					string sChip = dict["data"]["ranking"][i]["chips"].Value;
					rankItems[i] = new RankItemInfo(sRankNum, sUserName, sAvatar, sCountry, sChip);
				}

				RankingPopupHandler.Instance().DisplayRankingLists(rankItems);
			} 
		}catch{
			try{
				int error = int.Parse(dict["error_code"]);
				if(error == 1){
					Debug.Log ("Registration failed check credentials and try to register again !!!");
				}
			}catch(Exception e){
				Debug.Log (e.Message);
			}
		}
	}

	public void CheckVersion() {
		if (checkInternetConnection ()) {
			StartCoroutine (GetVersionInfo ());
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private IEnumerator GetVersionInfo() {
		Debug.Log ("---------------- Check Version -------------------");

		var responseString = CreateRequest("", 2);

		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			string gameVersion = dict["version"];
			string currentVersion = Config.version;
			if (gameVersion != currentVersion)
			{
				//SplashScene.Instance().ShowUpdateVersion(gameVersion);
			}
//			else
//				SplashScene.Instance().Go2Main();
		} catch{

		}
	}

	public void UpdateEmail(string email){
		//if (checkInternetConnection())
		if (checkInternetConnection ()) {
			StartCoroutine (RequireUpdateEmail (email));
		} else {
			ShowConnectionErrorMsg ();
		}
		//else 
			//profileEditor.ReceiveUpdateEmail(1);
	}

	private IEnumerator RequireUpdateEmail(string email) {
		Debug.Log ("----------- Under Processing for Fetching Ranking Data of tabName -----------");
		var postData = "op=UpdateEmail&user_id=" + PlayerPrefs.GetInt("user_id") + "&email=" + email;
		var responseString = "";
		responseString = CreateRequest (postData);
		Debug.Log ("Responce From server for Update Email == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			IVPickerExample.Instance().HandleUpdateEmail(error);
			//IVPickerExample.Instance().
			//profileEditor.ReceiveUpdateEmail(error);
		} catch{
			
		}
	}

	public void ChangePassword(string oldPw, string newPw){
		if (checkInternetConnection())
			StartCoroutine (RequireChangePassword(oldPw, newPw));
	//	else 
			//profileEditor.ReceiveChangePassword(1);
	}

	private IEnumerator RequireChangePassword(string oldPw, string newPw) {
		Debug.Log ("----------- Under Processing for Fetching Ranking Data of tabName -----------");
		var postData = "op=UpdatePassword&user_id=" + PlayerPrefs.GetInt("user_id") + "&password=" + newPw + "&current_password=" + oldPw;
		var responseString = "";
		responseString = CreateRequest (postData);
		Debug.Log ("Responce From server for Change Password == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);

		try {
			int error = int.Parse(dict["error_code"]);
			//profileEditor.ReceiveChangePassword(error);
		} catch {
			try {
				int error = int.Parse(dict["error_code"]);
				//profileEditor.ReceiveChangePassword(error);

				if(error == 1){
					Debug.Log ("Registration failed check credentials and try to register again !!!");
				}
			} catch(Exception e) {
				Debug.Log (e.Message);
			}
		}
	}

	public void DeleteAccount(){
		if (checkInternetConnection())
			StartCoroutine (RequireDeleteAccount());
//		else
//			profileEditor.ReceiveDeleteAccount(1);
	}

	private IEnumerator RequireDeleteAccount() {
		Debug.Log ("----------- Under Processing for Fetching Ranking Data of tabName -----------");
		var postData = "op=Delete&user_id=" + PlayerPrefs.GetInt("user_id");
		var responseString = "";

		responseString = CreateRequest(postData);
		Debug.Log ("Responce From server for Delete account == " + responseString);
		yield return responseString;
		var dict = JSON.Parse (responseString);

		try{
			int error = int.Parse(dict["error_code"]);
			//profileEditor.ReceiveDeleteAccount(error);
		}catch{
			
		}
	}

	public void AddChipLogGame(int nChargedChip){
		if (checkInternetConnection ())
			StartCoroutine (RequireAddChipLogGame (nChargedChip, false));
		else {
			StoreChipLocalStorage (nChargedChip);
			ShowConnectionErrorMsg ();
		}			
	}

	public void SendGameLog(long lChip, int win, int lose, bool bNew=false) {
		if (checkInternetConnection ()) { 
			StartCoroutine (PostGameLog (lChip, win, lose, bNew));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private void StoreChipLocalStorage(int nChip){
		if (!PlayerPrefs.HasKey ("gamelogchip")) {
			PlayerPrefs.SetInt ("gamelogchip", nChip);
		} else {
			int nChipAccumulate = PlayerPrefs.GetInt ("gamelogchip") + nChip;
			PlayerPrefs.SetInt ("gamelogchip", nChipAccumulate);
		}
	}

	private void SetLocalChipEmpty(){
		PlayerPrefs.SetInt ("gamelogchip", 0);
	}

	public void SendStageAchievement(int level, int stage) {
		if (checkInternetConnection ()) { 
			StartCoroutine (PostStageAchievement (level, stage));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private IEnumerator PostStageAchievement(int level, int stage) {
		Int32 unixTimeStamp = (Int32)(DateTime.UtcNow.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;
		string key2 = unixTimeStamp + "";
		string combinationKey = PlayerPrefs.GetInt ("user_id") + "StageAchieve" + key2;
		string key1 = MD5Sum (combinationKey);

		var postData = "op=StageAchieve&user_id=" + PlayerPrefs.GetInt("user_id") + "&level=" + level + "&stage=" + stage + "&key1=" + key1 + "&key2=" + key2;
		var responseString = "";
		responseString = CreateRequest(postData);

		Debug.Log ("Response From server for stage Achieve == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				Debug.Log("--stage Achieve added to server!---");
			}
			else{
				Debug.Log("---stage Achieve Error!---");
			}
		} catch {

		}
	}

	public void LogPurchaseResult(string purchased_item, int count) {
		if (checkInternetConnection ()) {
			StartCoroutine (PostPurchaseRequest (purchased_item, count));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private IEnumerator PostPurchaseRequest(string purchased_item, int count) {
		Int32 unixTimeStamp = (Int32)(DateTime.UtcNow.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;
		string key2 = unixTimeStamp + "";
		string combinationKey = PlayerPrefs.GetInt ("user_id") + "Purchase" + key2;
		string key1 = MD5Sum (combinationKey);

		var postData = "op=Purchase&user_id=" + PlayerPrefs.GetInt("user_id") + "&item=" + purchased_item + "&count=" + count + "&key1=" + key1 + "&key2=" + key2;
		var responseString = "";
		Debug.Log (postData);
		responseString = CreateRequest(postData);

		Debug.Log ("Response From server for Purchase Item == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				Debug.Log("---Purchase log added to server!---");
			}
			else{
				Debug.Log("---Purchase log Error!---");
			}
		} catch {

		}

	}

	private string MD5Sum(string strToEncrypt) {
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ();
		byte[] bytes = ue.GetBytes (strToEncrypt);
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ();
		byte[] hashBytes = md5.ComputeHash (bytes);
		string hashString = "";
		for (int i = 0; i < hashBytes.Length; i++) {
			hashString += System.Convert.ToString (hashBytes[i], 16).PadLeft(2, '0');
		}
		return hashString.PadLeft (32, '0');
	}

	private IEnumerator RequireAddChipLogGame(int nChargedChip, bool bLocalChip) {
		Debug.Log ("----------- Under Processing for Adding Chip Log of game Data of tabName -----------");
		string gameLogDate = "";
		DateTime d = DateTime.Today;
		gameLogDate = d.Year + "-" + d.Month + "-" + d.Day;
		var postData = "op=AddChipLog_Game&user_id=" + PlayerPrefs.GetInt("user_id") + "&charge=" + nChargedChip + "&device_token=test98765" + "&game_log_date=" + gameLogDate;
		var responseString = "";
		responseString = CreateRequest(postData);

		Debug.Log ("Response From server for Add Chip Login == " + responseString);
		yield return responseString;
	
		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				Debug.Log("---Game Log added to server!---");
				if (bLocalChip)
					SetLocalChipEmpty();
				else
					StoreChipLocalStorage(nChargedChip);
			}
			else{
				if (!bLocalChip)
					StoreChipLocalStorage(nChargedChip);
			}
		} catch {
			
		}
	}

	private IEnumerator SendProfileUpdate(string username, string email, int countryId) {
		var postData = "op=UpdateProfile&user_id=" + PlayerPrefs.GetInt ("user_id") + "&username=" + username + "&email="
		               + email + "&newcountrycode=" + countryId;
		GameFlags.email = email;
		var responseString = "";
		responseString = CreateRequest(postData);

		Debug.Log ("Response From server for profile update == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		int error = int.Parse(dict["error_code"]);
		try{
			
			if (error == 0) {
				Debug.Log("---profile updated to server!---");
				PlayerPrefs.SetString ("Email", email);
				PlayerPrefs.SetString ("userName", username);
				PlayerPrefs.SetInt("country", countryId);
			}
			else{
				Debug.Log("Error Game Log posting.");
			}
		} catch {

		}
		IVPickerExample.Instance().HandleUpdateProfile(error);
	}

	private IEnumerator PostGameLog(long lChip, int win, int lose, bool bNew=false) {
		Debug.Log ("----------- Under Processing for Game Log of result -----------");
		string gameLogDate = "";
		DateTime d = DateTime.Today;
		gameLogDate = d.Year + "-" + d.Month + "-" + d.Day;
		var postData = "op=AddChipLog_Game&user_id=" + PlayerPrefs.GetInt("user_id") + "&charge=" + lChip + "&win=" + win + "&lose=" + lose + "&game_log_date=" + gameLogDate;
		if (win == 0 && lose == 0) {
			if (lChip > 0)
				postData = "op=AddChipLog_Daily&user_id=" + PlayerPrefs.GetInt ("user_id") + "&charge=" + lChip + "&game_log_date=" + gameLogDate;
			else
				postData = "op=AddChipLog_Purchase&user_id=" + PlayerPrefs.GetInt ("user_id") + "&charge=" + lChip + "&game_log_date=" + gameLogDate;
		}
		var responseString = "";
		responseString = CreateRequest(postData);

		Debug.Log ("Response From server for Add Chip Login == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);
		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				Debug.Log("---Game Log added to server!---");
				if (win == 0 && lose == 0) {
					GetPersonalGameRank();
				}
			}
			else{
				Debug.Log("Error Game Log posting.");
			}
		} catch {

		}
	}

	public void GetPersonalGameRank() {
		//if (checkInternetConnection ())
		if (checkInternetConnection ()) {
			StartCoroutine (RequireGetPersonalGameRank ());
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private IEnumerator RequireGetPersonalGameRank() {
		Debug.Log ("----------- Under Processing for Getting personal Rank info -----------");

		string gameLogDate = "";
		var postData = "op=GetGameChipRank&user_id=" + PlayerPrefs.GetInt("user_id");
		var responseString = "";
		responseString = CreateRequest(postData);
		Debug.Log ("Responce From server for Get Personal Game Rank == " + responseString);
		yield return responseString;
		var dict = JSON.Parse (responseString);

		try{
			//int success = int.Parse( dict["success"]);
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				//profileEditor.DisplayPersonalRankDetails(dict);
				string strWeekRank = dict["data"]["three_month"]["rank"].Value;
				if (strWeekRank.Contains("rank"))
					strWeekRank = "";
				LevelSceneManager.Instance().txt_personal_rank.text = "" + strWeekRank;	
			}
		}catch{

		}
	}
		

	public void GetDiaryChip(int nKind) {
		if (checkInternetConnection ()) {
			StartCoroutine (RequireGetDiaryChip (nKind));
		} else {
			ShowConnectionErrorMsg ();
		}
	}

	private IEnumerator RequireGetDiaryChip(int nKind) {
		Debug.Log ("----------- Under Processing for Adding Chip Log of game Data of tabName -----------");
		string gameLogDate = "";
		var postData = "op=GetDailyChip&user_id=" + PlayerPrefs.GetInt("user_id");
		var responseString = "";
		responseString = CreateRequest(postData);
		Debug.Log ("Responce From server for Get Dairy chip == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);

		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				int nFreeCoin = int.Parse(dict["data"]["daily_chip"]);
				if (nKind == 0) {
//					shopManager.GetComponent<ShopManager>().HandleFreeCharge(nFreeCoin);
//					shopManager.GetComponent<ShopManager>().ShowDiaryDlg(error, nFreeCoin);
				}
//				else if (nKind == 1)
					//diaryChipManager.HandleFreeCharge(error, nFreeCoin);
			}
			else{
				if (nKind == 0) {
//					shopManager.GetComponent<ShopManager>().DisableFreeChargeBtn();
//					shopManager.GetComponent<ShopManager>().ShowDiaryDlg(error, 0);
				}
//				else if (nKind == 1)
					//diaryChipManager.HandleFreeCharge(error, 0);
			}

		}catch{
			
		}
	}


	public void UploadImage_old(string localPath) {
		//profileEditor.ShowLoadCircleAnimation ();
		//if (checkInternetConnection ())
		if (checkInternetConnection ()) {
			StartCoroutine (RequireUploadImage_old (localPath));
		} else {
			ShowConnectionErrorMsg ();
		}
		//else {
			//profileEditor.DisplayErrorDialog (1);
		//}
	}

	private IEnumerator RequireUploadImage_old(string localPath) {
		Debug.Log ("----------- Under Processing for UPloading Image -----------");

		WWW localFile = new WWW("file:///" + localPath);
		yield return localFile;
		if (localFile.error == null) {
			Debug.Log ("loaded file successfully");
		} else {
			Debug.Log ("OPen file Error");
			//profileEditor.DisplayErrorDialog (3);
			yield break;
		}
		string[] splitFileName = localPath.Split ('.');

		string fileUploadName = PlayerPrefs.GetInt ("user_id") + "." + splitFileName[1];
		WWWForm form = new WWWForm();
		form.AddField ("op", "UpdateImage");
		form.AddField ("user_id", PlayerPrefs.GetInt("user_id"));
		form.AddBinaryData ("file", localFile.bytes, "screenshot.png", "image/png");

		LevelSceneManager.Instance ().ShowDbgLog ("local path: " + localPath);
		LevelSceneManager.Instance ().ShowDbgLog ("file Name: " + fileUploadName);
		LevelSceneManager.Instance ().ShowDbgLog ("file size: " + localFile.bytes.Length);

		WWW w = new WWW (Config.API_URL, form);
		yield return w;
		if (!string.IsNullOrEmpty (w.error)) {
			LevelSceneManager.Instance ().ShowDbgLog ("Upload error: " + w.error);
		} else {
			LevelSceneManager.Instance ().ShowDbgLog ("Upload success:");
		}
//		WWW fileUpload = new WWW(Config.API_URL, form);
//		yield return fileUpload;
//		Debug.Log (fileUpload.text);
//		LevelSceneManager.Instance ().ShowDbgLog (fileUpload.text);
//		var dict = JSON.Parse (fileUpload.text);
//
//		if (fileUpload.error == null) {
//			Debug.Log (dict ["error_code"]);
//			int error_code = int.Parse (dict ["error_code"]);
//			if (error_code == 0) {
//				string imageName = dict ["data"] ["image"];
//				
//			} else {
//				Debug.Log ("Upload error");
//
//			}
//				
//		} else {
//			Debug.Log ("UPload error");
//
//		}
	}
		

	public void UploadImage(Texture2D tex) {
		//profileEditor.ShowLoadCircleAnimation ();
		//if (checkInternetConnection ())
		if (checkInternetConnection ()) {
			StartCoroutine (RequireUploadImage (tex));
			//StartCoroutine(ReqireUploadImageMultipart(tex));
		} else {
			ShowConnectionErrorMsg ();
		}
		//else {
		//profileEditor.DisplayErrorDialog (1);
		//}
	}
		
//	private IEnumerator RequestUploadImage_test(Texture2D tex) {
//		byte[] bytes = tex.EncodeToPNG ();
//
//	}

	private IEnumerator ReqireUploadImageMultipart(Texture2D tex) {
		byte[] bytes = tex.EncodeToPNG ();

		var responseString = "";
		responseString = CreateMultiPartRequest(Config.API_URL, bytes);
		Debug.Log ("Responce From server for Get Dairy chip == " + responseString);
		yield return responseString;

		var dict = JSON.Parse (responseString);

	}

	private IEnumerator RequireUploadImage(Texture2D tex) {
		Debug.Log ("----------- Under Processing for UPloading Image -----------");
		byte[] bytes = tex.EncodeToPNG ();
		byte[] bFile = File.ReadAllBytes ("c:/test.png");
		WWWForm form = new WWWForm();
		form.AddField ("op", "UpdateImage");
		form.AddField ("user_id", PlayerPrefs.GetInt("user_id"));
		form.AddBinaryData ("file", bFile, "test.png", "text/plain");

		string uploadURL = "https://kometdigital.com/games/chef/backend/user/index.php";
		UnityWebRequest request = UnityWebRequest.Post (Config.API_URL, form);
		yield return request.Send ();
		if (request.isError) {
			Debug.Log (request.error);
		} else {
			Debug.Log (request.responseCode);
		}

//		WWW fileUpload = new WWW(uploadURL, form);
//		yield return fileUpload;
//		Debug.Log ("upload return: " + fileUpload.text);
//		LevelSceneManager.Instance ().ShowDbgLog (fileUpload.text);
//		var dict = JSON.Parse (fileUpload.text);
//
//		if (fileUpload.error == null) {
//			Debug.Log (dict ["error_code"]);
//			int error_code = int.Parse (dict ["error_code"]);
//			if (error_code == 0) {
//				string imageName = dict ["data"] ["image"];
//				
//			} else {
//				Debug.Log ("Upload error");
//
//			}
//				
//		} else {
//			Debug.Log ("UPload error");
//		}
		
	}

	public void GetChip() {
		if (checkInternetConnection ())
			StartCoroutine (RequireGetChip ());
		else
			ShowConnectionErrorMsg ();
	}

	private IEnumerator RequireGetChip() {
		Debug.Log ("----------- Under Processing of Getting chip for user -----------");
		//string gameLogDate = "";
		var postData = "op=GetChip&user_id=" + PlayerPrefs.GetInt("user_id");
		var responseString = "";
		responseString = CreateRequest(postData);
		Debug.Log ("Responce From server for GetChip == " + responseString);
		var dict = JSON.Parse (responseString);
		Debug.Log ("GetChip error code: " + int.Parse(dict["error_code"]));
		try{
			//int success = int.Parse( dict["success"]);
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
				PlayerPrefs.SetString("TotalCoins", dict["data"]["chips"]);
				Debug.Log("--- > totalCoins :" + dict["data"]["chips"]);
			}
			else{
				Debug.Log("--- Failed to fetch the chip info from the server ---");
			}

		}catch{
			
		}
		yield return responseString;
	}

	public void UpdateChip(long chipAmount) {
		//PlayerPrefs.SetInt ("UpdateChip", 1);
		if (checkInternetConnection ())
			StartCoroutine (RequireUpdateChip (chipAmount));
		else
			ShowConnectionErrorMsg ();
	}

	private IEnumerator RequireUpdateChip(long chipAmount) {
		Debug.Log ("----------- Under Processing of Updating chip for user -----------");
		var postData = "op=UpdateChip&user_id=" + PlayerPrefs.GetInt("user_id") + "&password=" + PlayerPrefs.GetString("encPass") + "&chips=" + chipAmount;
		var responseString = "";

		responseString = CreateRequest (postData);
		Debug.Log ("Responce From server for UPdateChip == " + responseString);
		var dict = JSON.Parse (responseString);

		try{
			int error = int.Parse(dict["error_code"]);
			if (error == 0) {
//				PlayerPrefs.SetInt ("UpdateChip", 0);
				Debug.Log("--- Update User Chip Succcess---");

			}
			else{
				Debug.Log("--- Update User Chip Failed---");
			}

		}catch{
			
		}
		yield return responseString;
	}
		
	private IEnumerator FailedTOLogin(){
		yield return new WaitForSeconds (1.0f);
		loadingPanel.SetActive (false);
		loginFailed.SetActive (true);
		yield return new WaitForSeconds (2.5f);
		loginFailed.SetActive (false);
		Application.LoadLevel (Application.loadedLevel);
	}

	// Update is called once per frame
	void Update () {
	
	}
}//ApiHandler