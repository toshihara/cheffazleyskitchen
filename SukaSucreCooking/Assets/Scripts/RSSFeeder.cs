﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine.UI;
using System;
using Ricimi;

public class RSSFeeder : MonoBehaviour {

	private GameObject ItemTitleText;
	private GameObject ItemDescriptionText;
	private GameObject ArticleImage;

	// Synchronized arrays of things that may change via RSS.
	private RSSContentItem[] RSSItems;
	public Dropdown dropdown;
	public GameObject prefRSSPopup;
	private Transform mCanvas;
	// Tells us what spot to go to in our current set of arrays.
	private int CurrentSitePlacement;
	private int CurrentItemPlacement;
	private List<string> RSSTitles = new List<string>();
	// Our list of URLS, set from the component.
	public string[] URLs;

	#region Initialization & Setup
	void Awake() {
		// Declare GameObjects.
//		ItemTitleText = this.transform.Find("ArticleTitlePanel").Find ("TitleText").gameObject;
//		ItemDescriptionText = this.transform.Find("DescriptionPanel").Find ("DescriptionText").gameObject;
//		ArticleImage = this.transform.Find ("ArticleImage").gameObject;
	}

	void Start() {
		mCanvas = GameObject.Find ("Canvas").transform;
		CurrentSitePlacement = 0;
		StartCoroutine (GetXMLNodes ());

	}
	#endregion

	#region Changing on-screen text
	/// <summary>
	/// Changes our current displayed text as appropriate, making sure we properly save our place.
	/// </summary>
	private void ChangeAndDisplayText (int currentItem) {
		// Make sure we aren't looking for other images, hide the blank image unless we do.
		StopAllCoroutines ();
		//ArticleImage.SetActive (false);

		// Set our current place and get all applicable items.
		CurrentItemPlacement = currentItem;
		//ItemTitleText.GetComponent<Text> ().text = RSSItems[CurrentItemPlacement].Title;
		//ItemDescriptionText.GetComponent<Text> ().text = RSSItems[CurrentItemPlacement].Description;
		Debug.Log("RSS: " + RSSItems[CurrentItemPlacement]);
		if (RSSItems[CurrentItemPlacement].Image != "") {
			if (RSSItems[CurrentItemPlacement].Image.Contains("youtube")) {
				StartCoroutine (LoadCurrentImage (RSSItems[CurrentItemPlacement].Image));
			} else {
				StartCoroutine (LoadCurrentImage (RSSItems[CurrentItemPlacement].Image));
			}
		}
	}

	/// <summary>
	/// Loads our current image if appropriate.
	/// </summary>
	private IEnumerator LoadCurrentImage (string imageText) {
		Debug.Log ("Image Loading");
		WWW imageLink = new WWW (imageText);
		yield return imageLink;
		Debug.Log ("Image Loaded");
//		ArticleImage.SetActive (true);
//		ArticleImage.GetComponent<RawImage> ().texture = imageLink.texture;
	}

	/// <summary>
	/// Loads our current video if appropriate.  Not currently in use, but can be set up should we wish to use it.
	/// </summary>
	//	private IEnumerator LoadCurrentVideo(string imageText) {
	//		Debug.Log ("Image Loading");
	//		WWW imageLink = new WWW (imageText);
	//		yield return imageLink;
	//		Debug.Log ("Image Loaded");
	//		ArticleImage.SetActive (true);
	//		ArticleImage.GetComponent<RawImage> ().texture = imageLink.movie;
	//	}
	#endregion

	/// <summary>
	/// The Co-Routine used for gathering and displaying our news.
	/// </summary>
	public IEnumerator GetXMLNodes() {
		// Cover up our news panel with a loading panel while we work our magic.
		//		this.transform.FindChild ("LoadingPanel").gameObject.SetActive (true);

		// Reset Current Placement.
		CurrentItemPlacement = 0;
		string feedDate = PlayerPrefs.GetString ("RSS", "");
		string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");

		if (feedDate != currentDate || GameFlags.RSSItems.Count == 0) {
			// Create WWW Object via URL
			WWW xmlLink = new WWW (URLs [CurrentSitePlacement]);
			yield return xmlLink;

			// Wait to continue until we are done loading.
			if (xmlLink.isDone) {
				// Create a local instance of our XML Document
				XmlDocument currentXMLDoc = new XmlDocument ();

				// Load the entire XML document from our xmlLink WWW String
				try {
					currentXMLDoc.LoadXml (xmlLink.text);
				} catch {
					try {
						System.IO.StringReader stringReader = new System.IO.StringReader (xmlLink.text);
						stringReader.Read (); // skip BOM 
						currentXMLDoc.LoadXml (stringReader.ReadToEnd ());
					} catch {
						Debug.Log ("XML Not Loading.");
					}
				}

				// Use this one if the one above doesn't work right. Skips the initial BOM character.


				// Set our Site Title, so we know where we are.
				//this.transform.Find("SiteTitlePanel").Find("SiteTitleText").GetComponent<Text>().text = currentXMLDoc.SelectSingleNode ("rss/channel/title").InnerText;

				// Get each Item Node, add them to an XML Node List.
				XmlNodeList xmlNodeList = currentXMLDoc.SelectNodes ("rss/channel/item");

				// Make sure that we have the same amount of RSS items as we have items in our XML Node List.

				// Break apart each XML Node and add it to our RSS Items List.
				for (int i = 0; i < xmlNodeList.Count; i++) {
					RSSContentItem tempItem = new RSSContentItem ();
					tempItem.Title = xmlNodeList [i].SelectSingleNode ("title").InnerText;
					tempItem.Image = "";
					if ((xmlNodeList [i].SelectSingleNode ("description") != null) && (xmlNodeList [i].SelectSingleNode ("description").InnerText != null)) {
						tempItem.Description = xmlNodeList [i].SelectSingleNode ("description").InnerText;
					} else {
						tempItem.Description = "";
					}
					if ((xmlNodeList [i].SelectSingleNode ("url") != null) && (xmlNodeList [i].SelectSingleNode ("url").InnerText != null)) {
						tempItem.Link = xmlNodeList [i].SelectSingleNode ("url").InnerText;
					} else {
						tempItem.Link = "";
					}
					if ((xmlNodeList [i].SelectSingleNode ("image") != null) && (xmlNodeList [i].SelectSingleNode ("image").InnerText != null)) {
						tempItem.Image = xmlNodeList [i].SelectSingleNode ("image").InnerText;
					} else {
						tempItem.Image = "";
					}
					if ((xmlNodeList [i].SelectSingleNode ("buttonlabel") != null) && (xmlNodeList [i].SelectSingleNode ("buttonlabel").InnerText != null)) {
						tempItem.ButtonLabel = xmlNodeList [i].SelectSingleNode ("buttonlabel").InnerText;
					} else {
						tempItem.ButtonLabel = "";
					}

					// Look to see if we can find the content:encoded section.  Really don't like this implementation, but isn't coming up with SelectSingleNode.
//					foreach (XmlNode item in xmlNodeList[i]) {
//						if (item.Name == "content:encoded") {
//							if (item.InnerText.Contains ("img")) {
//								tempItem.Image = item.InnerText.Split (new string[] { "src=\"" }, StringSplitOptions.None) [1].Split ('\"') [0].Trim ();
//							} else if (item.InnerText.Contains ("youtube")) {
//								Debug.Log ("http://img.youtube.com/vi" + item.InnerText.Split (new string[] { "embed" }, StringSplitOptions.None) [1].Split ('\"') [0].Trim ().Split ('?') [0].Trim () + "/0.jpg");
//								tempItem.Image = "http://img.youtube.com/vi" + item.InnerText.Split (new string[] { "embed" }, StringSplitOptions.None) [1].Split ('\"') [0].Trim ().Split ('?') [0].Trim () + "/0.jpg";
//							}
//						}
//					}
					GameFlags.RSSItems.Add(tempItem);
				}
			}
			PlayerPrefs.SetString ("RSS", currentDate);
		} else {
			yield return null;
		}

		if (GameFlags.RSSItems.Count > 0) {
			for (int i = 0; i < GameFlags.RSSItems.Count; i++) {
				this.RSSTitles.Add (GameFlags.RSSItems[i].Title);
			}
		}

		dropdown.AddOptions (RSSTitles);
	}

	public void OnClickDropBox() {
		int nValue = dropdown.value;
		GameObject clonePopup = Instantiate (prefRSSPopup) as GameObject;
		clonePopup.transform.parent = mCanvas;
		clonePopup.transform.localScale = Vector3.one;
		clonePopup.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 11f);
		clonePopup.GetComponent<Popup> ().Open ();
		clonePopup.GetComponent<NewsPopup> ().InitPopup (nValue);
	}



	#region External Buttons
	/// <summary>
	/// Changes the main RSS text.
	/// </summary>
	public void ChangeCurrentItem(bool increment) {
		// Is this being incremented?  If so, check to ensure we don't go larger than the array.
		if (increment) {
			if ((CurrentItemPlacement + 1) <= (RSSItems.Length - 1)) {
				CurrentItemPlacement += 1;
			} else {
				CurrentItemPlacement = 0;
			}
		} else {						// If not, make sure we don't try to go below 0.
			if ((CurrentItemPlacement - 1) >= 0) {
				CurrentItemPlacement -= 1;
			} else {
				CurrentItemPlacement = (RSSItems.Length - 1);
			}
		}

		ChangeAndDisplayText (CurrentItemPlacement);
	}

	/// <summary>
	/// User wants to read the article online.  Send them to the proper link.
	/// </summary>
	public void ReadMorePressed () {
		Application.OpenURL (RSSItems[CurrentItemPlacement].Link);
	}

	/// <summary>
	/// Changes the current site being displayed.
	/// </summary>
	public void ChangeCurrentSite (bool increment) {
		// Is this being incremented?  If so, check to ensure we don't go larger than the array.
		if (increment) {
			if ((CurrentSitePlacement + 1) <= (URLs.Length - 1)) {
				CurrentSitePlacement += 1;
			} else {
				CurrentSitePlacement = 0;
			}
		} else {                        // If not, make sure we don't try to go below 0.
			if ((CurrentSitePlacement - 1) >= 0) {
				CurrentSitePlacement -= 1;
			} else {
				CurrentSitePlacement = (URLs.Length - 1);
			}
		}

		StartCoroutine (GetXMLNodes ());
	}
	#endregion
}
