﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;
public class Food : MonoBehaviour {
	public Transform target_pos;
	TargetMover targetMover;
	public FoodMaterial foodMaterial;
	public FoodMatCat foodMatCat;
	// Use this for initialization
	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover>();
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnMouseUp() {

	}

	public void OnMouseDown() {
		if (!GameFlags.bTutorial) {
			targetMover.PushTargetPosition2List (target_pos.position);
			CharacterAniController.Instance ().destinationFurniture = null;
			CharacterAniController.Instance ().destinationFood = this;
			CharacterAniController.Instance ().destinationTable = null;
			CharacterAniController.Instance ().bReachDestination = false;
			CharacterAniController.Instance ().targetCategories.Add ("Food");
			CharacterAniController.Instance ().targetFoodList.Add (this);
		} else {
			if (CharacterAniController.Instance ().bTakeFood == false) {
				if (foodMaterial == FoodMaterial.Macaron && TutorialManager.Instance ().nTutorialIndex == 2) {
					targetMover.PushTargetPosition2List (target_pos.position);
					CharacterAniController.Instance ().destinationFurniture = null;
					CharacterAniController.Instance ().destinationFood = this;
					CharacterAniController.Instance ().destinationTable = null;
					CharacterAniController.Instance ().bReachDestination = false;
					CharacterAniController.Instance ().targetCategories.Add ("Food");
					CharacterAniController.Instance ().targetFoodList.Add (this);
					TutorialManager.Instance ().HideArrow ();
					CharacterAniController.Instance ().bTakeFood = true;
				}
			}

		}
	}

	public void OnMouseExit() {

	}
}
