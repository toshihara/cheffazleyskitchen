﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Ricimi;
public class RecipeBook : MonoBehaviour {

	TargetMover targetMover;
	public Transform target_pos;
	public static RecipeBook recipe;

	public GameObject prefPopupRecipe;
	public GameObject mCanvas;
	GameObject popupRecipe;

	public static RecipeBook Instance() {
		if (!recipe) {
			recipe = FindObjectOfType (typeof(RecipeBook)) as RecipeBook;
		}
		return recipe;
	}

	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover>();
	}

	public void OnClickRecipeBook() {
		if (!GameFlags.bTutorial) {
			popupRecipe = Instantiate (prefPopupRecipe) as GameObject;
			popupRecipe.transform.parent = mCanvas.transform;

			popupRecipe.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupRecipe.GetComponent<Popup> ().Open ();
			popupRecipe.transform.localScale = Vector3.one;
			GameFlags.bPauseGame = true;
		} else {
			if (TutorialManager.Instance ().nTutorialIndex == 6) {
				popupRecipe = Instantiate (prefPopupRecipe) as GameObject;
				popupRecipe.transform.parent = mCanvas.transform;

				popupRecipe.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
				popupRecipe.GetComponent<Popup> ().Open ();
				popupRecipe.transform.localScale = Vector3.one;
				GameFlags.bPauseGame = true;
				TutorialManager.Instance ().HideArrow ();
			}
		}
	}

	public void OnClickRecipeClose() {
		RoundManager.Instance ().ResumeNewRound ();

	}



}
