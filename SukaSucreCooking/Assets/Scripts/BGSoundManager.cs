﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSoundManager : MonoBehaviour {
	public List<AudioSource> audioSources;

	public static BGSoundManager bgManager;

	public static BGSoundManager Instance() {
		if (!bgManager) {
			bgManager = FindObjectOfType (typeof(BGSoundManager)) as BGSoundManager;
		}
		return bgManager;
	}
	// Use this for initialization
	void Start () {
		CheckMuteSounds ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void CheckMuteSounds() {
		bool bMute = false;
		if (PlayerPrefs.GetInt ("MusicOff", 0) == 1) {
			bMute = true;
		}

		for (int i = 0; i < audioSources.Count; i++) {
			audioSources [i].mute = bMute;
		}
	}

	public void PlaySound(int nAudioIndex) {
		audioSources [nAudioIndex].mute = false;
		if (PlayerPrefs.HasKey ("MusicOff")) {
			if (PlayerPrefs.GetInt ("MusicOff", 0) == 1) {
				audioSources [nAudioIndex].mute = true;
			}
		}
		for (int i = 0; i < audioSources.Count; i++) {
			if (i == nAudioIndex)
				audioSources [i].Play ();
			else
				audioSources [i].Stop ();
		}
	}

	public void StopSounds() {
		for (int i = 0; i < audioSources.Count; i++) {
				audioSources [i].Stop ();
		}
	}


	public void ResumeGame() {
		GameFlags.bPauseGame = false;
	}
}
