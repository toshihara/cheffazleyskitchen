﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class CoinPurchaseScript : MonoBehaviour, IStoreListener {
	public static CoinPurchaseScript instance;
	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;

	private static string kProductIDSpeed = "chefspeed2";
	private static string kProductIDCoin4k = "coin.4000";
	private static string kProductIDCoin12k = "coin.12000";
	private static string kProductIDCoin20k = "coin.20000";
	private static string kProductIDRemoveAds = "removeads2";

	public static CoinPurchaseScript Instance() {
		if (!instance) {
			instance = FindObjectOfType (typeof(CoinPurchaseScript)) as CoinPurchaseScript;
		}
		return instance;
	}


	void Start() {
		if (m_StoreController == null) {
			InitializePurchasing ();
		}
	}

	public void InitializePurchasing() {
		if (IsInitialized ())
			return;
		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance());
		builder.AddProduct (kProductIDSpeed, ProductType.Consumable);
		builder.AddProduct (kProductIDRemoveAds, ProductType.Consumable);
		builder.AddProduct (kProductIDCoin4k, ProductType.Consumable);
		builder.AddProduct (kProductIDCoin12k, ProductType.Consumable);
		builder.AddProduct (kProductIDCoin20k, ProductType.Consumable);
		UnityPurchasing.Initialize (this, builder);

	}


	private bool IsInitialized() {
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	void BuyProductID(string productId) {
		if (IsInitialized ()) {
			Product product = m_StoreController.products.WithID (productId);

			if (product != null && product.availableToPurchase) {
				Debug.Log (string.Format ("Purchasing product asychronously:'{0}'", product.definition.id));
				m_StoreController.InitiatePurchase (product);
			} else {
				Debug.Log ("Buying productID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		} else {
			Debug.Log ("Buying PRoductID: FAIL. Not Initialized.");
		}

	}

	public void Buy4kCoin() {
		BuyProductID (kProductIDCoin4k);
	}

	public void Buy12kCoin() {
		BuyProductID (kProductIDCoin12k);
	}

	public void Buy20kCoin() {
		BuyProductID (kProductIDCoin20k);
	}

	public void BuyNoAds() {
		BuyProductID (kProductIDRemoveAds);
	}

	public void BuySpeedBooster() {
		BuyProductID (kProductIDSpeed);
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
		Debug.Log ("OnInitialized: PASS");
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error) {
		Debug.Log ("OnInitializedFailed InitializationFailureReason: " + error);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
		Debug.Log ("purchase Success: " + args.purchasedProduct.definition.id);
		int coinCount = PlayerPrefs.GetInt ("Coin");
		if (string.Equals (args.purchasedProduct.definition.id, kProductIDCoin4k, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin", coinCount + 4000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text> ().text = "" + (coinCount + 4000);
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip ((long)(coinCount + 4000));
				ApiHandler.Instance ().SendGameLog (4000, 0, 0);
			}
		} else if (string.Equals (args.purchasedProduct.definition.id, kProductIDCoin12k, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin", coinCount + 12000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text> ().text = "" + (coinCount + 12000);
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip ((long)(coinCount + 12000));
				ApiHandler.Instance ().SendGameLog (12000, 0, 0);
			}
		} else if (string.Equals (args.purchasedProduct.definition.id, kProductIDCoin20k, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("Coin", coinCount + 20000);
			GameObject.Find ("Text_Level_Coin").GetComponent<Text> ().text = "" + (coinCount + 20000);
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip ((long)(coinCount + 20000));
				ApiHandler.Instance ().SendGameLog (20000, 0, 0);
			}
		} else if (string.Equals (args.purchasedProduct.definition.id, kProductIDSpeed, System.StringComparison.Ordinal)) {
			PlayerPrefs.SetInt ("SpeedUp", 1);
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().LogPurchaseResult ("chefspeed2x", 1);
			}
			ShopPopupManager.Instance ().HidePurchaseButton ();
		} else {
			if (!PlayerPrefs.HasKey ("Ads") || PlayerPrefs.GetInt ("Ads", 0) == 0) {
				PlayerPrefs.SetInt ("Ads", 1);
				if (PlayerPrefs.HasKey ("encPass")) {
					ApiHandler.Instance ().LogPurchaseResult ("removeads1", 1);
				}
			}
			
		}
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
		Debug.Log (string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: '{1}'", product.definition.storeSpecificId, failureReason));
	}

	public void RestorePurchases() {
		if (!IsInitialized ()) {
			Debug.Log ("RestorePurchases Fail, Not Initialized.");
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {
			Debug.Log ("RestorePurchases started ...");
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
			apple.RestoreTransactions ((result) => {
				Debug.Log ("RestorePurchases Continuing: " + result);

			});

		} else {
			Debug.Log ("Not support this platform");
		}
	}
}

