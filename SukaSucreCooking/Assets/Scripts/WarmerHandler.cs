﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ricimi;
using Pathfinding;

public class WarmerHandler : MonoBehaviour {
	TargetMover targetMover;
	public Transform targetPos;
	public int nWarmerIndex;
	public string cookName;

	public Sprite[] sprite_cooks;
	public Image warmingCook;
	// Use this for initialization
	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover> ();
	}

	void Update() {
		if (cookName == "") {
			warmingCook.gameObject.SetActive (false);
		}
		else
			warmingCook.gameObject.SetActive (true);
	}

	public void OnClickWarmer() {
		if (!GameFlags.bTutorial) {
			targetMover.PushTargetPosition2List(targetPos.position);
			CharacterAniController.Instance ().destinationFood = null;
			CharacterAniController.Instance ().destinationTable = null;
			CharacterAniController.Instance ().destinationFurniture = null;
			CharacterAniController.Instance().targetWarmingStationList.Add(nWarmerIndex);
			CharacterAniController.Instance ().bReachDestination = false;
			CharacterAniController.Instance ().targetCategories.Add ("Warmer");
		}

	}

	int ReturnSpriteIndex(string strCook) {
		int nSpriteCook = -1;
		switch (strCook) {
		case "Plain Macaron":
			nSpriteCook = 0;
			break;
		case "Chocolate Macaron":
			nSpriteCook = 1;
			break;
		case "Chocolate Caramel Macaron":
			nSpriteCook = 2;
			break;
		case "Churros Plain":
			nSpriteCook = 3;
			break;
		case "Churros Chocolate":
			nSpriteCook = 4;
			break;
		case "Churros Chocolate Caramel":
			nSpriteCook = 5;
			break;
		case "Plain Cake":
			nSpriteCook = 6;
			break;
		case "Chocolate Cake":
			nSpriteCook = 7;
			break;
		case "Chocolate Caramel Cake":
			nSpriteCook = 8;
			break;
		case "Pasta Piyash":
			nSpriteCook = 9;
			break;
		case "Pasta Payish":
			nSpriteCook = 10;
			break;
		case "Pasta Daging Salai Lemak Cili Api":
			nSpriteCook = 11;
			break;
		case "French Butter Rice":
			nSpriteCook = 12;
			break;
		case "French Butter Rice With Rendang & Vegetable Pickles":
			nSpriteCook = 13;
			break;
		case "French Butter Rice With Daging Salai Lemak Cili Api":
			nSpriteCook = 14;
			break;
		case "Ratatouille Pizza":
			nSpriteCook = 15;
			break;
		case "Rendang Pizza":
			nSpriteCook = 16;
			break;
		case "Heavy Duty Meat Pizza":
			nSpriteCook = 17;
			break;
		case "Toasted Eclair":
			nSpriteCook = 18;
			break;
		case "Toasted Eclair Pulled Beef Rendang":
			nSpriteCook = 19;
			break;
		case "Toasted Eclair Daging Salai":
			nSpriteCook = 20;
			break;
		case "Americano":
			nSpriteCook = 21;
			break;
		case "Caramel Latte":
			nSpriteCook = 22;
			break;
		case "Hot Chocolate":
			nSpriteCook = 23;
			break;
		case "Frozen Grapes Mint Soda":
			nSpriteCook = 24;
			break;
		case "Soda Bandung":
			nSpriteCook = 25;
			break;
		case "Sucre Tea":
			nSpriteCook = 26;
			break;
		case "Burn":
			nSpriteCook = 27;
			break;
		}
		return nSpriteCook;
	}

	public bool ReceiveCook(string cook_name) {
		if (cookName == "") {
			if (cook_name != "" && cook_name != "Burn") {
				cookName = cook_name;
				Debug.Log ("##### Cook Name: " + cookName);
				int nSpriteCook = ReturnSpriteIndex (cookName);
				warmingCook.gameObject.SetActive (true);
				warmingCook.sprite = sprite_cooks [nSpriteCook];
				return true;
			}
		}
		return false;
	}

	public string GiveBackCook() {
		if (cookName != "") {
			string returnCook = cookName;
			cookName = "";
			warmingCook.gameObject.SetActive (false);
			Debug.Log ("CookName:" + returnCook);
			return returnCook;
		}
		return "";
	}

	public void EmptyWarmer() {
		cookName = "";
		warmingCook.gameObject.SetActive (false);
	}
}
