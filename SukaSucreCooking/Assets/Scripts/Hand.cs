﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hand: MonoBehaviour {
//	public SpriteRenderer spriteHand;
	public string foodName;
	public List<FoodMaterial> currentFoodMaterialList = new List<FoodMaterial>();
	public bool bHoldCook = false;
//	public SpriteRenderer spriteIdlePoseHand;
	public SpriteRenderer spriteFrontPoseHand;
	public SpriteRenderer spriteBackPoseHand;
//	public SpriteRenderer spriteLeftPoseHand;
//	public SpriteRenderer spriteRightPoseHand;

	public HandKind handKind;

	public void SetFood(string strFoodName) {
		foodName = strFoodName;
		int nfoodIndex = CharacterAniController.Instance ().GetCookIndex (foodName);
//		spriteHand.sprite = CharacterAniController.Instance ().cookSpriteList[nfoodIndex];
		UpdateHandSprites(CharacterAniController.Instance ().cookSpriteList[nfoodIndex]);
	}
		
	void Update() {
		CheckPose ();
//		if (currentFoodMaterialList.Count == 0 && !bHoldCook) {
//			spriteHand.gameObject.transform.localScale = Vector3.zero; // (false);
//		} else
//			spriteHand.gameObject.transform.localScale = Vector3.one * 0.5f; //SetActive (true);
	}

	public void EmptyHand() {
		//spriteHand.gameObject.transform.localScale = Vector3.zero;//.SetActive (false);
		this.ActivateHandSprites(0);
		currentFoodMaterialList.Clear ();
		bHoldCook = false;
		foodName = "";
	}

	public void GetCook() {
		foodName = CharacterAniController.Instance().destinationFurniture.GetFood();

		CharacterAniController.Instance().destinationFurniture.EmptyUtensil ();
		CharacterAniController.Instance().destinationFurniture = null;

		int nCookId = CharacterAniController.Instance().GetCookIndex (foodName);
		if (nCookId > -1) {
			bHoldCook = true;
			//spriteHand.sprite = CharacterAniController.Instance().cookSpriteList [nCookId];
			UpdateHandSprites(CharacterAniController.Instance().cookSpriteList [nCookId]);
		}
	}

	public bool CheckSendMaterials2Furniture() {
		if (currentFoodMaterialList.Count > 0 || CharacterAniController.Instance().destinationFurniture.utensilType == UtensilType.CoffeeMachine || CharacterAniController.Instance().destinationFurniture.utensilType == UtensilType.ColdMachine) {
			bool bAccepted = CharacterAniController.Instance().destinationFurniture.ReceiveFoodMaterials (currentFoodMaterialList);
			if (bAccepted) {
				currentFoodMaterialList.Clear ();
				return true;
			}
			//destinationFurniture = null;
		}
		return false;
	}

	public void CheckNAddFoodMaterial(FoodMaterial foodMat) {
		if (currentFoodMaterialList.Count > 0 && currentFoodMaterialList [0] == FoodMaterial.Ice)
			currentFoodMaterialList.Clear ();
		if (currentFoodMaterialList.Count == 0) {
			switch (foodMat) {
			case FoodMaterial.Macaron:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [0];
				UpdateHandSprites (CharacterAniController.Instance ().foodMatList [0]);
				CharacterAniController.Instance ().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 2) {
					TutorialManager.Instance ().ShowTutorial (2);
				}
				break; 
			case FoodMaterial.Churros:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [3];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [3]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.Cake:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [6];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [6]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.Spaghetti:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [9];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [9]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.Rice:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [12];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [12]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.PizzaCrust:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [15];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [15]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.Eclairs:
				currentFoodMaterialList.Add (foodMat);
				//spriteHand.sprite = CharacterAniController.Instance().foodMatList [18];
				UpdateHandSprites(CharacterAniController.Instance().foodMatList [18]);
				CharacterAniController.Instance().destinationFood = null;
				ChefAudioManager.Instance ().PickupRawFood ();
				break;
			case FoodMaterial.Ice:
				//currentFoodMaterialList.Add (foodMat);

				CheckAddIce2Drink();
				//CharacterAniController.Instance().destinationFood = null;
				break;
			}
		} else if (currentFoodMaterialList.Count == 1) {
			switch (currentFoodMaterialList[0]) {
			case FoodMaterial.Macaron:
				if (foodMat == FoodMaterial.Chocolate) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [1];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [1]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break; 
			case FoodMaterial.Churros:
				if (foodMat == FoodMaterial.Chocolate) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [4];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [4]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break;
			case FoodMaterial.Cake:
				if (foodMat == FoodMaterial.Chocolate) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [7];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [7]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break;
			case FoodMaterial.Spaghetti:
				if (foodMat == FoodMaterial.Tomato) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [10];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [10]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.Rice:
				if (foodMat == FoodMaterial.Meat) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [13];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [13]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.PizzaCrust:
				if (foodMat == FoodMaterial.Meat) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [16];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [16]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.Eclairs:
				if (foodMat == FoodMaterial.Meat) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [19];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [19]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			}
		}else if (currentFoodMaterialList.Count == 2) {
			switch (currentFoodMaterialList[0]) {
			case FoodMaterial.Macaron:
				if (foodMat == FoodMaterial.Caramel) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [2];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [2]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break; 
			case FoodMaterial.Churros:
				if (foodMat == FoodMaterial.Caramel) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [5];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [5]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break;
			case FoodMaterial.Cake:
				if (foodMat == FoodMaterial.Caramel) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [8];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [8]);
					CharacterAniController.Instance().destinationFood = null;
					//ChefAudioManager.Instance ().PickupRawFood ();
					SoundManager.Instance().PlaySound(7);
				}
				break;
			case FoodMaterial.Spaghetti:
				if (foodMat == FoodMaterial.Turmeric) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [11];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [11]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.Rice:
				if (foodMat == FoodMaterial.Turmeric) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [14];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [14]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.PizzaCrust:
				if (foodMat == FoodMaterial.Tomato) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [17];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [17]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			case FoodMaterial.Eclairs:
				if (foodMat == FoodMaterial.Turmeric) {
					currentFoodMaterialList.Add (foodMat);
					//spriteHand.sprite = CharacterAniController.Instance().foodMatList [20];
					UpdateHandSprites(CharacterAniController.Instance().foodMatList [20]);
					CharacterAniController.Instance().destinationFood = null;
					ChefAudioManager.Instance ().PickupRawFood ();
				}
				break;
			}
		}
	}
		
	public void CheckAddIce2Drink() {
		if (this.foodName != "") {
			switch (foodName) {
			case "Frozen Grapes Mint Soda no_ice":
				foodName = "Frozen Grapes Mint Soda";
				UpdateHandSprites (CharacterAniController.Instance ().cookSpriteList [27]);
				CharacterAniController.Instance ().destinationFood = null;
				//ChefAudioManager.Instance ().PickupRawFood ();
				SoundManager.Instance().PlaySound(6);
				break;
			case "Soda Bandung no_ice":
				foodName = "Soda Bandung";
				UpdateHandSprites(CharacterAniController.Instance().cookSpriteList [28]);
				CharacterAniController.Instance().destinationFood = null;
				//ChefAudioManager.Instance ().PickupRawFood ();
				SoundManager.Instance().PlaySound(6);
				break;
			case "Sucre Tea no_ice":
				foodName = "Sucre Tea";
				UpdateHandSprites(CharacterAniController.Instance().cookSpriteList [29]);
				CharacterAniController.Instance().destinationFood = null;
				//ChefAudioManager.Instance ().PickupRawFood ();
				SoundManager.Instance().PlaySound(6);
				break;
			}

		}
	}

	public bool Throw2DustBin() {
		if (bHoldCook || this.currentFoodMaterialList.Count > 0) {
			DustBinController.Instance ().OpenDustBin ();
			if (bHoldCook) {
				bHoldCook = false;
				this.foodName = "";
			}
			if (this.currentFoodMaterialList.Count > 0)
				this.currentFoodMaterialList.Clear ();
			ChefAudioManager.Instance ().Throw2DustBin ();
			return true;
		}
		return false;
		//bClickDustBin = false;
	}

	public void UpdateHandSprites(Sprite spriteHandMat) {
		spriteFrontPoseHand.sprite = spriteHandMat;
		spriteBackPoseHand.sprite = spriteHandMat;
//		spriteLeftPoseHand.sprite = spriteHandMat;
//		spriteRightPoseHand.sprite = spriteHandMat;
//		spriteIdlePoseHand.sprite = spriteHandMat;
//		spriteHand.sprite = spriteHandMat;
	}

	void ActivateHandSprites(int nActiveCode) {
		if (nActiveCode == 0) {
			spriteFrontPoseHand.color = new Color (0, 0, 0, 0);
			spriteBackPoseHand.color = new Color (0, 0, 0, 0);
//			spriteLeftPoseHand.color = new Color (0, 0, 0, 0);
//			spriteRightPoseHand.color = new Color (0, 0, 0, 0);
//			spriteIdlePoseHand.color = new Color (0, 0, 0, 0);
//			spriteHand.color = new Color (0,0,0,0);
		} else if (nActiveCode == 1) { // front up
			spriteFrontPoseHand.color = new Color (1f, 1f, 1f, 1f);
			spriteBackPoseHand.color = new Color (0, 0, 0, 0);
//			spriteLeftPoseHand.color = new Color (0, 0, 0, 0);
//			spriteRightPoseHand.color = new Color (0, 0, 0, 0);
//			spriteIdlePoseHand.color = new Color (0, 0, 0, 0);
//			spriteHand.color = new Color (1f, 1f, 1f, 1f);
			if (handKind == HandKind.Left)
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0,0,-27f);
			else
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -153f);
			
		} else if (nActiveCode == 2) {
			spriteFrontPoseHand.color = new Color (0, 0, 0, 0);
			spriteBackPoseHand.color = new Color (1f, 1f, 1f, 1f);
//			spriteLeftPoseHand.color = new Color (0, 0, 0, 0);
//			spriteRightPoseHand.color = new Color (0, 0, 0, 0);
//			spriteIdlePoseHand.color = new Color (0, 0, 0, 0);
//			spriteHand.color = new Color (1f, 1f, 1f, 1f);
			if (handKind == HandKind.Left)
				spriteBackPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -153f);
			else
				spriteBackPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -27f);
		} else if (nActiveCode == 3) {
			spriteFrontPoseHand.color = new Color (1f, 1f, 1f, 1f);
			spriteBackPoseHand.color = new Color (0, 0, 0, 0);
//			spriteLeftPoseHand.color = new Color (1f, 1f, 1f, 1f);
//			spriteRightPoseHand.color = new Color (0, 0, 0, 0);
//			spriteIdlePoseHand.color = new Color (0, 0, 0, 0);
			spriteFrontPoseHand.color = new Color (1f, 1f, 1f, 1f);
			if (handKind == HandKind.Left)
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, 6f);
			else
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -183f);
		} else if (nActiveCode == 4) {
			spriteFrontPoseHand.color = new Color (1f, 1f, 1f, 1f);
			spriteBackPoseHand.color = new Color (0, 0, 0, 0);
//			spriteLeftPoseHand.color = new Color (0, 0, 0, 0);
//			spriteRightPoseHand.color = new Color (1f, 1f, 1f, 1f);
//			spriteIdlePoseHand.color = new Color (0, 0, 0, 0);
//			spriteHand.color = new Color (1f, 1f, 1f, 1f);
			if (handKind == HandKind.Left)
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, 6f);
			else
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -183f);
		} else if (nActiveCode == 5) {
			spriteFrontPoseHand.color = new Color (1f, 1f, 1f, 1f);
			spriteBackPoseHand.color = new Color (0, 0, 0, 0);
//			spriteLeftPoseHand.color = new Color (0, 0, 0, 0);
//			spriteRightPoseHand.color = new Color (0 ,0, 0, 0);
//			spriteIdlePoseHand.color = new Color (1f, 1f, 1f, 1f);
//			spriteHand.color = new Color (1f, 1f, 1f, 1f);
			if (handKind == HandKind.Left)
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -27f);
			else
				spriteFrontPoseHand.gameObject.transform.localEulerAngles = new Vector3 (0, 0, -153f);
		}
	}

	public void CheckPose() {
		if (bHoldCook || currentFoodMaterialList.Count > 0) {
//			Debug.Log ("Hand:" + handKind + ", playerPos:" + GameFlags.currentPlayerPose);
			switch (GameFlags.currentPlayerPose) {
			case PlayerPoseSituation.IdleHandsDown:
				ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.IdleHandsUp:
				ActivateHandSprites (5);
				break;
			case PlayerPoseSituation.IdleLeftHandUp:
				if (handKind == HandKind.Left)
					ActivateHandSprites (5);
				else
					ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.IdleRightHandUp:
				if (handKind == HandKind.Right)
					ActivateHandSprites (5);
				else
					ActivateHandSprites (0);
				break;

			case PlayerPoseSituation.WalkFrontHandsDown:
				ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkFrontHandsUp:
				ActivateHandSprites (1);
				break;
			case PlayerPoseSituation.WalkFrontLeftHandUp:
				if (handKind == HandKind.Left)
					ActivateHandSprites (1);
				else
					ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkFrontRightHandUp:
				if (handKind == HandKind.Right)
					ActivateHandSprites (1);
				else
					ActivateHandSprites (0);
				break;

			case PlayerPoseSituation.WalkLeftHandsDown:
				ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkLeftHandsUp:
				ActivateHandSprites (4);
				break;
			case PlayerPoseSituation.WalkLeftLeftHandUp:
				if (handKind == HandKind.Left)
					ActivateHandSprites (4);
				else
					ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkLeftRightHandUp:
				if (handKind == HandKind.Right)
					ActivateHandSprites (4);
				else
					ActivateHandSprites (0);
				break;

			case PlayerPoseSituation.WalkRightHandsDown:
				ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkRightHandsUp:
				ActivateHandSprites (3);
				break;
			case PlayerPoseSituation.WalkRightLeftHandUp:
				if (handKind == HandKind.Left)
					ActivateHandSprites (3);
				else
					ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkRightRightHandUp:
				if (handKind == HandKind.Right)
					ActivateHandSprites (3);
				else
					ActivateHandSprites (0);
				break;

			case PlayerPoseSituation.WalkBackHandsDown:
				ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkBackHandsUp:
				ActivateHandSprites (2);
				break;
			case PlayerPoseSituation.WalkBackLeftHandUp:
				if (handKind == HandKind.Left)
					ActivateHandSprites (2);
				else
					ActivateHandSprites (0);
				break;
			case PlayerPoseSituation.WalkBackRightHandUp:
				if (handKind == HandKind.Right)
					ActivateHandSprites (2);
				else
					ActivateHandSprites (0);
				break;
			}
		} else {
			this.ActivateHandSprites (0);
		}
	}

	public bool IsHandEmpty() {
		if (bHoldCook || this.currentFoodMaterialList.Count > 0)
			return false;
		return true;
	}
		
}
