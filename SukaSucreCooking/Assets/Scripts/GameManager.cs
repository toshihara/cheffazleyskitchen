﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Text;


public class GameManager : MonoBehaviour {
	public static GameManager gm;
	public GameObject levelSceenManagerObj;
	public GameObject roundManagerObj;
	public GameObject loadingPanel;
	public GameObject prefLevelManager;
	public GameObject prefRoundManager;
	public GameObject prefLoadingPanel;
	public GameObject prefCreditsObj;
	public GameObject creditsObj;
	public bool bPlayLevelVoice = false;

	public static GameManager Instance() {
		if (!gm) {
			gm = FindObjectOfType (typeof(GameManager)) as GameManager;
		}
		return gm;
	}

	// Use this for initialization
	void Start () {
		LoadRoundInfo ();
		if (!bPlayLevelVoice) {
			VoiceManager.Instance ().PlayLevelSelectionVoice ();
			bPlayLevelVoice = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayGame() {
//		roundManagerObj = Instantiate (prefRoundManager, Vector3.zero, Quaternion.identity) as GameObject;
//		if (levelSceenManagerObj)
//			Destroy (levelSceenManagerObj);
		StartCoroutine(Wait2GeneratePlayScene());
	}

	IEnumerator Wait2GeneratePlayScene() {
		BGSoundManager.Instance ().StopSounds ();
		loadingPanel = Instantiate (prefLoadingPanel) as GameObject;
		if (levelSceenManagerObj)
			Destroy (levelSceenManagerObj);
		VoiceManager.Instance ().PlayLoadingVoice ();
		yield return new WaitForSeconds (2f);
		roundManagerObj = Instantiate (prefRoundManager, Vector3.zero, Quaternion.identity) as GameObject;
		Destroy (loadingPanel);
		roundManagerObj.SendMessage ("InitTimeTracer");
		//roundManagerObj.SendMessage ("PlaySound", 1);
		BGSoundManager.Instance().PlaySound(1);
	}

	IEnumerator Wait2GenerateLevelScene() {
		BGSoundManager.Instance ().StopSounds ();
		loadingPanel = Instantiate (prefLoadingPanel) as GameObject;
		if (roundManagerObj)
			Destroy (roundManagerObj);
		yield return new WaitForSeconds (2f);
		levelSceenManagerObj = Instantiate (prefLevelManager, Vector3.zero, Quaternion.identity) as GameObject;
		Destroy (loadingPanel);
		levelSceenManagerObj.SendMessage ("DisplayCoin");

		//levelSceenManagerObj.SendMessage ("PlaySound", 1);
		//BGSoundManager.Instance().PlaySound(0);
	}

	public void LoadLevelScene() {
//		levelSceenManagerObj = Instantiate (prefLevelManager, Vector3.zero, Quaternion.identity) as GameObject;
//		if (roundManagerObj)
//			Destroy (roundManagerObj);
//		levelSceenManagerObj.SendMessage ("DisplayCoin");
		StartCoroutine(Wait2GenerateLevelScene());
	}

	public void LoadCreditsScene() {
		creditsObj = Instantiate (prefCreditsObj) as GameObject;
		if (roundManagerObj)
			Destroy (roundManagerObj);
	}

	public void LoadLevelSceneFromCredits(GameObject credits) {
		levelSceenManagerObj = Instantiate (prefLevelManager, Vector3.zero, Quaternion.identity) as GameObject;
		if (credits)
			Destroy (credits);
		levelSceenManagerObj.SendMessage ("DisplayCoin");
	}

	private void LoadRoundInfo() {

		TextAsset textAsset = (TextAsset)Resources.Load("RoundInfo");
		XmlDocument xmlDoc = new XmlDocument ();
		xmlDoc.LoadXml (textAsset.text);
		XmlNodeList level = xmlDoc.GetElementsByTagName ("Round");
		GameFlags.roundInfoList.Clear ();

		for (int i = 0; i < level.Count; i++) {
			
			int nRoundId = int.Parse(level [i].Attributes ["value"].Value);
			float fRoundTime = float.Parse(level[i].Attributes["time"].Value);
			int nRoundTargetScore = int.Parse(level[i].Attributes["target"].Value);
			int nRoundBonusPoint = int.Parse(level [i].Attributes ["starpoint"].Value);
			float fInterval = float.Parse (level[i].Attributes["interval"].Value);
			XmlNodeList roundMenus = level [i].ChildNodes;
			List<string> menus = new List<string> ();

			for (int j = 0; j < roundMenus.Count; j++) {
				menus.Add(roundMenus [j].Attributes ["value"].Value);
			}
			//Debug.Log ("Round Time: " + nRoundTargetScore);
			RoundInfomation roundInfo = new RoundInfomation (1, nRoundId, nRoundTargetScore, nRoundBonusPoint, fRoundTime, fInterval);
			roundInfo.menuList = menus;
			GameFlags.roundInfoList.Add (roundInfo);

		}

	}

	public void CheckUserStatus() {
		if (!PlayerPrefs.HasKey ("CurrentLevel")) {
			PlayerPrefs.SetInt ("CurrentLevel", 1);
			PlayerPrefs.SetInt ("Stove", 1);
			PlayerPrefs.SetInt ("Oven", 1);
			PlayerPrefs.SetInt ("CoffeeMachine", 1);
			PlayerPrefs.SetInt ("DrinkMachine", 1);
			PlayerPrefs.SetInt ("CurrentRound", 1);
			PlayerPrefs.SetInt ("Fridge", 1);
			PlayerPrefs.SetInt ("AvailLevel", 1);
		}
	}
		
}
