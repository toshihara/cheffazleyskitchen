﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class DiarySpinController : MonoBehaviour {

	public static DiarySpinController spinController;
	public GameObject btnSpin;
	public Text txt_spin;
	public static DiarySpinController Instance() {
		if (!spinController) {
			spinController = FindObjectOfType (typeof(DiarySpinController)) as DiarySpinController;
		}
		return spinController;
	}

	// Use this for initialization
	void Start () {
		CheckValidSpin ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GetSpinResult(int spinResult) {
		int coin = PlayerPrefs.GetInt ("Coin");
		int nSpinCoin = 0;
		switch (spinResult) {
		case 0:
			nSpinCoin = 20;
			break;
		case 1:
			nSpinCoin = 60;
			break;
		case 2:
			nSpinCoin = 30;
			break;
		case 3:
			nSpinCoin = 40;
			break;
		case 4:
			nSpinCoin = 80;
			break;
		case 5:
			nSpinCoin = 50;
			break;
		case 6:
			nSpinCoin = 10;
			break;
		case 7:
			nSpinCoin = 70;
			break;

		}
		PlayerPrefs.SetInt ("Coin", nSpinCoin + coin);
		if (PlayerPrefs.HasKey ("encPass")) {
			long lChip = (long)(nSpinCoin + coin);
			ApiHandler.Instance ().SendGameLog ((long)nSpinCoin, 0, 0);
			ApiHandler.Instance ().UpdateChip ((long)(nSpinCoin + coin));

		}
		LevelSceneManager.Instance ().DisplayCoin ();
		string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
		PlayerPrefs.SetString ("SpinDate",currentDate);
		CheckValidSpin ();

	}

	void CheckValidSpin() {
		string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
		btnSpin.SetActive (true);
		txt_spin.text = "Spin to win\n coins daily. ";
		if (PlayerPrefs.HasKey ("SpinDate")) {
			if (PlayerPrefs.GetString ("SpinDate") == currentDate) {
				btnSpin.SetActive (false);
				txt_spin.text = "You already receive\n today's spin coins. ";
			}
		}
	}
}
