﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : MonoBehaviour {

	public CustomerProperty Adam 	= new CustomerProperty (3f, 20f, 4f, 3f);
	public CustomerProperty Alia 	= new CustomerProperty (3f, 20f, 4f, 3f);
	public CustomerProperty Suresh	= new CustomerProperty (3f, 17f, 4f, 3f);
	public CustomerProperty Stacy	= new CustomerProperty (3f, 17f, 4f, 3f);
	public CustomerProperty Faidaus = new CustomerProperty (3f, 15f, 4f, 3f);
	public CustomerProperty Adri 	= new CustomerProperty (3f, 15f, 4f, 3f);
	public CustomerProperty Anom 	= new CustomerProperty (3f, 12f, 4f, 3f);
	public CustomerProperty Mikey 	= new CustomerProperty (3f, 12f, 4f, 3f);

	public List<int> listEmptySeat = new List<int>(); 

	public float fTimeTick = 0f;
	public float fTimeRate = 10f;
	bool bReadyPop = false;
//	bool bEnd = false;
	public Customer[] customerList;
	int nSeatIndex;
	public static CustomerManager customerManager;
	public Sprite[] customerSprites;

	int nCountCustomer = 0;

	public static CustomerManager Instance() {
		if (!customerManager) {
			customerManager = FindObjectOfType (typeof(CustomerManager)) as CustomerManager;
		}
		return customerManager;
	}
	// Use this for initialization
	void Start () {
		InitSeatList ();
		if (GameFlags.bTutorial) {
			PopTutorialChair ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameFlags.bTutorial) {
			if (!GameFlags.bEndRound && !GameFlags.bPauseGame && !GameFlags.bTimesup)
				UpdateTimeRate ();
			if (listEmptySeat.Count == 3) {
				GameFlags.bEmptySeat = true;
			} else
				GameFlags.bEmptySeat = false; 
		}
	}
		
	void InitSeatList() {
		listEmptySeat.Clear ();
		listEmptySeat.Add (0);
		listEmptySeat.Add (1);
		listEmptySeat.Add (2);
	}

	public void InsertSeat2EmptyList(int seatIndex) {
		listEmptySeat.Add (seatIndex);
	}

	public void PopSeatFromEmptyList(int seatIndex) {
		listEmptySeat.RemoveAt(seatIndex);
	}

	/*A function that make the customer activate based on 
	 *  the time rate which is related to rounds.
	 * called at update() function
	*/
	public void PopTutorialChair() {
		nSeatIndex = listEmptySeat [0];
		customerList[listEmptySeat[0]].InitCharacter ();
		PopSeatFromEmptyList (0);
	}




	void UpdateTimeRate() {
		if (nCountCustomer >= RoundManager.Instance ().nMaxCustomerCounter)
			return;
		if (fTimeTick == 0 || bReadyPop) {
			if (listEmptySeat.Count > 0) {
				if (listEmptySeat.Count == 1) {
					nSeatIndex = listEmptySeat [0];
					customerList[listEmptySeat[0]].InitCharacter ();
					PopSeatFromEmptyList (0);
				} else {
					nSeatIndex = Random.Range (0, listEmptySeat.Count);
					customerList[listEmptySeat[nSeatIndex]].InitCharacter ();
					PopSeatFromEmptyList (nSeatIndex);
				}
				bReadyPop = false;
				nCountCustomer++;
			} else
				bReadyPop = true;
		} 

		fTimeTick += Time.deltaTime;
		if (fTimeTick > fTimeRate || bReadyPop) {
			fTimeTick = 0;
		}

	}

	public void RestartGame() {
		fTimeRate = RoundManager.Instance().fInterval;
		fTimeTick = 0;
		nCountCustomer = 0;
		InitSeatList ();
		for (int i = 0; i < 3; i++) {
			customerList [i].InitCustomer ();
		}
		Furniture[] furnitures = FindObjectsOfType (typeof(Furniture)) as Furniture[];
		foreach(Furniture furniture in furnitures) {
			furniture.EmptyUtensil ();
		}

	}

	public void EndGameRound() {
		for (int i = 0; i < 3; i++) {
			customerList [i].InitCustomer ();
		}

		Furniture[] furnitures = FindObjectsOfType (typeof(Furniture)) as Furniture[];
		foreach(Furniture furniture in furnitures) {
			furniture.EmptyUtensil ();
		}
	}
}
