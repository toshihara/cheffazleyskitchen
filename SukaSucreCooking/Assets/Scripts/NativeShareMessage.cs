﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;

/*
 * https://github.com/ChrisMaire/unity-native-sharing 
 */

public class NativeShareMessage : MonoBehaviour {
	public string ScreenshotName = "screenshot.png";
	public static NativeShareMessage natshare;
	public static NativeShareMessage Instance() {
		if (!natshare) {
			natshare = FindObjectOfType (typeof(NativeShareMessage)) as NativeShareMessage;
		}
		return natshare;
	}

	void DisplayImage(string imgPath) {
		StartCoroutine (LoadImageFromServer(imgPath));
	}

	IEnumerator LoadImageFromServer (string urlImage) {
		WWW www = new WWW(urlImage);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			//img_avatar.texture = www.texture;
			byte[] bytes = www.texture.EncodeToPNG();
			File.WriteAllBytes(Application.persistentDataPath + "/" + ScreenshotName, bytes);
			Debug.Log (Application.persistentDataPath + "/" + ScreenshotName);
			Share ("", Application.persistentDataPath + "/" + ScreenshotName, "");
		}
	}

    public void ShareScreenshotWithText(string text)
    {
        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
        Application.CaptureScreenshot(ScreenshotName);

        Share(text,screenShotPath,"");
    }

	public void ShareImageWithText(Texture2D imgTexture, string imgName) {
		byte[] bytes = imgTexture.EncodeToPNG ();
		File.WriteAllBytes(Application.dataPath + "/" + imgName, bytes);
		Share ("", Application.dataPath + "/" + imgName, "");
	}

	public void ShareFavImage(string imgPath){
		//Share ("", imgPath, "");
		Share(Config.URL_SHARE_IMG, "", "");
	}

	public void ShareURLImage() {
		DisplayImage(Config.URL_SHARE_IMG);
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
#if UNITY_ANDROID
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);
		
		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		
		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);
#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
#else
		Debug.Log("No sharing set up for this platform.");
#endif
	}

#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);
	
	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}
	
	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);
	
	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;
		
		showSocialSharing(ref conf);
	}
#endif
}
