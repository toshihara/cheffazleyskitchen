﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehavior : MonoBehaviour {
	float fTimeTracker = 0;
	float fTimeLimit = 2f;
	float fEulerX = 1440f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.position = Vector3.Lerp (transform.position, GameFlags.CoinController, fTimeTracker/fTimeLimit);
		transform.eulerAngles = Vector3.Lerp (transform.eulerAngles, new Vector3(720f, 0, 0), fTimeTracker/fTimeLimit);

		fTimeTracker += Time.deltaTime;
		if (fTimeTracker > fTimeLimit) {
			RoundManager.Instance ().DestroyCoinObject (gameObject);
		}
			
	}



}
