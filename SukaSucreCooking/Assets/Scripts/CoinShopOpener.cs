﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ricimi;
public class CoinShopOpener : MonoBehaviour {
	public GameObject prefCoinShop;
	GameObject coinShop;
	Transform mCanvas;
	void Start() {
		mCanvas = GameObject.Find ("Canvas").transform;
	}

	public void OpenCoinShop () {
		coinShop = Instantiate (prefCoinShop) as GameObject;
		mCanvas = GameObject.Find ("Canvas").transform;
		coinShop.transform.parent = mCanvas;
		coinShop.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -238f);
		coinShop.GetComponent<Popup> ().Open ();
	}


}
