﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePopupController : MonoBehaviour {
	public GameObject img_oven;
	public GameObject img_stove;
	public GameObject img_warmstation;
	public GameObject img_speedup;
	public GameObject btn_confirm;
	public GameObject btn_no;
	public Text txt_description;
	int selectionId = 0;


	public void InitUpgradePopup(int nItemKind) {
		selectionId = nItemKind;
		switch (nItemKind) {
		case 0:
			img_oven.SetActive (true);
			txt_description.text = "Do you want to purchase new oven?";
			break;
		case 1:
			img_oven.SetActive (true);
			txt_description.text = "Your coin is not enough to purchase new oven.";
			btn_confirm.SetActive (false);
			btn_no.SetActive (false);
			break;
		case 2:
			img_stove.SetActive (true);
			txt_description.text = "Do you want to purchase new stove?";
			break;
		case 3:
			img_stove.SetActive (true);
			txt_description.text = "Your coin is not enough to purchase new stove.";
			btn_confirm.SetActive (false);
			btn_no.SetActive (false);
			break;
		case 4:
			img_warmstation.SetActive (true);
			txt_description.text = "Do you want to purchase new warmer?";
			break;
		case 5:
			img_warmstation.SetActive (true);
			txt_description.text = "Your coin is not enough to purchase new warmer.";
			btn_confirm.SetActive (false);
			btn_no.SetActive (false);
			break;
		case 6: 
			img_speedup.SetActive (true);
			txt_description.text = "Do you want to upgrade chef speed up?";
			break;
		}
	}

	public void ConfirmSelection() {
		switch (selectionId) {
		case 0:
			ShopPopupManager.Instance ().OnPressOvenBtn ();
			break;
		case 1:
			ShopPopupManager.Instance ().OnPressOvenBtn ();
			break;
		case 2:
			ShopPopupManager.Instance ().OnPressStoveBtn ();
			break;
		case 3:
			ShopPopupManager.Instance ().OnPressStoveBtn ();
			break;
		case 4:
			ShopPopupManager.Instance ().OnPressWarmStation ();
			break;
		case 5:
			ShopPopupManager.Instance ().OnPressWarmStation ();
			break;
		case 6: 
			ShopPopupManager.Instance ().OnPressChefSpeedUp ();
			break;
		}
		gameObject.SendMessage ("Close");
	}
}
