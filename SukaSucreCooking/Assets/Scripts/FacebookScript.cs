﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using System.IO;


public class FacebookScript : MonoBehaviour {

	public RawImage profileThumb;
	private void Awake() {
		if (!FB.IsInitialized) {
			FB.Init (() => {
				if (FB.IsInitialized)
					FB.ActivateApp ();
				else
					Debug.LogError ("Couldn't initialize");
			},
				isGameShown => {
					if (!isGameShown)
						Time.timeScale = 0;
					else
						Time.timeScale = 1;
				}
			);
		} else
			FB.ActivateApp ();
	}
		

	#region Login / Logout
	public void FacebookLogin() {
		var permissions = new List<string>() {
			"public_profile", "email"
		};
		FB.LogInWithReadPermissions (permissions, AuthCallBack);
	}

	void AuthCallBack(IResult result) {
		if (result.Error != null) {
			Debug.Log (result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB is logged in");
			} else {
				Debug.Log ("FB is not logged in");
			}
			DealWithFBMenus (FB.IsLoggedIn);
		}
	}

	void DealWithFBMenus(bool isLoggedIn) {
		if (isLoggedIn) {
			//FB.API (Util.GetPictureURL("me", 128, 128), HttpMethod.GET, SaveProfileImage);
			FB.API ("/me?fields=name,id,locale", HttpMethod.GET, SaveUserName, new Dictionary<string, string>(){});

		} else {
			Debug.Log ("FB login error");
		}

	}

	void SaveUserName(IResult result) {
		GameFlags.userName = "" + result.ResultDictionary ["name"];
		//GameFlags.email = "" + result.ResultDictionary["email"];
		GameFlags.locale = "" + result.ResultDictionary["locale"];
		//Debug.Log ("--->locale: " + GameFlags.locale);
		GameFlags.email = "" + result.ResultDictionary["id"] + "@fbId.com";
		UploadSignupData ();

	}

	void SaveProfileImage(IGraphResult result) {
		if (result.Texture != null) {
//			string savePath = Application.persistentDataPath + "/savedImage.png";
//			byte[] bytes = result.Texture.EncodeToPNG ();
//			File.WriteAllBytes (savePath, bytes);
//			PlayerPrefs.SetString ("AvatarPath", savePath);
//			profileThumb.texture = LevelSceneManager.Instance ().LoadTexture (PlayerPrefs.GetString("AvatarPath"));

		}
		//UploadSignupData ();
	}

	void UploadSignupData() {
		string username = GameFlags.userName;
		string email = GameFlags.email;
		string password = "" + Random.Range (111111, 999999);
		//int countryCode = GetCountryID (GameFlags.locale);
		int countryCode = 111;
		Debug.Log ("signup Success");
		ApiHandler.Instance ().PostRegisterData (username, email, password, countryCode);

	}

	int GetCountryID(string locale) {
		int countryId = 111;
		locale = locale.ToLower ();
		string[] subLocales = locale.Split ('_');

		switch (subLocales[1]) {
		case "af":
			countryId = 0;
			break;
		case "al":
			countryId = 1;
			break;
		case "dz":
			countryId = 2;
			break;

		case "ad":
			countryId = 3;
			break;

		case "ao":
			countryId = 4;
			break;

		case "ag":
			countryId = 5;
			break;

		case "ar":
			countryId = 6;
			break;

		case "am":
			countryId = 7;
			break;

		case "aw":
			countryId = 8;
			break;

		case "au":
			countryId = 9;
			break;

		case "at":
			countryId = 10;
			break;

		case "az":
			countryId = 11;
			break;

		case "bs":
			countryId = 12;
			break;

		case "bh":
			countryId = 13;
			break;

		case "bd":
			countryId = 14;
			break;

		case "bb":
			countryId = 15;
			break;

		case "by":
			countryId = 16;
			break;

		case "be":
			countryId = 17;
			break;

		case "bz":
			countryId = 18;
			break;

		case "bj":
			countryId = 19;
			break;

		case "bt":
			countryId = 20;
			break;

		case "bo":
			countryId = 21;
			break;

		case "ba":
			countryId = 22;
			break;

		case "bw":
			countryId = 23;
			break;

		case "br":
			countryId = 24;
			break;

		case "bn": //25
			countryId = 25;
			break;

		case "bg":
			countryId = 26;
			break;

		case "bf":
			countryId = 27;
			break;

		case "mm":
			countryId = 28;
			break;

		case "bi":
			countryId = 29;
			break;

		case "kh":
			countryId = 30;
			break;

		case "cm":
			countryId = 31;
			break;

		case "ca":
			countryId = 32;
			break;

		case "cv":
			countryId = 33;
			break;

		case "cf":
			countryId = 34;
			break;

		case "td":
			countryId = 35;
			break;

		case "cl":
			countryId = 36;
			break;

		case "cn":
			countryId = 37;
			break;

		case "co":
			countryId = 38;
			break;

		case "km":
			countryId = 39;
			break;

		case "cd":
			countryId = 40;
			break;

		case "cg":
			countryId = 41;
			break;

		case "cr":
			countryId = 42;
			break;

		case "ci":
			countryId = 43;
			break;

		case "hr":
			countryId = 44;
			break;

		case "cu":
			countryId = 45;
			break;

		case "cw":
			countryId = 46;
			break;

		case "cy":
			countryId = 47;
			break;

		case "cz":
			countryId = 48;
			break;

		case "dk":
			countryId = 49;
			break;

		case "dj"://50
			countryId = 50;
			break;
		case "dm":
			countryId = 51;
			break;

		case "do":
			countryId = 52;
			break;

		case "tl":
			countryId = 53;
			break;

		case "ec":
			countryId = 54;
			break;

		case "eg":
			countryId = 55;
			break;

		case "sv":
			countryId = 56;
			break;

		case "gq":
			countryId = 57;
			break;

		case "er":
			countryId = 58;
			break;

		case "ee":
			countryId = 59;
			break;

		case "et":
			countryId = 60;
			break;

		case "fj":
			countryId = 61;
			break;

		case "fi":
			countryId = 62;
			break;

		case "fr":
			countryId = 63;
			break;

		case "ga":
			countryId = 64;
			break;

		case "gm":
			countryId = 65;
			break;
		case "ge":
			countryId = 66;
			break;
		case "de":
			countryId = 67;
			break;
		case "gh":
			countryId = 68;
			break;
		case "gr":
			countryId = 69;
			break;
		case "gd":
			countryId = 70;
			break;
		case "gt":
			countryId = 71;
			break;
		case "gn":
			countryId = 72;
			break;
		case "gw":
			countryId = 73;
			break;
		case "gy":
			countryId = 74;
			break;
		case "ht": //75
			countryId = 75;
			break;
		case "va":
			countryId = 76;
			break;
		case "hn":
			countryId = 77;
			break;
		case "hk":
			countryId = 78;
			break;
		case "hu":
			countryId = 79;
			break;
		case "is":
			countryId = 80;
			break;
		case "in":
			countryId = 81;
			break;
		case "id":
			countryId = 82;
			break;
		case "ir":
			countryId = 83;
			break;
		case "iq":
			countryId = 84;
			break;
		case "ie":
			countryId = 85;
			break;
		case "il":
			countryId = 86;
			break;
		case "it":
			countryId = 87;
			break;
		case "jm":
			countryId = 88;
			break;
		case "jp":
			countryId = 89;
			break;
		case "jo":
			countryId = 90;
			break;
		case "kz":
			countryId = 91;
			break;
		case "ke":
			countryId = 92;
			break;
		case "ki":
			countryId = 93;
			break;
		case "kp":
			countryId = 94;
			break;
		case "kr":
			countryId = 95;
			break;
		case "kv":
			countryId = 96;
			break;
		case "kw":
			countryId = 97;
			break;
		case "kg":
			countryId = 98;
			break;
		case "la":
			countryId = 99;
			break;
		case "lv"://100
			countryId = 100;
			break;
		case "lb":
			countryId = 101;
			break;
		case "ls":
			countryId = 102;
			break;
		case "lr":
			countryId = 103;
			break;
		case "ly":
			countryId = 104;
			break;
		case "li":
			countryId = 105;
			break;
		case "lu":
			countryId = 106;
			break;
		case "mo":
			countryId = 107;
			break;
		case "mk":
			countryId = 108;
			break;
		case "mg":
			countryId = 109;
			break;
		case "mw":
			countryId = 110;
			break;
			///111
		case "my":
			countryId = 111;
			break;
		case "mv":
			countryId = 112;
			break;
		case "ml":
			countryId = 113;
			break;
		case "mt":
			countryId = 114;
			break;
		case "mh":
			countryId = 115;
			break;
		case "mr":
			countryId = 116;
			break;
		case "mu":
			countryId = 117;
			break;
		case "mx":
			countryId = 118;
			break;
		case "fm":
			countryId = 119;
			break;
		case "md":
			countryId = 120;
			break;
		case "mc":
			countryId = 121;
			break;
		case "mn":
			countryId = 122;
			break;
		case "me":
			countryId = 123;
			break;
		case "ma":
			countryId = 124;
			break;
		case "mz"://125
			countryId = 125;
			break;
		case "na":
			countryId = 126;
			break;
		case "nr":
			countryId = 127;
			break;
		case "np":
			countryId = 128;
			break;
		case "nl":
			countryId = 129;
			break;
		case "nz":
			countryId = 130;
			break;
		case "ni":
			countryId = 131;
			break;
		case "ne":
			countryId = 132;
			break;
		case "no":
			countryId = 133;
			break;
		case "om":
			countryId = 134;
			break;
		case "pk":
			countryId = 135;
			break;
		case "pw":
			countryId = 136;
			break;
		case "ps":
			countryId = 137;
			break;
		case "pa":
			countryId = 138;
			break;
		case "pg":
			countryId = 139;
			break;
		case "py":
			countryId = 140;
			break;
		case "pe":
			countryId = 141;
			break;
		case "ph":
			countryId = 142;
			break;
		case "pl":
			countryId = 143;
			break;
		case "pt":
			countryId = 144;
			break;
		case "qa":
			countryId = 145;
			break;
		case "ro":
			countryId = 146;
			break;
		case "ru":
			countryId = 147;
			break;
		case "rw":
			countryId = 148;
			break;
		case "kn":
			countryId = 149;
			break;
		case "lc"://150
			countryId = 150;
			break;
		case "vc":
			countryId = 151;
			break;
		case "ws":
			countryId = 152;
			break;
		case "sm":
			countryId = 153;
			break;
		case "st":
			countryId = 154;
			break;
		case "sa":
			countryId = 155;
			break;
		case "sn":
			countryId = 156;
			break;
		case "rs":
			countryId = 157;
			break;
		case "sc":
			countryId = 158;
			break;
		case "sl":
			countryId = 159;
			break;
		case "sg":
			countryId = 160;
			break;
		case "sx":
			countryId = 161;
			break;
		case "sk":
			countryId = 162;
			break;
		case "si":
			countryId = 163;
			break;
		case "sb":
			countryId = 164;
			break;
		case "so":
			countryId = 165;
			break;
		case "za":
			countryId = 166;
			break;
		case "ss":
			countryId = 167;
			break;
		case "es":
			countryId = 168;
			break;
		case "lk":
			countryId = 169;
			break;
		case "sd":
			countryId = 170;
			break;
		case "sr":
			countryId = 171;
			break;
		case "sz":
			countryId = 172;
			break;
		case "se":
			countryId = 173;
			break;
		case "ch":
			countryId = 174;
			break;
		case "sy"://175
			countryId = 175;
			break;
		case "tw":
			countryId = 176;
			break;
		case "tj":
			countryId = 177;
			break;
		case "tz":
			countryId = 178;
			break;
		case "th":
			countryId = 179;
			break;
		case "tl1":
			countryId = 180;
			break;
		case "tg":
			countryId = 181;
			break;
		case "to":
			countryId = 182;
			break;
		case "tt":
			countryId = 183;
			break;
		case "tn":
			countryId = 184;
			break;
		case "tr":
			countryId = 185;
			break;
		case "tm":
			countryId = 186;
			break;
		case "tv":
			countryId = 187;
			break;
		case "ug":
			countryId = 188;
			break;
		case "ua":
			countryId = 189;
			break;
		case "ae":
			countryId = 190;
			break;
		case "gb":
			countryId = 191;
			break;
		case "uy":
			countryId = 192;
			break;
		case "uz":
			countryId = 193;
			break;
		case "us":
			countryId = 194;
			break;
		case "vu":
			countryId = 195;
			break;
		case "ve":
			countryId = 196;
			break;
		case "vn":
			countryId = 197;
			break;
		case "ye":
			countryId = 198;
			break;
		case "zm":
			countryId = 199;
			break;
		case "zw"://200
			countryId = 200;
			break;

		}
		return countryId;
	}

	public void FacebookLogOut() {
		FB.LogOut ();
	}
	#endregion

	public void FacebookShareLogin() {
		var permissions = new List<string>() {
			"public_profile"
		};
		FB.LogInWithReadPermissions (permissions, AuthShareCallBack);
	}

	void AuthShareCallBack(IResult result) {
		if (result.Error != null) {
			Debug.Log (result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB is logged in");
				FacebookShare ();
			} else {
				Debug.Log ("FB is not logged in");
			}

		}
	}

	public void FacebookShare() {
		string shareStr = "Hi, Guys! I've completed stage " + GameFlags.nCurrentRound + " with " + GameFlags.nStars + " stars in " +
		                  "Chef Fazley's Kitchen app." +
		                  " Wish me luck for the contest by Chef Fazley!";

		FB.FeedShare (
			link: new System.Uri("http://kometdigital.com/games/chef"),
			linkName: shareStr,
			linkCaption: "Chef Fazley's kitchen",
			linkDescription: shareStr,
			picture: new System.Uri("http://kometdigital.com/games/chef/shared.png")
		);

//		int coinCount = PlayerPrefs.GetInt ("Coin",0);
//		PlayerPrefs.SetInt ("Coin", coinCount + 100);
//		Debug.Log ("GameObject Name:" + gameObject.name);
//		if (PlayerPrefs.HasKey ("encPass")) {
//			ApiHandler.Instance ().UpdateChip ((long)(coinCount + 100));
//			ApiHandler.Instance ().SendGameLog (100, 0, 0);
//		}
		gameObject.SendMessage ("DisableShareButton");
	}
		
}
