// Copyright (C) 2015, 2016 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms.

using UnityEngine;
using UnityEngine.UI;

namespace Ricimi
{
    // Specialized version of the PopupOpener class that opens the PlayPopup popup
    // and sets an appropriate number of stars (that can be configured from within the
    // editor).
    public class PlayPopupOpener : PopupOpener
    {
		public GameObject popupLock;
        public int starsObtained = 0;
		public GameObject objLock;
		public Image imgRoundBtn;
//		public Sprite[] arr_sprites;
		public Text txt_round_label;
		public int nRoundId;
		public GameObject[] goldStars;
		public GameObject[] greyStars;
		public bool bLock;

        public override void OpenPopup()
        {
			if (!bLock) {
				var popup = Instantiate (popupPrefab) as GameObject;
				popup.SetActive (true);
				popup.transform.localScale = Vector3.zero;
				popup.transform.SetParent (m_canvas.transform, false);

				var playPopup = popup.GetComponent<PlayRoundPopup> ();
				playPopup.Open ();
				playPopup.SetAchievedStars (starsObtained);
				playPopup.InitRound (nRoundId, starsObtained);
				GameFlags.nCurrentRound = nRoundId;

			} else {
//				var popupLockObj = Instantiate (popupLock) as GameObject;
//				popupLockObj.SetActive (true);
//				popupLockObj.transform.localScale = Vector3.zero;
//				popupLockObj.transform.SetParent (m_canvas.transform, false);
//
//				var playPopup = popupLockObj.GetComponent<Popup> ();
//				playPopup.Open ();
				//playPopup.SetAchievedStars (starsObtained);
			}
        }

		public void InitRoundBtn(bool bLock, int nStar, int nRoundNum) {
			starsObtained = nStar;
			nRoundId = nRoundNum;
			objLock.SetActive (bLock);
			this.bLock = bLock;
			txt_round_label.text = "" + nRoundId;
//			if (bLock) {
//				for (int i = 0; i < 3; i++) {
//					goldStars [i].SetActive (false);
//					greyStars [i].SetActive (true);
//					txt_round_label.text = "";
//					//imgRoundBtn.sprite = arr_sprites [5];
//				}
//			} else 
			{
				for (int j = 0; j < nStar; j++) {
					goldStars [j].SetActive (true);
					greyStars [j].SetActive (false);
				}
				for (int k = nStar; k < 3; k++) {
					goldStars [k].SetActive (false);
					greyStars [k].SetActive (true);
				}
				//imgRoundBtn.sprite = arr_sprites [nRoundId % 5];
			}
		}
    }
}
