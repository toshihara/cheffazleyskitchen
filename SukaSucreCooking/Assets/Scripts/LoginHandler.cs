﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoginHandler : MonoBehaviour {
	public InputField inpt_email;
	public InputField inpt_password;
	public GameObject prefPopupError;
	GameObject popupError;
	public GameObject txt_warning;
	Transform mCanvas;

	public void Login() {
		mCanvas = GameObject.Find ("Canvas").transform;
		string user_email = inpt_email.text;
		string user_password = inpt_password.text;
		if (user_email == "") {
			popupError = Instantiate (prefPopupError) as GameObject;
			popupError.transform.parent = mCanvas;
			popupError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupError.GetComponent<PopupSignError> ().ShowPopUp (2);
		} else if (user_password.Length < 5) {
			popupError = Instantiate (prefPopupError) as GameObject;
			popupError.transform.parent = mCanvas;
			popupError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupError.GetComponent<PopupSignError> ().ShowPopUp (5);
		} else {
			ApiHandler.Instance ().LoginGameAccount (user_email, user_password);
		}
	}

	public void ShowWarning() {
		if (inpt_email.text == "") {
			txt_warning.SetActive (false);
		} else {
			txt_warning.SetActive (true);
		}
	}
}
