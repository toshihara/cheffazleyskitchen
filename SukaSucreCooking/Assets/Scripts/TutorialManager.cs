﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {
	public GameObject[] objArrows;
	public Animator aniTutorialPanel;
	public Animator[] aniTutorialContents;
	public int nTutorialIndex = 0;
	public static TutorialManager tutorialManager;
	float fHideProcessTimer = 2f;
	bool bTriggerLock = false;
	bool bFlagShow = false;
	public static TutorialManager Instance() {
		if (!tutorialManager) {
			tutorialManager = FindObjectOfType (typeof (TutorialManager)) as TutorialManager;
		}
		return tutorialManager;
	}

	// Use this for initialization
	void Start () {
		Debug.Log ("nCurrentRound: " + GameFlags.nCurrentRound);
		Debug.Log ("pref Tutorial: " + PlayerPrefs.GetInt ("Tutorial", 0));
		//if (PlayerPrefs.GetInt ("Tutorial", 0) == 0) {
			if (GameFlags.nCurrentRound == 1) {
				GameFlags.bTutorial = true;
				//CustomerManager.Instance ().PopTutorialChair ();
			}
		//}
	}
	
	// Update is called once per frame
	void Update () {
		if (fHideProcessTimer < 4f) {
			fHideProcessTimer += Time.deltaTime;
		}
	}

	public void HandleTutorialEvent() {
		switch (nTutorialIndex) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			break;
		}
	}

	public void ShowTutorial(int nIndex) {
		if (bFlagShow)
			return;
		if (nIndex != nTutorialIndex)
			return;
		if (bTriggerLock)
			return;
		GameFlags.bPauseGame = true;
		bFlagShow = true;
		aniTutorialPanel.Play ("FadeIn");
		switch (nTutorialIndex) {
		case 0:
			StartCoroutine (EnableTargetObject(aniTutorialContents[0].gameObject, 1f));
			break;
		case 1:
			StartCoroutine (EnableTargetObject (aniTutorialContents [1].gameObject, 1f));
			StartCoroutine(EnableTargetObject (objArrows[0], 2f));
			break;
		case 2:
			StartCoroutine (EnableTargetObject(aniTutorialContents[2].gameObject, 1f));
			StartCoroutine(EnableTargetObject (objArrows[1], 2f));
			break;
		case 3:
			StartCoroutine (EnableTargetObject(aniTutorialContents[3].gameObject, 1f));
			StartCoroutine(EnableTargetObject (objArrows[1], 2f));
			break;
		case 4:
			StartCoroutine (EnableTargetObject(aniTutorialContents[4].gameObject, 1f));
			StartCoroutine(EnableTargetObject (objArrows[2], 2f));
			break;
		case 5:
			StartCoroutine (EnableTargetObject(aniTutorialContents[5].gameObject, 1f));
			StartCoroutine(EnableTargetObject (objArrows[3], 2f));
			break;
		case 6:
			StartCoroutine (EnableTargetObject(aniTutorialContents[6].gameObject, 1f));
			break;
		}
	}

	public void HideArrow() {
		if (nTutorialIndex == 2) {
			objArrows [0].SetActive (false);
		} else if (nTutorialIndex == 3 || nTutorialIndex == 4) {
			objArrows [1].SetActive (false);
		} else if (nTutorialIndex == 5) {
			objArrows [2].SetActive (false);
		} else if (nTutorialIndex == 6) {
			objArrows [3].SetActive (false);
		}

	}

	IEnumerator EnableTargetObject (GameObject obj, float fDelayTime) {
		yield return new WaitForSeconds(fDelayTime);
		obj.SetActive(true);
	}

	IEnumerator DisablePrevObject (GameObject obj, float fDelayTime) {
		yield return new WaitForSeconds (fDelayTime);
		obj.SetActive (false);
		bTriggerLock = false;
	}

	IEnumerator DelayAnimation(Animator ani, string stateName, float fDelayTime) {
		yield return new WaitForSeconds (fDelayTime);
		ani.Play (stateName);
	}

	IEnumerator UpdateGamePauseFlag(float fDelayTime) {
		yield return new WaitForSeconds (fDelayTime);
		GameFlags.bPauseGame = false;
		if (nTutorialIndex == 7)
			GameFlags.bTutorial = false;
	}

	IEnumerator TriggerTutorial_1() {
		yield return new WaitForSeconds (3f);
		ShowTutorial (1);
	}

	public void HideTutorial() {
		if (fHideProcessTimer < 4f)
			return;
		bTriggerLock = true;
		fHideProcessTimer = 0;
		StartCoroutine (UpdateGamePauseFlag(2f));
		StartCoroutine (DelayAnimation(aniTutorialPanel, "FadeOut", 1f));
		bFlagShow = false;
		switch (nTutorialIndex) {
		case 0:
			StartCoroutine (DisablePrevObject (aniTutorialContents [0].gameObject, 1f));
			aniTutorialContents [0].Play ("FadeOut");
			StartCoroutine (TriggerTutorial_1());
			break;
		case 1:
			StartCoroutine (DisablePrevObject (aniTutorialContents [1].gameObject, 1f));
			aniTutorialContents [1].Play ("FadeOut");
			break;
		case 2:
			StartCoroutine (DisablePrevObject (aniTutorialContents [2].gameObject, 1f));
			aniTutorialContents [2].Play ("FadeOut");
			break;
		case 3:
			StartCoroutine (DisablePrevObject (aniTutorialContents [3].gameObject, 1f));
			aniTutorialContents [3].Play ("FadeOut");
			break;
		case 4:
			StartCoroutine (DisablePrevObject (aniTutorialContents [4].gameObject, 1f));
			aniTutorialContents [4].Play ("FadeOut");
			break;
		case 5:
			StartCoroutine (DisablePrevObject (aniTutorialContents [5].gameObject, 1f));
			aniTutorialContents [5].Play ("FadeOut");
			break;
		case 6:
			StartCoroutine (DisablePrevObject (aniTutorialContents [6].gameObject, 1f));
			aniTutorialContents [6].Play ("FadeOut");

			break;
		}

		nTutorialIndex++;
	}
}
