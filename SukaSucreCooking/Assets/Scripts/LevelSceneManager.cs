﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Ricimi;

public class LevelSceneManager : MonoBehaviour {
	public static LevelSceneManager levelSceneManager;
	public GameManager gameManager;

	public Animator aniRoundPanel;
	public GameObject pnl_round_sample;
	public List<GameObject> list_rounds;
	public GameObject pnl_round_selection;

	public GameObject welcomePanel;
	public GameObject loginPanel;
	public GameObject signUpPanel;

	public SignUpPopUP signUPPopUP;
	public GameObject btn_profile;
	public RawImage profilethumb;
	public GameObject prefProfilePopup;
	GameObject profilePopup;
	public Text totalCoin;
	public Text txt_personal_rank;

	public Transform mCanvas;

	public GameObject fxCloud;
	public GameObject fxFireworks;
	public Text txt_dbg;
	public GameObject prefPopupSignError;
	GameObject popupSignError;

	public static LevelSceneManager Instance() {
		if (!levelSceneManager) {
			levelSceneManager = FindObjectOfType (typeof(LevelSceneManager)) as LevelSceneManager;
		}
		return levelSceneManager;
	}

	// Use this for initialization
	void Start () {
		DisplayCoin ();
		GenerateRoundPanel ();
		GetRankInfo ();

		//VoiceManager.Instance ().PlayLevelSelectionVoice ();
		StartCoroutine (PlayBGMusic());
	}

	IEnumerator PlayBGMusic() {
		yield return new WaitForSeconds (2f);
		BGSoundManager.Instance ().PlaySound (0);
	}

	public void GetRankInfo() {
		if (PlayerPrefs.HasKey ("encPass")) {
			ApiHandler.Instance ().GetPersonalGameRank ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnRoundSelected(int nRound) {
		GameFlags.nCurrentRound = nRound;
	}

	public void OnActivateFX() {
		fxCloud.SetActive (true);
		fxFireworks.SetActive (true);
	}

	public void RoundPanelOpen() {
		if (!aniRoundPanel.gameObject.activeSelf)
			aniRoundPanel.gameObject.SetActive (true);
		int nAvailRound = PlayerPrefs.GetInt ("AvailRound", 1);
		float fPosY = ((nAvailRound - 1) / 5) * 220f;
		pnl_round_selection.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, fPosY);

		aniRoundPanel.SetTrigger ("OpenRoundPanel");
	}

	public void RoundPanelClose() {
		aniRoundPanel.SetTrigger ("HideRoundPanel");
		StartCoroutine (DeactivateRoundPanel());
	}

	IEnumerator DeactivateRoundPanel() {
		yield return new WaitForSeconds (1f);
		aniRoundPanel.gameObject.SetActive (false);
		pnl_round_selection.transform.localScale = Vector3.one;
		pnl_round_selection.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
	}

	public void OnPressPlay() {
		gameManager.PlayGame ();
	}

	public void GenerateRoundPanel() {
		if (!PlayerPrefs.HasKey ("userName")) {
			//signUpPanel.SetActive (true);
			welcomePanel.SetActive(true);
			btn_profile.SetActive (false);
		} else {
			welcomePanel.SetActive (false);
			btn_profile.SetActive (true);
		}
		StartCoroutine (InstantiateRoundsList());
	}

	public void ShowSignupPopup() {
		signUpPanel.SetActive (true);
		StartCoroutine(HideWelcome());
	}

	public void ShowLoginPopup() {
		loginPanel.SetActive (true);
		StartCoroutine(HideWelcome());
	}

	IEnumerator HideWelcome() {
		yield return new WaitForSeconds (0.5f);
		welcomePanel.SetActive (false);
	}

	public Texture2D LoadTexture(string filePath) {
		byte[] bytes = File.ReadAllBytes (filePath);
		Texture2D texture = new Texture2D (2,2);
		texture.filterMode = FilterMode.Bilinear;
		texture.LoadImage (bytes);
		return texture;
	}

	public void RefreshRoundsList(int nLevel) {
		//txt_personal_rank.text = "stage: " + PlayerPrefs.GetInt ("AvailRound");
		StartCoroutine (InstantiateRoundsList());

	}

	IEnumerator InstantiateRoundsList() {
		//----test purpose---
		//PlayerPrefs.SetInt("AvailRound", 100);
		//----
		for (int i = 0; i < 100; i++) {
			GameObject roundPanel = list_rounds [i];

			roundPanel.transform.localScale = Vector3.one * 0.8f;
			string strRoundKey = "Round_Achieve_1_" + (i + 1);
			string strRoundDateCount = "Round_Dialy_Play_1_" + (i + 1);
			string strRoundRecentDate = "Round_Play_Recent_1_" + (i + 1);

			if (!PlayerPrefs.HasKey("AvailRound"))
				PlayerPrefs.SetInt ("AvailRound", 1);

			if (PlayerPrefs.GetInt ("AvailRound", 1) > i + 1 ) {
				if (!PlayerPrefs.HasKey (strRoundKey)) {
					roundPanel.GetComponent<PlayPopupOpener>().InitRoundBtn(false, 0, i+1);
				} else {
					int nStar = PlayerPrefs.GetInt (strRoundKey);
					string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
					if (PlayerPrefs.HasKey (strRoundRecentDate)) {
						if (currentDate != PlayerPrefs.GetString (strRoundRecentDate)) {
							PlayerPrefs.SetString (strRoundRecentDate, currentDate);
							PlayerPrefs.SetInt (strRoundDateCount, 0);
						}
					}

					int diaryCount = PlayerPrefs.GetInt (strRoundDateCount, 0);

					if (diaryCount < GameFlags.MAX_PLAYING_COUNT)
						roundPanel.GetComponent<PlayPopupOpener>().InitRoundBtn(false, nStar, i+1);
					else
						roundPanel.GetComponent<PlayPopupOpener>().InitRoundBtn(true, nStar, i+1);
				}
			}
			else if (PlayerPrefs.GetInt ("AvailRound", 1) == i + 1 ) {
				roundPanel.GetComponent<PlayPopupOpener>().InitRoundBtn(false, 0, i+1);
			}
			else{
				roundPanel.GetComponent<PlayPopupOpener>().InitRoundBtn(true, 0, i+1);
			}
			yield return null;
		}
		int nAvailRound = PlayerPrefs.GetInt ("AvailRound", 1);
		float fPosY = (nAvailRound / 5) * 220f;
		pnl_round_selection.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, fPosY);
		OnActivateFX ();
	}

	public void ActivateSignUpPanel() {
		signUpPanel.SetActive (true);
	}

	public void StoreAccountInfo(bool bNew=true) {
		if (PlayerPrefs.HasKey ("Coin"))
		{
			int nCoin = PlayerPrefs.GetInt ("Coin");
			//PlayerPrefs.SetInt ("Coin", nCoin);
			long lCoin = (long)nCoin;
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lCoin);

			}
			DisplayCoin ();
		}

		btn_profile.SetActive (true);
		if (signUpPanel.activeSelf)
			CloseSignupPopup ();
		if (loginPanel.activeSelf)
			CloseLoginPopup ();
		if (welcomePanel.activeSelf)
			CloseWelcomePopup ();	
		if (bNew)
			ShowErrorPopup(6);
	}

	public void CloseSignupPopup() {
		signUpPanel.GetComponent<Animator> ().SetTrigger ("CloseSignUP");
		StartCoroutine (DeactivateSignup());
		welcomePanel.SetActive (true);
	}

	IEnumerator DeactivateSignup() {
		yield return new WaitForSeconds (1f);
		signUpPanel.SetActive (false);
	}

	public void CloseWelcomePopup() {
		welcomePanel.GetComponent<Animator> ().SetTrigger ("CloseSignUP");
		StartCoroutine (DeactivateWelcome());
	}

	IEnumerator DeactivateWelcome() {
		yield return new WaitForSeconds (1f);
		welcomePanel.SetActive (false);
	}

	public void CloseLoginPopup() {
		loginPanel.GetComponent<Animator> ().SetTrigger ("CloseSignUP");
		StartCoroutine (DeactivateLogin());
		welcomePanel.SetActive (true);
	}

	IEnumerator DeactivateLogin() {
		yield return new WaitForSeconds (1f);
		loginPanel.SetActive (false);
	}


	public void HandleLoginResult(int nResult) {

	}

	public void HandleRegisterResult(int nResult) {
		switch(nResult) {
		case 0:
			StoreAccountInfo ();
			break;
		case 1:
			signUPPopUP.ShowErrorPopup (1);
			break;
		case 2:
			signUPPopUP.ShowErrorPopup (2);
			break;
		case 3:
			signUPPopUP.ShowErrorPopup (3);
			break;
		case 4:
			signUPPopUP.ShowErrorPopup (4);
			break;
		case 5: 
			signUPPopUP.ShowErrorPopup (5);
			break;
		}
	}

	public void HandleUpdateEmail(int nResult) {
		switch (nResult) {
		case 0:
			PlayerPrefs.SetString ("Email", GameFlags.email);
			break;
		case 1:
			signUPPopUP.ShowErrorPopup (1);
			break;
		case 2:
			signUPPopUP.ShowErrorPopup (4);
			break;
		case 3:
			signUPPopUP.ShowErrorPopup (3);
			break;
		}
	}

	public void ShowProfilePopup() {
		profilePopup = Instantiate (prefProfilePopup) as GameObject;
		profilePopup.transform.parent = mCanvas;
		profilePopup.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -225f);
		profilePopup.GetComponent<Popup> ().Open ();
	}

	public void CloseProfilePopup() {
		profilePopup.GetComponent<Popup> ().Close ();
	}

	public void UpdateProfileThumb(Texture2D tex) {
		profilethumb.texture = tex;
	}

	public void ActivateVisualEffects() {
		fxCloud.SetActive (true);
		fxFireworks.SetActive (true);
	}

	public void DeactivateVisualEffects() {
		fxCloud.SetActive (false);
		fxFireworks.SetActive (false);
	}

	public void DisplayCoin() {
		totalCoin.text = "" + PlayerPrefs.GetInt ("Coin", 0);
	}

	public void BuyNoAds() {
		CoinPurchaseScript.Instance ().BuyNoAds ();
	}

	public void ShowErrorPopup(int nError) {
		switch (nError) {
		case 1:
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (6);
			break;
		case 2: //user exsit
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (8);
			break;
		case 3://email exist
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (3);
			break;
		case 4: // email format
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (7);
			break;
		case 5:
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (5);
			break;
		case 6:
			popupSignError = Instantiate (prefPopupSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (9);
			break;

		}
	}

	public void FlagFBLogin() {
		GameFlags.bFBLogin = true;
	}

	public void ShowDbgLog(string strDbg) {
		txt_dbg.text += "\n" + strDbg;
	}
		
}


