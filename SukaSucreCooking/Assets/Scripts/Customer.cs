﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour {
	public int nCustomerIndex;
	public SpriteRenderer sprite_customer;
	public Sprite[] sprite_characters;
	public Image img_waiting_bar;
	public Image img_waiting_bar_back;
	public GameObject dialogFood;
	float fTimeArrive;
	float fTimeRead;
	float fTimeWait;
	float fTimeEat;
	public CustomerProperty customerProperty;
	public CustomerStatus 	customerStatus;
	public CustomerManager customerManager;
	public Animator aniChair;
	public string orderedFood = "Plain Macaron";
	public Sprite[] sprite_cooks;
	public Sprite[] sprite_emos;
	public Image img_emoji;
	public SpriteRenderer orderedCookImage;
	public RectTransform pnl_time_mask;
	public GameObject canvasTimeBar;
	public WaitingTableController waitingTable;
	int foodScore = 0;
	int nCustomerProperty = 0;
	public Transform coinObjSpawner;
	public GameObject prefCoinObj;
	int nStep = 0;
	// Use this for initialization
	void Start () {
		InitCustomer ();
	}

	public void InitCustomer() {
//		img_waiting_bar.gameObject.SetActive (false);
//		img_waiting_bar_back.gameObject.SetActive (false);
		pnl_time_mask.sizeDelta = new Vector2(pnl_time_mask.sizeDelta.x, GameFlags.fEmojiGuage);
		img_emoji.sprite = sprite_emos[0];
		canvasTimeBar.SetActive(false);

		sprite_customer.enabled = false;
		dialogFood.SetActive (false);
		this.fTimeArrive = 0;
		this.fTimeEat = 0;
		this.fTimeRead = 0;
		this.fTimeWait = 0;
		customerStatus = CustomerStatus.Gone;
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameFlags.bPauseGame)
			CharacterBehavior ();
	}

	public void InitCharacter () {
		StartCoroutine (OpenChair());
	}

	IEnumerator OpenChair() {
		//coming in
		int nStep = 0;
		SoundManager.Instance ().PlaySound (1);
		aniChair.SetTrigger ("OpenChair");
		yield return new WaitForSeconds (1f);
		nCustomerProperty = Random.Range (0, 8);
		SetProperty (nCustomerProperty);
		customerStatus = CustomerStatus.ReadingMenu;
		sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5];
		sprite_customer.enabled = true;
		dialogFood.SetActive (false);
		fTimeRead = 0;
	}

	void SetProperty(int nCustomerProp) {
		switch (nCustomerProp) {
		case 0:
			customerProperty = customerManager.Adam;
			break;
		case 1:
			customerProperty = customerManager.Alia;
			break;
		case 2:
			customerProperty = customerManager.Suresh;
			break;
		case 3:
			customerProperty = customerManager.Stacy;
			break;
		case 4:
			customerProperty = customerManager.Faidaus;
			break;
		case 5:
			customerProperty = customerManager.Adri;
			break;
		case 6:
			customerProperty = customerManager.Anom;
			break;
		case 7:
			customerProperty = customerManager.Mikey;
			break;
		}
		//sprite_customer.sprite = sprite_characters[nCustomerProp];

	}

	void OrderFood() {
		//order sound:
		SoundManager.Instance().PlaySound(3);
		int roundCookCounter = FoodMaterialController.Instance ().roundCookList.Count;
		this.orderedFood = FoodMaterialController.Instance ().roundCookList [0].cookName;
		if (roundCookCounter > 1) {
			int nRandCook = Random.Range (0, roundCookCounter);
			this.orderedFood = FoodMaterialController.Instance ().roundCookList [nRandCook].cookName;
			sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5 + 1];
		}
		SetOrderedCookSprite (orderedFood);
	}

	void SetOrderedCookSprite(string cookName) {
		int nSpriteCook = 0;
		switch (cookName) {
		case "Plain Macaron":
			nSpriteCook = 0;
			foodScore = 7;
			break;
		case "Chocolate Macaron":
			nSpriteCook = 1;
			foodScore = 12;
			break;
		case "Chocolate Caramel Macaron":
			nSpriteCook = 2;
			foodScore = 17;
			break;
		case "Churros Plain":
			nSpriteCook = 3;
			foodScore = 7;
			break;
		case "Churros Chocolate":
			nSpriteCook = 4;
			foodScore = 12;
			break;
		case "Churros Chocolate Caramel":
			nSpriteCook = 5;
			foodScore = 17;
			break;
		case "Plain Cake":
			nSpriteCook = 6;
			foodScore = 7;
			break;
		case "Chocolate Cake":
			nSpriteCook = 7;
			foodScore = 12;
			break;
		case "Chocolate Caramel Cake":
			nSpriteCook = 8;
			foodScore = 17;
			break;
		case "Pasta Piyash":
			nSpriteCook = 9;
			foodScore = 7;
			break;
		case "Pasta Payish":
			nSpriteCook = 10;
			foodScore = 12;
			break;
		case "Pasta Daging Salai Lemak Cili Api":
			nSpriteCook = 11;
			foodScore = 17;
			break;
		case "French Butter Rice":
			nSpriteCook = 12;
			foodScore = 7;
			break;
		case "French Butter Rice With Rendang & Vegetable Pickles":
			nSpriteCook = 13;
			foodScore = 12;
			break;
		case "French Butter Rice With Daging Salai Lemak Cili Api":
			nSpriteCook = 14;
			foodScore = 17;
			break;
		case "Ratatouille Pizza":
			nSpriteCook = 15;
			foodScore = 7;
			break;
		case "Rendang Pizza":
			nSpriteCook = 16;
			foodScore = 12;
			break;
		case "Heavy Duty Meat Pizza":
			nSpriteCook = 17;
			foodScore = 17;
			break;
		case "Toasted Eclair":
			nSpriteCook = 18;
			foodScore = 7;
			break;
		case "Toasted Eclair Pulled Beef Rendang":
			nSpriteCook = 19;
			foodScore = 12;
			break;
		case "Toasted Eclair Daging Salai":
			nSpriteCook = 20;
			foodScore = 17;
			break;
		case "Americano":
			nSpriteCook = 21;
			foodScore = 7;
			break;
		case "Caramel Latte":
			nSpriteCook = 22;
			foodScore = 7;
			break;
		case "Hot Chocolate":
			nSpriteCook = 23;
			foodScore = 7;
			break;
		case "Frozen Grapes Mint Soda":
			nSpriteCook = 24;
			foodScore = 7;
			break;
		case "Soda Bandung":
			nSpriteCook = 25;
			foodScore = 7;
			break;
		case "Sucre Tea":
			nSpriteCook = 26;
			foodScore = 7;
			break;
		}
		orderedCookImage.sprite = sprite_cooks[nSpriteCook];
	}

	public void CharacterBehavior() {
		switch (customerStatus) {
		case CustomerStatus.ReadingMenu:
			if (fTimeRead < customerProperty.fTimeReadMenu)
				fTimeRead += Time.deltaTime;
			else {
				fTimeRead = 0;
				fTimeWait = 0;
				sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5 + 1];
				customerStatus = CustomerStatus.WaitingFood;
				ActivateWaitingTimeBar ();
				OrderFood ();
				dialogFood.SetActive (true);
				if (GameFlags.bTutorial) {
					TutorialManager.Instance ().ShowTutorial (0);
				}
			}
			break;
		case CustomerStatus.WaitingFood:
			if (!GameFlags.bTutorial) {
				if (fTimeWait < customerProperty.fTimeWaitingFood) {
					fTimeWait += Time.deltaTime;
					DecreaseWaitingTimeBar ();
				} else {
					LoseCustomer ();
					customerAway ();
				}
			}
			break;
		case CustomerStatus.EatingFood:
			if (fTimeEat < customerProperty.fTimeEatFood) {
				fTimeEat += Time.deltaTime;
				sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5 + 4];
			} else {
				SatisfyCustomer ();
				customerAway ();
			}
			break;
		}
	}

	public void Change2EatingState() {
		
	}

	void ActivateWaitingTimeBar() {
		pnl_time_mask.sizeDelta = new Vector2(pnl_time_mask.sizeDelta.x, GameFlags.fEmojiGuage);
		img_emoji.sprite = sprite_emos[0];
		canvasTimeBar.SetActive(true);
//		img_waiting_bar.gameObject.SetActive (true);
//		img_waiting_bar_back.gameObject.SetActive (true);
//		img_waiting_bar.gameObject.transform.localScale = new Vector3 (1f, 1f, 1f);
//		img_waiting_bar.color = Color.green;
	}

	public void DeactivateWaitingTimeBar() {
		pnl_time_mask.sizeDelta = new Vector2(pnl_time_mask.sizeDelta.x, GameFlags.fEmojiGuage);
		img_emoji.sprite = sprite_emos[0];
		canvasTimeBar.SetActive(false);
//		img_waiting_bar.gameObject.SetActive (false);
//		img_waiting_bar_back.gameObject.SetActive (false);
//		img_waiting_bar.gameObject.transform.localScale = new Vector3 (1f, 1f, 1f);
//		img_waiting_bar.color = Color.green;
	}

	void DecreaseWaitingTimeBar() {
//		img_waiting_bar.gameObject.transform.localScale = new Vector3 (1f, 1f - fTimeWait/customerProperty.fTimeWaitingFood, 1f);
//		img_waiting_bar.color = Color.Lerp (Color.green, Color.red, fTimeWait/customerProperty.fTimeWaitingFood);
		float fWaitingStatus = 1 - fTimeWait / customerProperty.fTimeWaitingFood;
		pnl_time_mask.sizeDelta = new Vector2 (pnl_time_mask.sizeDelta.x , GameFlags.fEmojiGuage * fWaitingStatus);
		if (fWaitingStatus > 0.7f) {
			img_emoji.sprite = sprite_emos [0];
			sprite_customer.sprite = CustomerManager.Instance ().customerSprites [nCustomerProperty * 5 + 1];
		}
		else if (fWaitingStatus > 0.35f) {
			if (nStep == 0) {
				SoundManager.Instance ().PlaySound (3);
				nStep++;
			}
			img_emoji.sprite = sprite_emos [1];
			sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5 + 2];
		} else {
			if (nStep == 1) {
				SoundManager.Instance ().PlaySound (3);
				nStep++;
			}
			img_emoji.sprite = sprite_emos [2];
			sprite_customer.sprite = CustomerManager.Instance().customerSprites[nCustomerProperty * 5 + 3];
		}
	}

	public void GoneCustomer() {
		
	}

	void LoseCustomer() {
//		img_waiting_bar.gameObject.SetActive (false);
//		img_waiting_bar_back.gameObject.SetActive (false);
		pnl_time_mask.sizeDelta = new Vector2(pnl_time_mask.sizeDelta.x, GameFlags.fEmojiGuage);
		img_emoji.sprite = sprite_emos[0];
		canvasTimeBar.SetActive(false);
	}

	void SatisfyCustomer() {
		pnl_time_mask.sizeDelta = new Vector2(pnl_time_mask.sizeDelta.x, GameFlags.fEmojiGuage);
		img_emoji.sprite = sprite_emos[0];
		canvasTimeBar.SetActive(false);
//		img_waiting_bar.gameObject.SetActive (false);
//		img_waiting_bar_back.gameObject.SetActive (false);
		RoundManager.Instance ().UpdateScore (foodScore);
		GenerateCoinObjs ();
	}

	//disapprear customer character in the scene
	public void customerAway() {
		sprite_customer.enabled = false;
		fTimeWait = 0;
		fTimeEat = 0;
		dialogFood.SetActive (false);
		if (customerStatus == CustomerStatus.EatingFood)
			SoundManager.Instance ().PlaySound (2);
		else
			SoundManager.Instance ().PlaySound (4);
		customerStatus = CustomerStatus.Gone;
		orderedFood = "";
		waitingTable.HideEatingdish ();
		//aniChair.SetTrigger ("CloseChair");
		StartCoroutine(CloseChair());
	}

	IEnumerator CloseChair() {
		aniChair.SetTrigger ("CloseChair");
		yield return new WaitForSeconds (1f);
		customerManager.InsertSeat2EmptyList (nCustomerIndex);
	}

	public void GenerateCoinObjs () {
		int nCoin = 1;
		if (foodScore == 7)
			nCoin = 3;
		else if (foodScore == 12)
			nCoin = 6;
		else if (foodScore == 17)
			nCoin = 8;
		//Coin in
		SoundManager.Instance ().PlaySound (0);
		StartCoroutine (GenerateCoins(nCoin));
	}

	IEnumerator GenerateCoins(int nCoin) {
		for (int i = 0; i < nCoin; i++) {
			GameObject coinObj = Instantiate (prefCoinObj, coinObjSpawner.transform.position, coinObjSpawner.transform.rotation) as GameObject;
			yield return new WaitForSeconds (0.1f);
		}

	}

}