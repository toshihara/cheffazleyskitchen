﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodMaterialController : MonoBehaviour {
	public GameObject[] foodMaterials;
	public List<CookInfo> roundCookList = new List<CookInfo>();
	public static FoodMaterialController foodMatController;

	public static FoodMaterialController Instance() {
		if (!foodMatController) {
			foodMatController = FindObjectOfType (typeof (FoodMaterialController)) as FoodMaterialController;
		}
		return foodMatController;
	}

	// Use this for initialization
	void Start () {
		InitFoodMaterials ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitFoodMaterials() {
		for (int i = 0; i < foodMaterials.Length; i++) {
			foodMaterials [i].SetActive (false);
		}
		roundCookList.Clear ();
		List<string> listRoundMenu = RoundManager.Instance ().listRoundMenu;
		for (int k = 0; k <listRoundMenu.Count; k++) {
			string strCookMenu = listRoundMenu [k];
			CookInfo cookInfo = new CookInfo (strCookMenu);
			ActivateFoodMaterials (cookInfo);
			roundCookList.Add (cookInfo);
		}
		CheckDrinkMachinesAvailable ();
	}

	void CheckDrinkMachinesAvailable() {
		int[] coffeeMachines = new int[] {0,0,0};
		int[] coldMachines = new int[] {0,0,0};
		for (int i = 0; i < roundCookList.Count; i++) {
			switch (roundCookList [i].cookName) {
			case "Americano":
				coffeeMachines [0] = 1;
				break;
			case "Caramel Latte":
				coffeeMachines [1] = 1;
				break;
			case "Hot Chocolate":
				coffeeMachines [2] = 1;
				break;
			case "Frozen Grapes Mint Soda":
				coldMachines [0] = 1;
				break;
			case "Soda Bandung":
				coldMachines [1] = 1;
				break;
			case "Sucre Tea":
				coldMachines [2] = 1;
				break;
			}
		}
		RoundManager.Instance ().RecheckDrinkMachineActivation (coffeeMachines, coldMachines);
	}

	void ActivateFoodMaterials(CookInfo cookInfo) {
		for (int i = 0; i < cookInfo.foodMaterialList.Count; i++){
			FoodMaterial foodMat = cookInfo.foodMaterialList [i];
			switch (foodMat) {
			case FoodMaterial.Macaron:
				this.foodMaterials [0].SetActive (true);
				break;
			case FoodMaterial.Churros:
				this.foodMaterials [1].SetActive (true);
				break;
			case FoodMaterial.Cake:
				this.foodMaterials [2].SetActive (true);
				break;
			case FoodMaterial.Spaghetti:
				this.foodMaterials [3].SetActive (true);
				break;
			case FoodMaterial.Rice:
				this.foodMaterials [4].SetActive (true);
				break;
			case FoodMaterial.PizzaCrust:
				this.foodMaterials [5].SetActive (true);
				break;
			case FoodMaterial.Eclairs:
				this.foodMaterials [6].SetActive (true);
				break;
			case FoodMaterial.Chocolate:
				this.foodMaterials [7].SetActive (true);
				break;
			case FoodMaterial.Caramel:
				this.foodMaterials [8].SetActive (true);
				break;
			case FoodMaterial.Tomato:
				this.foodMaterials [9].SetActive (true);
				break;
			case FoodMaterial.Meat:
				this.foodMaterials [10].SetActive (true);
				break;
			case FoodMaterial.Turmeric:
				this.foodMaterials [11].SetActive (true);
				break;

			}
		}
	}
		
}

public class CookInfo {
	public string cookName;
	public List<FoodMaterial> foodMaterialList;
	public UtensilType utensil;
	public CookType cookType;

	public CookInfo(string cookId) {
		foodMaterialList = new List<FoodMaterial> ();
		switch (cookId) {
		case "m1":
			cookName = "Plain Macaron";
			foodMaterialList.Add (FoodMaterial.Macaron);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_1"))
				PlayerPrefs.SetInt ("CookRecord_1", 1);
			break;
		case "m2":
			cookName = "Chocolate Macaron";
			foodMaterialList.Add (FoodMaterial.Macaron);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_2"))
				PlayerPrefs.SetInt ("CookRecord_2", 1);
			break;
		case "m3":
			cookName = "Chocolate Caramel Macaron";
			foodMaterialList.Add (FoodMaterial.Macaron);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			foodMaterialList.Add (FoodMaterial.Caramel);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_3"))
				PlayerPrefs.SetInt ("CookRecord_3", 1);
			break;
		case "m4":
			cookName = "Churros Plain";
			foodMaterialList.Add (FoodMaterial.Churros);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_4"))
				PlayerPrefs.SetInt ("CookRecord_4", 1);
			break;
		case "m5":
			cookName = "Churros Chocolate";
			foodMaterialList.Add (FoodMaterial.Churros);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_5"))
				PlayerPrefs.SetInt ("CookRecord_5", 1);
			break;
		case "m6":
			cookName = "Churros Chocolate Caramel";
			foodMaterialList.Add (FoodMaterial.Churros);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			foodMaterialList.Add (FoodMaterial.Caramel);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_6"))
				PlayerPrefs.SetInt ("CookRecord_6", 1);
			break;
		case "m7":
			cookName = "Plain Cake";
			foodMaterialList.Add (FoodMaterial.Cake);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_7"))
				PlayerPrefs.SetInt ("CookRecord_7", 1);
			break;
		case "m8":
			cookName = "Chocolate Cake";
			foodMaterialList.Add (FoodMaterial.Cake);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_8"))
				PlayerPrefs.SetInt ("CookRecord_8", 1);
			break;
		case "m9":
			cookName = "Chocolate Caramel Cake";
			foodMaterialList.Add (FoodMaterial.Cake);
			foodMaterialList.Add (FoodMaterial.Chocolate);
			foodMaterialList.Add (FoodMaterial.Caramel);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_9"))
				PlayerPrefs.SetInt ("CookRecord_9", 1);
			break;
		case "m10":
			cookName = "Pasta Piyash";
			foodMaterialList.Add (FoodMaterial.Spaghetti);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_10"))
				PlayerPrefs.SetInt ("CookRecord_10", 1);
			break;
		case "m11":
			cookName = "Pasta Payish";
			foodMaterialList.Add (FoodMaterial.Spaghetti);
			foodMaterialList.Add (FoodMaterial.Tomato);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_11"))
				PlayerPrefs.SetInt ("CookRecord_11", 1);
			break;
		case "m12":
			cookName = "Pasta Daging Salai Lemak Cili Api";
			foodMaterialList.Add (FoodMaterial.Spaghetti);
			foodMaterialList.Add (FoodMaterial.Tomato);
			foodMaterialList.Add (FoodMaterial.Turmeric);
			utensil = UtensilType.Stove;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_12"))
				PlayerPrefs.SetInt ("CookRecord_12", 1);
			break;
		case "m13":
			cookName = "French Butter Rice";
			foodMaterialList.Add (FoodMaterial.Rice);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_13"))
				PlayerPrefs.SetInt ("CookRecord_13", 1);
			break;
		case "m14":
			cookName = "French Butter Rice With Rendang & Vegetable Pickles";
			foodMaterialList.Add (FoodMaterial.Rice);
			foodMaterialList.Add (FoodMaterial.Meat);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_14"))
				PlayerPrefs.SetInt ("CookRecord_14", 1);
			break;
		case "m15":
			cookName = "French Butter Rice With Daging Salai Lemak Cili Api";
			foodMaterialList.Add (FoodMaterial.Rice);
			foodMaterialList.Add (FoodMaterial.Meat);
			foodMaterialList.Add (FoodMaterial.Turmeric);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_15"))
				PlayerPrefs.SetInt ("CookRecord_15", 1);
			break;
		case "m16":
			cookName = "Ratatouille Pizza";
			foodMaterialList.Add (FoodMaterial.PizzaCrust);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_16"))
				PlayerPrefs.SetInt ("CookRecord_16", 1);
			break;
		case "m17":
			cookName = "Rendang Pizza";
			foodMaterialList.Add (FoodMaterial.PizzaCrust);
			foodMaterialList.Add (FoodMaterial.Meat);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_17"))
				PlayerPrefs.SetInt ("CookRecord_17", 1);
			break;
		case "m18":
			cookName = "Heavy Duty Meat Pizza";
			foodMaterialList.Add (FoodMaterial.PizzaCrust);
			foodMaterialList.Add (FoodMaterial.Meat);
			foodMaterialList.Add (FoodMaterial.Tomato);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_18"))
				PlayerPrefs.SetInt ("CookRecord_18", 1);
			break;
		case "m19":
			cookName = "Toasted Eclair";
			foodMaterialList.Add (FoodMaterial.Eclairs);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_19"))
				PlayerPrefs.SetInt ("CookRecord_19", 1);
			break;
		case "m20":
			cookName = "Toasted Eclair Pulled Beef Rendang";
			foodMaterialList.Add (FoodMaterial.Eclairs);
			foodMaterialList.Add (FoodMaterial.Meat);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_20"))
				PlayerPrefs.SetInt ("CookRecord_20", 1);
			break;
		case "m21":
			cookName = "Toasted Eclair Daging Salai";
			foodMaterialList.Add (FoodMaterial.Eclairs);
			foodMaterialList.Add (FoodMaterial.Meat);
			foodMaterialList.Add (FoodMaterial.Turmeric);
			utensil = UtensilType.Oven;
			cookType = CookType.Food;
			if (!PlayerPrefs.HasKey ("CookRecord_21"))
				PlayerPrefs.SetInt ("CookRecord_21", 1);
			break;
		case "d1":
			cookName = "Americano";
			utensil = UtensilType.CoffeeMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_22"))
				PlayerPrefs.SetInt ("CookRecord_22", 1);
			break;
		case "d2":
			cookName = "Caramel Latte";
			utensil = UtensilType.CoffeeMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_23"))
				PlayerPrefs.SetInt ("CookRecord_23", 1);
			break;
		case "d3":
			cookName = "Hot Chocolate";
			utensil = UtensilType.CoffeeMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_24"))
				PlayerPrefs.SetInt ("CookRecord_24", 1);
			break;
		case "d4":
			cookName = "Frozen Grapes Mint Soda";
			foodMaterialList.Add (FoodMaterial.Ice);
			utensil = UtensilType.ColdMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_25"))
				PlayerPrefs.SetInt ("CookRecord_25", 1);
			break;
		case "d5":
			cookName = "Soda Bandung";
			foodMaterialList.Add (FoodMaterial.Ice);
			utensil = UtensilType.ColdMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_26"))
				PlayerPrefs.SetInt ("CookRecord_26", 1);
			break;
		case "d6":
			cookName = "Sucre Tea";
			foodMaterialList.Add (FoodMaterial.Ice);
			utensil = UtensilType.ColdMachine;
			cookType = CookType.Drink;
			if (!PlayerPrefs.HasKey ("CookRecord_27"))
				PlayerPrefs.SetInt ("CookRecord_27", 1);
			break;
		}
	}

}
