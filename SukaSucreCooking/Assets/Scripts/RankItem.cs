﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankItem : MonoBehaviour {

	public GameObject badge_0;
	public Text txt_badge_0;
	public GameObject badge_1;
	public Text txt_badge_1;
	public RawImage img_avatar;
	public Image img_badge_back;
	public Image img_item_back;
	public Text txt_userName;
	public Text txt_userScore;
	public Text txt_country;
	public Sprite[] topRankSprites;
	public Sprite[] backGrounds;

	public RawImage img_flag;
	public Text txt_rank;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitRankingItem(int nRank, string userName,  string flagURL, string userAvatarURL, long lScore, bool bMyRank=false, string countryName="Malaysia") {
//		if (nRank < 4) {
//			this.badge_0.SetActive (true);
//			this.badge_1.SetActive (false);
//			this.txt_badge_0.text = "" + nRank;
//			if (nRank == 1)
//				img_badge_back.sprite = this.topRankSprites [0];
//			else if (nRank == 2)
//				img_badge_back.sprite = this.topRankSprites [1];
//			else
//				img_badge_back.sprite = this.topRankSprites [2];
//
//		} else {
//			this.badge_0.SetActive (false);
//			this.badge_1.SetActive (true);
//			this.txt_badge_1.text = "" + nRank;
//		}
		this.txt_rank.text = nRank + "";
		this.txt_userName.text = userName;
		this.txt_userScore.text = string.Format ("{0:#,#}", lScore);
		this.txt_country.text = countryName;
//		if (bMyRank)
//			this.img_item_back.sprite = this.backGrounds[1];
//		else
//			this.img_item_back.sprite = this.backGrounds[0];
		if (userAvatarURL != "")
			DisplayAvatarImage (userAvatarURL);
		if (flagURL != "") {
			DisplayFlagImage (flagURL);
		}
//		else
//			this.img_avatar.texture = RankingPopupHandler.Instance().listSpriteAvatar[(nRank-1) % 10];

	}

	void DisplayAvatarImage(string imgPath) {
		StartCoroutine (LoadAvatarImageFromServer(imgPath));
	}

	IEnumerator LoadAvatarImageFromServer (string urlImage) {
		WWW www = new WWW(urlImage);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			img_avatar.texture = www.texture;
		}
	}

	void DisplayFlagImage(string imgPath) {
		StartCoroutine (LoadFlagImageFromServer(imgPath));
	}

	IEnumerator LoadFlagImageFromServer (string urlImage) {
		WWW www = new WWW(urlImage);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			img_flag.texture = www.texture;
		}
	}
}
