﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ricimi;
using Pathfinding;
public class RoundManager : MonoBehaviour {
	public Image imgTimeBar;
	public GameObject[] starObjs;
	public Text gameCoin;
	public float fTimeCounter;
	public float fInterval;
	public int nMaxCustomerCounter = 0;
	public bool bStartUITimeTracking = false;
	public static RoundManager uc;
	public int nScore;
	public Text txt_time;
//	public Text txt_score;
//	public Image img_progress;

	public GameObject prefPopupLose;
	public GameObject prefPopupWin;
	public GameObject prefPopupPause;
	public GameObject mCanvas;
	public GameObject prefChief;
	public GameObject prefTarget;
	public GameObject SpawnPos;

	public GameObject target;
	public GameObject Chief;

	public GameObject prefRoundInfoPopup;
	public List<string> listRoundMenu = new List<string>(); 

	public GameObject[] ovens;
	public GameObject[] lockedOvens;
	public GameObject[] stoves;
	public GameObject[] lockedStoves;
	public GameObject[] coffeeMachines;
	public GameObject[] lockedCoffeeMachines;
	public GameObject[] coldMachines;
	public GameObject[] lockedColdMachines;
	public GameObject[] warmStations;
	public GameObject[] lockedWarmStations;
	public GameObject lockedIceMachine;
	public WaitingTableController[] waitingTables;
	public GameObject uiTutorial;

	GameObject popupLose;
	GameObject popupWin;
	GameObject popupPause;


	int nRoundTimeLimit;
	int nTargetScore;
	int nBonusScore;
	float fTimeProgressBar = 0;

	// Use this for initialization
	public static RoundManager Instance() {
		if (!uc) {
			uc = FindObjectOfType (typeof(RoundManager)) as RoundManager;
		}
		return uc;
	}

	void Start () {
		//InitTimeTracer ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameFlags.bPauseGame && !GameFlags.bTutorial)
			UpdateTimeTracker ();
	}

	public void  UpdateTimeTracker() {
		if (bStartUITimeTracking) {
			fTimeCounter += Time.deltaTime;
			if (fTimeCounter < this.nRoundTimeLimit * 1f) {
				imgTimeBar.gameObject.transform.localScale = new Vector3 (1f - fTimeCounter / this.nRoundTimeLimit * 1f, 1f, 1f);
				int nMin = (this.nRoundTimeLimit - (int)fTimeCounter) / 60;
				int nSec = (this.nRoundTimeLimit - (int)fTimeCounter) % 60;
				if (nSec < 10)
					this.txt_time.text = "" + nMin + ":0" + nSec;
				else
					this.txt_time.text = "" + nMin + ":" + nSec;
			}
			else if (!GameFlags.bEndRound) {
				this.txt_time.text = "0:00";
				if (!GameFlags.bTimesup) {
					GameFlags.bTimesup = true;
					ShowResult ();
				}
			}
		}
	}

	public void DestroyCoinObject (GameObject coinObj) {
		Destroy (coinObj);
	}

	// when round end we need to initialize ( make empty ) the cooking utensils
	void EmptyUtensils() {
		int nOvens = PlayerPrefs.GetInt ("Oven",2);
		int nStoves = PlayerPrefs.GetInt ("Stove", 2);
		int nCoffeeMachine = PlayerPrefs.GetInt ("CoffeeMachine", 1);
		int nColdMachine = PlayerPrefs.GetInt ("ColdMachine", 1);
		int nWarmStation = PlayerPrefs.GetInt ("WarmStation", 2);

		for (int i = 0; i < 4; i++) {
			if (i < nOvens)
				ovens [i].GetComponent<Furniture> ().EmptyUtensil ();
			
			if (i < nStoves)
				stoves [i].GetComponent<Furniture> ().EmptyUtensil ();
			
			if (i < nWarmStation)
				warmStations [i].GetComponent<WarmerHandler> ().EmptyWarmer ();
		}

		for (int i = 0; i < 3; i++) {
			if (coffeeMachines [i].activeSelf)
				coffeeMachines [i].GetComponent<Furniture> ().EmptyUtensil ();
			
			if (coldMachines[i].activeSelf)
				coldMachines [i].GetComponent<Furniture> ().EmptyUtensil ();
		}

		for (int i = 0; i < waitingTables.Length; i++)
			waitingTables [i].HideEatingdish ();
	}

	//called when the round ends up
	public void ShowResult() {
		GameFlags.bEndRound = true;
		CustomerManager.Instance ().EndGameRound ();
		int nCoin = PlayerPrefs.GetInt ("Coin");
		if (nScore < nTargetScore) {
			popupLose = Instantiate (prefPopupLose) as GameObject;
			popupLose.transform.parent = mCanvas.transform;

			popupLose.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);

			popupLose.transform.localScale = Vector3.one;
			popupLose.GetComponent<PopupLose> ().SetScore (nScore);
			nCoin += 5;
			PlayerPrefs.SetInt ("Coin", nCoin);
			long lCoin = (long)nCoin;
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lCoin);
				ApiHandler.Instance ().SendGameLog (5, 0, 1);
			}
			VoiceManager.Instance ().PlayFailVoice ();

		} else {
			int nStarCount = 1 + (nScore - nTargetScore) / nBonusScore;
			if (nStarCount > 3)
				nStarCount = 3;
			
			for (int i = 0; i < nStarCount; i++)
				starObjs [i].SetActive (true);

			string strKey = "Round_Achieve_1_" + GameFlags.nCurrentRound;
			if (PlayerPrefs.GetInt(strKey) < nStarCount)
				PlayerPrefs.SetInt (strKey, nStarCount);
			nCoin += nScore;// + nStarCount * 50;
			//nCoin += nScore;
			PlayerPrefs.SetInt ("Coin", nCoin);
			long lCoin = (long)nCoin;
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lCoin);
				ApiHandler.Instance ().SendGameLog ((long)(nScore), 1, 0);
			}
			VoiceManager.Instance ().PlayWinVoice ();
			string strRoundDateCount = "Round_Dialy_Play_1_" + GameFlags.nCurrentRound;
			string strRoundRecentDate = "Round_Play_Recent_1_" + GameFlags.nCurrentRound;
			string strDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
			if (!PlayerPrefs.HasKey (strRoundRecentDate)) {
				PlayerPrefs.SetString (strRoundRecentDate, strDate);
			}
			int currentCount = PlayerPrefs.GetInt(strRoundDateCount, 0);
			PlayerPrefs.SetInt (strRoundDateCount, currentCount + 1);

			popupWin = Instantiate (prefPopupWin) as GameObject;
			popupWin.transform.parent = mCanvas.transform;
			popupWin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupWin.transform.localScale = Vector3.one;
			popupWin.GetComponent<PopupWin> ().InitWinPopup (nScore, GameFlags.nCurrentRound, nStarCount);
		}
		EmptyUtensils ();
		//StartCoroutine (ShowRoundResults());
	}

	public void PauseGame() {
		popupPause = Instantiate (prefPopupPause) as GameObject;
		popupPause.transform.parent = mCanvas.transform;
		popupPause.transform.localScale = Vector3.one;
		popupPause.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (21f, 33f);
		popupPause.GetComponent<PopupPause> ().SetGamePaused ();
	}

	/*
	 * Resume game:
	 * close the pause popup window and set the pause flag flase
	*/
	public void ResumeGame() {
		//GameFlags.bPauseGame = true;
		popupPause.GetComponent<PopupPause> ().ResumeGame ();
	}

	public void ResumeNewRound() {
		StartCoroutine (WaitSeconds(1f));
	}

	IEnumerator WaitSeconds(float fSec) {
		yield return new WaitForSeconds (fSec);
		GameFlags.bPauseGame = false;
	}

	/*  Restart Game
	 * Restart the current Round: 
	 * close the current active popup window ( pause, win, lose) and reset the parameters for RoundManager, 
	 * CharacterAniController, CustomerManager, Customer, Food etc ...
	*/
	public void RestartGame() {
		if (popupWin)
			popupWin.GetComponent<PopupWin> ().Close ();
		if (popupLose)
			popupLose.GetComponent<PopupLose> ().Close ();
		if (popupPause)
			popupPause.GetComponent<PopupPause> ().Close ();
		
		target.transform.position = SpawnPos.transform.position;
		InitTimeTracer ();
	}

	/*
	 * PlayNextRound():
	 * Play the next round and shows the round info in popup window before start playing round
	 * */
	public void PlayNextRound() {
		if (popupWin)
			popupWin.GetComponent<PopupWin> ().Close ();
		if (popupLose)
			popupLose.GetComponent<PopupLose> ().Close ();
		if (popupPause)
			popupPause.GetComponent<PopupPause> ().Close ();

		target.transform.position = SpawnPos.transform.position;
		InitTimeTracer ();

		GameFlags.bPauseGame = true;
		GameObject popupRoundInfo = Instantiate (prefRoundInfoPopup) as GameObject;
		popupRoundInfo.SetActive (true);
		popupRoundInfo.transform.localScale = Vector3.zero;
		GameObject m_canvas = GameObject.Find("Canvas");
		popupRoundInfo.transform.SetParent (m_canvas.transform, false);

		var playPopup = popupRoundInfo.GetComponent<PlayRoundPopup> ();
		playPopup.Open ();
		int starsObtained = 0;

		string strKey = "Round_Achieve_1_" + GameFlags.nCurrentRound;
		if (PlayerPrefs.HasKey (strKey))
			starsObtained = PlayerPrefs.GetInt (strKey);
		//PlayerPrefs.SetInt ("CurrentRound", GameFlags.nCurrentRound);
		//InitTimeTracer ();
		playPopup.SetAchievedStars (starsObtained);
		playPopup.InitRound (GameFlags.nCurrentRound, starsObtained);

	}


	public void Go2Main() {
		GameManager.Instance ().LoadLevelScene ();
	}

	void ActivateFurnitures() {
		int nOvens = PlayerPrefs.GetInt ("Oven",2);
		int nStoves = PlayerPrefs.GetInt ("Stove", 2);
		int nCoffeeMachine = PlayerPrefs.GetInt ("CoffeeMachine", 1);
		int nColdMachine = PlayerPrefs.GetInt ("ColdMachine", 1);
		int nWarmStation = PlayerPrefs.GetInt ("WarmStation", 2);
		if (nWarmStation < 2) {
			nWarmStation = 2;
			PlayerPrefs.SetInt ("WarmStation", 2);
		}

		Debug.Log ("current round:" + PlayerPrefs.GetInt ("CurrentRound"));

		for (int i = 0; i < 4; i++) {
			if (i < nOvens) {
				ovens [i].SetActive (true);
				lockedOvens [i].SetActive (false);
			} else {
				ovens [i].SetActive (false);
				lockedOvens [i].SetActive (true);
			}

			if (i < nStoves) {
				stoves [i].SetActive (true);
				lockedStoves [i].SetActive (false);
			} else { 
				stoves [i].SetActive (false);
				lockedStoves [i].SetActive (true);
			}

			if (i < nWarmStation) {
				warmStations [i].SetActive (true);
				lockedWarmStations [i].SetActive (false);
			} else { 
				warmStations [i].SetActive (false);
				lockedWarmStations [i].SetActive (true);
			}
				
		}
		GameFlags.nCurrentRound = PlayerPrefs.GetInt ("CurrentRound");
		if (GameFlags.nCurrentRound >= Config.Oven1stFreeStage && GameFlags.nCurrentRound < Config.Oven2ndFreeStage) {
			ovens [1].SetActive (false);
			lockedOvens [1].SetActive (true);
		} else if (GameFlags.nCurrentRound < Config.Oven1stFreeStage) {
			for (int i = 0; i < 2; i++) {
				ovens [i].SetActive (false);
				lockedOvens [i].SetActive (true);
			}
		}

		if (GameFlags.nCurrentRound >= Config.Stove1stFreeStage && GameFlags.nCurrentRound < Config.Stove2ndFreeStage) {
			stoves [1].SetActive (false);
			lockedStoves [1].SetActive (true);
		} else if (GameFlags.nCurrentRound < Config.Stove1stFreeStage) {
			for (int i = 0; i < 2; i++) {
				stoves [i].SetActive (false);
				lockedStoves [i].SetActive (true);
			}
		}

		if (GameFlags.nCurrentRound >= Config.Warming1stFreeStage && GameFlags.nCurrentRound < Config.Warming2ndFreeStage) {
			warmStations [1].SetActive (false);
			lockedWarmStations [1].SetActive (true);
		} else if (GameFlags.nCurrentRound < Config.Warming1stFreeStage) {
			for (int i = 0; i < 2; i++) {
				warmStations [i].SetActive (false);
				lockedWarmStations [i].SetActive (true);
			}
		}


		for (int i = 0; i < 3; i++) {
//			if (i < nCoffeeMachine)
//				coffeeMachines [i].SetActive (true);
//			else 
//				coffeeMachines [i].SetActive (false);
//			if (i < nColdMachine)
//				coldMachines [i].SetActive (true);
//			else
//				coldMachines [i].SetActive (false);
			coffeeMachines [i].SetActive (true);
			coldMachines [i].SetActive (true);
		}

	}

	public void RecheckDrinkMachineActivation(int[] coffeeChecks, int[] coldChecks) {
		for (int i = 0; i < 3; i++) {
			if (coffeeChecks [i] == 0) {
				coffeeMachines [i].SetActive (false);
				lockedCoffeeMachines [i].SetActive (true);
			} else {
				coffeeMachines [i].SetActive (true);
				lockedCoffeeMachines [i].SetActive (false);
			}

			if (coldChecks [i] == 0) {
				coldMachines [i].SetActive (false);
				lockedColdMachines [i].SetActive (true);
			}
			else {
				coldMachines [i].SetActive (true);
				lockedColdMachines [i].SetActive (false);
				this.lockedIceMachine.SetActive (false);
			}
		}
	}

	public void InitTimeTracer () {
		Debug.Log ("------> InitTimeTracer: ");
		ActivateFurnitures ();
		EmptyUtensils ();
		for (int i = 0; i < 3; i++) {
			starObjs [i].SetActive (false);
		}
		fTimeCounter = 0;
		nScore = 0;
		//txt_score.text = "0";
		gameCoin.text = "0";

		bStartUITimeTracking = true;
//		this.img_progress.fillAmount = 0;

		GameFlags.bEndRound = false;
		GameFlags.bPauseGame = false;
		GameFlags.bTimesup = false;
		GameFlags.nCurrentRound = PlayerPrefs.GetInt ("CurrentRound");
		if (GameFlags.nCurrentRound < 1)
			GameFlags.nCurrentRound = 1;
		this.listRoundMenu.Clear ();
		for (int i = 0; i < GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].menuList.Count; i++) {
			this.listRoundMenu.Add (GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].menuList[i]);
		}
		this.nTargetScore = GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].nTargetScore;
		this.nRoundTimeLimit = (int)GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].fTimeDuration;
		this.nBonusScore = GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].nBonusPoint;
		this.fInterval = GameFlags.roundInfoList [GameFlags.nCurrentRound - 1].fInterval;
		this.nMaxCustomerCounter = (int)(nRoundTimeLimit/fInterval);
		int nSec = nRoundTimeLimit % 60;
		int nMin = nRoundTimeLimit / 60;
		txt_time.text = "" + nMin + ":" + nSec;

		target.transform.position = SpawnPos.transform.position;
		Chief.transform.position = target.transform.position;
		Chief.GetComponent<AILerp> ().TeleportPosition (target.transform.position);
		Chief.GetComponent<CharacterAniController> ().InitChief ();
		FoodMaterialController.Instance ().InitFoodMaterials ();
		CustomerManager.Instance ().RestartGame ();
	}
		
	public void UpdateScore(int nAddScore) {
		nScore += nAddScore;
		gameCoin.text = "" + nScore;
		//txt_score.text = "" + nScore;
//		if ((nScore - nAddScore) < nTargetScore && nScore >= nTargetScore) {
//			starObjs [0].SetActive (true);
//		} else if ((nScore - nAddScore) < (nTargetScore + nBonusScore) && nScore > (nTargetScore + nBonusScore)) {
//			starObjs [0].SetActive (true);
//			starObjs [1].SetActive (true);
//		} else if ((nScore - nAddScore) < (nTargetScore + 2 * nBonusScore) && nScore > (nTargetScore + 2 * nBonusScore)) {
//			starObjs [0].SetActive (true);
//			starObjs [1].SetActive (true);
//			starObjs [2].SetActive (true);
//		}

		int nStarCount = 0;
		if (nScore >= nTargetScore) {
			nStarCount = 1 + (nScore - nTargetScore) / nBonusScore;
		}
		if (nStarCount > 3)
			nStarCount = 3;
		for (int i = 0; i < 3; i++) {
			if (i < nStarCount) {
				starObjs [i].SetActive (true);
			}
			else
				starObjs [i].SetActive (false);
		}
//		float fProgress = 0;
//		if (nScore < this.nTargetScore) {
//			fProgress = nScore * GameFlags.fProgressTarget / this.nTargetScore ;
//			//StartCoroutine (UpdateProgressBar(fProgress));
//		} else {
//			fProgress = GameFlags.fProgressTarget + GameFlags.fProgressBonus * (nScore - this.nTargetScore) / this.nBonusScore;
//		}
//		fTimeProgressBar = 0;
//		//txt_score.text = "15";
//
//		//this.img_progress.fillAmount = fProgress;
//		StartCoroutine (UpdateProgressBar(fProgress));
	}

	IEnumerator UpdateProgressBar(float target) {
		while (fTimeProgressBar < 1f) {
			fTimeProgressBar += Time.deltaTime;
//			float fCurrent = Mathf.Lerp (this.img_progress.fillAmount, target, fTimeProgressBar);
//			this.img_progress.fillAmount = fCurrent;
			if (fTimeProgressBar > 1f)
				break;
			yield return null;
		}
	}
}
