﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookMenuHandler : MonoBehaviour {

	public GameObject[] cook_items;
	public Text txt_count;
	int nCounter = 0;
	// Use this for initialization
	void Start () {
		ActivateCookMenuItems ();
	}

	void ActivateCookMenuItems() {
		for (int i = 0; i < cook_items.Length; i++) {
			string prefKey = "CookRecord_" + (i + 1);
			if (PlayerPrefs.HasKey (prefKey)) {
				cook_items [i].SetActive (true);
				nCounter++;
			}
			else
				cook_items [i].SetActive (false);
		}
		txt_count.text = "" + nCounter + " / 27";
	}

	public void OnClose() {
		GameFlags.bPauseGame = false;
		if (GameFlags.bTutorial && TutorialManager.Instance ().nTutorialIndex == 6) {
			TutorialManager.Instance ().ShowTutorial (6);
		}
	}
}
