﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ricimi;
public class LeaderBoardOpener : MonoBehaviour {
	public GameObject prefRankPopup;
	public Transform mCanvas;
	GameObject rankPopup;
	// Use this for initialization
	void Start () {
		mCanvas = GameObject.Find ("Canvas").transform;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitRankPopup() {
		mCanvas = GameObject.Find ("Canvas").transform;
		rankPopup = Instantiate (prefRankPopup) as GameObject;
		rankPopup.transform.parent = mCanvas;
		rankPopup.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (50f, -224f);
		//rankPopup.GetComponent<RankingPopupHandler> ().DisplayRankLists ();
		//ApiHandler.Instance().GetRankingData("three_month");
		rankPopup.GetComponent<Popup> ().Open();
	}
}
