﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPanelHandler : MonoBehaviour {

	public void OnClickCoinButton(int nButtonId) {
		switch (nButtonId) {
		case 0:
			CoinPurchaseScript.Instance ().Buy4kCoin ();
			break;
		case 1:
			CoinPurchaseScript.Instance ().Buy12kCoin ();
			break;
		case 2:
			CoinPurchaseScript.Instance ().Buy20kCoin ();
			break;
		}
	}
}
