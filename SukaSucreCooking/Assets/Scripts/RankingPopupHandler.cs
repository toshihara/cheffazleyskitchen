﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using Ricimi;

public class RankingPopupHandler : MonoBehaviour {

	public GameObject prefRankItem;
	public GameObject prefPopupRules;
	public GameObject pnl_my_ranking;
	public Text myRank;
	public RawImage myAvatar;
	public Transform mContent;
	public List<GameObject> listRankItems = new List<GameObject>();
	public static RankingPopupHandler handler;
	public Texture2D[] listSpriteAvatar;

	public static RankingPopupHandler Instance() {
		if (!handler) {
			handler = FindObjectOfType (typeof(RankingPopupHandler)) as RankingPopupHandler;
		}
		return handler;
	} 
	// Use this for initialization
	void Start () {
		//mContent.gameObject.SetActive (false);
		ApiHandler.Instance().GetRankingData("three_month");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Go2RulePage() {
		//Application.OpenURL (Config.URL_RULE);
		GameObject rulesPopup = Instantiate(prefPopupRules) as GameObject;
		rulesPopup.transform.parent = GameObject.Find ("Canvas").transform;
		rulesPopup.transform.localScale = 0.8f * Vector3.one;
		rulesPopup.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -100f);
		rulesPopup.GetComponent<Popup> ().Open ();
	}

	public void Go2WinnerPage() {
		Application.OpenURL (Config.URL_WINNER);
	}

	public void DisplayRankingLists(RankItemInfo[] rankItems) {
		mContent.gameObject.SetActive (true);
		FreshRankLists ();
		int nAdjustIndex = 0;
		int nLength = rankItems.Length;
		//Debug.Log ("ranking length:" + nLength);
		for (int i = 0; i < nLength; i++) {

			string userName = rankItems[i].userName;
			//userName = userName.Substring (1, userName.Length - 2);
			string subname = "";
			if (userName.Length > 3)
				subname = userName.Substring (1, userName.Length - 2) + "";

			if (subname != "deleted") {
				GameObject rankItem = Instantiate (prefRankItem) as GameObject;
				string str_item_name = "rank_item_" + i;
				rankItem.name = str_item_name;
				rankItem.transform.parent = mContent;
				rankItem.transform.localScale = Vector3.one;
				int nRank = rankItems[i].rankNum - nAdjustIndex;
				long lScore = rankItems[i].chips;
				long nCountry = rankItems[i].country;
				string strFlagURL = Config.FLAG_URL + nCountry + ".png";
				//Debug.Log ("flagURL:!!! " + strFlagURL);

				string strAvatarName = rankItems[i].avatarName;
				//Debug.Log ("avatar: " + strAvatarName);
				string strAvatarPath = "";
				if (strAvatarName != "")
					strAvatarPath = Config.IMG_URL + strAvatarName;

				rankItem.GetComponent<RankItem>().InitRankingItem(nRank, userName, strFlagURL, strAvatarPath, lScore);
				listRankItems.Add (rankItem);
				if (listRankItems.Count == 30)
					break;
			}
		}
	}

	public void DisplayRankLists(JSONNode dict) {
		mContent.gameObject.SetActive (true);
		FreshRankLists ();
		int nAdjustIndex = 0;
		int nLength = dict ["data"] ["ranking"].Count;
		Debug.Log ("ranking length:" + nLength);
		for (int i = 0; i < nLength; i++) {
			//Debug.Log (dict["data"]["ranking"][i]["username"].ToString());
			string userName = dict["data"]["ranking"][i]["username"].ToString();
			userName = userName.Substring (1, userName.Length - 2);
			string subname = "";
			if (userName.Length > 3)
				subname = userName.Substring (1, userName.Length - 2) + "";

			if (subname != "deleted") {
				GameObject rankItem = Instantiate (prefRankItem) as GameObject;
				string str_item_name = "rank_item_" + i;
				rankItem.name = str_item_name;
				rankItem.transform.parent = mContent;
				rankItem.transform.localScale = Vector3.one;
				int nRank = int.Parse (dict["data"]["ranking"][i]["display_rank"]) - nAdjustIndex;
				long lScore = long.Parse (dict["data"]["ranking"][i]["chips"]);
				long nCountry = long.Parse (dict["data"]["ranking"][i]["country"]);
				string strFlagURL = Config.FLAG_URL + nCountry + ".png";
				//Debug.Log ("flagURL:!!! " + strFlagURL);

				string strAvatarName = dict["data"]["rankig"][i]["image"].ToString();
				//string strAvatar = dict["data"]["ranking"]
				if (strAvatarName.Length > 2)
					strAvatarName = strAvatarName.Substring (1, strAvatarName.Length - 2);
				else
					strAvatarName = "";

				string strAvatarPath = "";
				if (strAvatarName != "")
					strAvatarPath = Config.IMG_URL + strAvatarName;
				//Debug.Log (dict["data"]["rankig"][i]["image"].ToString());

				rankItem.GetComponent<RankItem>().InitRankingItem(nRank, userName, strFlagURL, strAvatarPath, lScore);
				listRankItems.Add (rankItem);
				if (listRankItems.Count == 30)
					break;
			}
		}
	}

	public void FreshRankLists() {
		for(int i = 0; i < listRankItems.Count; i++) {
			Destroy (listRankItems[i]);
		}
		listRankItems.Clear ();
	}
}


public struct RankItemInfo {
	public int rankNum;
	public string userName;
	public string avatarName;
	public int country;
	public long chips;
	public RankItemInfo(string sRankNum, string sUserName, string sAvatarName, string sCountry, string sChips) {
		rankNum = int.Parse (sRankNum);
		userName = sUserName;
		avatarName = sAvatarName;
		country = int.Parse (sCountry);
		chips = long.Parse (sChips);
	}

}