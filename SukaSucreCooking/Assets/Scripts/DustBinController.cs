﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class DustBinController : MonoBehaviour {
	TargetMover targetMover;
	public Transform target_pos;
	public static DustBinController dustBinController;
	public Animator aniDust;

	public static DustBinController Instance() {
		if (!dustBinController) {
			dustBinController = FindObjectOfType (typeof(DustBinController)) as DustBinController;
		}
		return dustBinController;
	}

	void Start () {
		targetMover = GameObject.FindWithTag ("target").GetComponent<TargetMover>();
	}

	public void OnClickDustBin() {
//		targetMover.SetTargetPosition (target_pos.position);
		targetMover.PushTargetPosition2List(target_pos.position);
		CharacterAniController.Instance ().destinationFurniture = null;
		CharacterAniController.Instance ().destinationFood = null;
		CharacterAniController.Instance ().destinationTable = null;
		CharacterAniController.Instance ().bClickDustBin = true;
		//CharacterAniController.Instance ().nWarmerIndex = -1;
		CharacterAniController.Instance ().bReachDestination = false;

		CharacterAniController.Instance ().targetCategories.Add ("Dustbin");
	}

	public void OpenDustBin() {
		aniDust.SetTrigger ("Open");
	}

}
