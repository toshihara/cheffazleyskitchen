﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomProduct : MonoBehaviour {
	public Animator aniZoomProduct;
	// Use this for initialization
	void Start () {
		
	}
	
	public void ZoomOut() {
		aniZoomProduct.SetTrigger ("zoomout");
	}

	public void ZoomIn() {
		aniZoomProduct.SetTrigger ("zoomin");
	}

	public void Go2Linker() {
		Application.OpenURL (Config.URL_RULE);
	}
}
