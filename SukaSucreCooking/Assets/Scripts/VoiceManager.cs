﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceManager : MonoBehaviour {
	public AudioSource[] levelSelectionVoices;
	public AudioSource[] loadingVoices;
	public AudioSource[] winVoices;
	public AudioSource[] loseVoices;
	public AudioSource buttonSound;
	public static VoiceManager vManager;
	bool bPlayLevelSelection = true;
	public static VoiceManager Instance() {
		if (!vManager) {
			vManager = FindObjectOfType (typeof(VoiceManager)) as VoiceManager;
		}
		return vManager;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayLevelSelectionVoice() {
		//if (bPlayLevelSelection) {
		int nRand = Random.Range (0, levelSelectionVoices.Length);
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			levelSelectionVoices [nRand].Play ();
		//	bPlayLevelSelection = false;
		//}
	}

	public void PlayLoadingVoice() {
		int nRand = Random.Range (0, loadingVoices.Length);
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			loadingVoices [nRand].Play ();
	}

	public void PlayWinVoice() {
		int nRand = Random.Range (0, winVoices.Length);
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			winVoices [nRand].Play ();
	}

	public void PlayFailVoice() {
		int nRand = Random.Range (0, loseVoices.Length);
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			loseVoices [nRand].Play ();
	}

	public void PlayButtonSound() {
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
			buttonSound.Play ();
	}
}
