﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ImageAndVideoPicker;
using UnityEngine.Networking;

public class IVPickerExample : MonoBehaviour {

	string log = "";
	Texture2D texture;
	public RawImage avatarImg;
	//public Text txt_name;
	public InputField inpt_name;
	public InputField inpt_email;
	public static IVPickerExample ivPicker;
	public GameObject prefSignError;
	GameObject popupSignError;
	public Text txt_coin;
	Transform mCanvas;
	public GameObject saveEmailButton;
	public RawImage img_flag;
	public Text textDebug;
	public RawImage img_avatar;
	public Dropdown dropdown;
	public Text txt_result;
	public GameObject pnl_email;
	public Texture2D test_avatar;

	public static IVPickerExample Instance() {
		if (!ivPicker) {
			ivPicker = FindObjectOfType (typeof (IVPickerExample)) as IVPickerExample;
		}
		return ivPicker;
	}

	void Start() {
		mCanvas = GameObject.Find ("Canvas").transform;
		//txt_name.text = PlayerPrefs.GetString ("userName");
		inpt_name.text = PlayerPrefs.GetString ("userName");
		inpt_email.text = PlayerPrefs.GetString ("Email");
		if (inpt_email.text.Contains ("@fbId.com")) {
			inpt_email.text = "";
			pnl_email.SetActive (false);
		}
			
		txt_coin.text = PlayerPrefs.GetInt ("Coin") + "";
//		if (PlayerPrefs.HasKey("AvatarPath"))
//			avatarImg.texture = LevelSceneManager.Instance ().LoadTexture (PlayerPrefs.GetString("AvatarPath"));
		string flagURL = "";
		if (PlayerPrefs.HasKey ("country")) {
			flagURL = Config.FLAG_URL + PlayerPrefs.GetInt ("country") + ".png";
			dropdown.value = PlayerPrefs.GetInt ("country");
		} else {
			flagURL = Config.FLAG_URL + 111 + ".png";
		}
	
		txt_result.gameObject.SetActive (false);
		DisplayFlagImage (flagURL);
//		if (PlayerPrefs.GetString ("AvatarURL", "") != "") {
//			DisplayAvatarImage (PlayerPrefs.GetString ("AvatarURL"));
//		}
	}

	void DisplayFlagImage(string imgPath) {
		StartCoroutine (LoadFlagImageFromServer(imgPath));
	}

	IEnumerator LoadFlagImageFromServer (string urlImage) {
		WWW www = new WWW(urlImage);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			img_flag.texture = www.texture;
		}
	}

	void DisplayAvatarImage(string imgPath) {
		StartCoroutine (LoadAvatarImageFromServer(imgPath));
	}

	IEnumerator LoadAvatarImageFromServer (string urlImage) {
		WWW www = new WWW(urlImage);

		// Wait for download to complete
		yield return www;

		// assign texture
		if (string.IsNullOrEmpty(www.error)) {
			img_avatar.texture = www.texture;
		}
	}

	void OnEnable()
	{
		PickerEventListener.onImageSelect += OnImageSelect;
		PickerEventListener.onImageLoad += OnImageLoad;
		PickerEventListener.onVideoSelect += OnVideoSelect;
		PickerEventListener.onError += OnError;
		PickerEventListener.onCancel += OnCancel;
	}
	
	void OnDisable()
	{
		PickerEventListener.onImageSelect -= OnImageSelect;
		PickerEventListener.onImageLoad -= OnImageLoad;
		PickerEventListener.onVideoSelect -= OnVideoSelect;
		PickerEventListener.onError -= OnError;
		PickerEventListener.onCancel -= OnCancel;
	}

	
	void OnImageSelect(string imgPath, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
		Debug.Log ("Image Location : "+imgPath);
		log += "\nImage Path : " + imgPath;
		log += "\nImage Orientation : " + imgOrientation;
	}


	void OnImageLoad(string imgPath, Texture2D tex, ImageAndVideoPicker.ImageOrientation imgOrientation)
	{
		Debug.Log ("Image Location : "+imgPath);

		avatarImg.texture = tex;
		PlayerPrefs.SetString ("AvatarPath", imgPath);
		//ApiHandler.Instance ().UploadImage (imgPath);
		ApiHandler.Instance ().UploadImage (tex);
		LevelSceneManager.Instance ().UpdateProfileThumb (tex);
		img_avatar.texture = tex;
		LevelSceneManager.Instance ().ActivateVisualEffects ();
	}

	void OnVideoSelect(string vidPath)
	{
		Debug.Log ("Video Location : "+vidPath);
		log += "\nVideo Path : " + vidPath;
		Handheld.PlayFullScreenMovie ("file://" + vidPath, Color.blue, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
	}
	
	void OnError(string errorMsg)
	{
		Debug.Log ("Error : "+errorMsg);
		log += "\nError :" +errorMsg;
	}

	void OnCancel()
	{
		Debug.Log ("Cancel by user");
		log += "\nCancel by user";
	}

	public void SwitchNewAvatar() {
		LevelSceneManager.Instance ().DeactivateVisualEffects ();
		#if UNITY_ANDROID
		AndroidPicker.BrowseImage(true);
		#elif UNITY_IPHONE
		IOSPicker.BrowseImage(true); // true for pick and crop
		#endif
	}

	public void UploadNewAvatar() {
//		var tex = new Texture2D (200, 200, TextureFormat.RGB24, false);
//		tex.ReadPixels (new Rect(0,0,200, 200), 0, 0);
//		tex.Apply ();
//		ApiHandler.Instance ().UploadImage (tex);
		StartCoroutine(ReadScreen());
	}

	IEnumerator ReadScreen() {
		yield return new WaitForEndOfFrame ();
		var tex = new Texture2D (200, 200, TextureFormat.RGB24, false);
		tex.ReadPixels (new Rect(0,0,200, 200), 0, 0);
		tex.Apply ();
		//StartCoroutine (UploadImage(tex));
		ApiHandler.Instance ().UploadImage (tex);
	}

	public void ChangeEmail() {
		string newEmail = inpt_email.text;
		GameFlags.email = newEmail;
		ApiHandler.Instance ().UpdateEmail (newEmail);
		saveEmailButton.SetActive (false);
	}

	public void HandleUpdateEmail(int nResult) {
		switch (nResult) {
		case 0:
			PlayerPrefs.SetString ("Email", GameFlags.email);
			txt_result.gameObject.SetActive (true);
			break;
		case 1:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (6);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			//popupSignError.SetActive (true);
			break;
		case 2:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (7);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			break;
		case 3:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (3);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			break;
		}
	}

	public void ChangeFlag() {
		int nCountryId = dropdown.value;
		PlayerPrefs.SetInt ("country", nCountryId);
		string flagURL = Config.FLAG_URL + nCountryId + ".png";
		DisplayFlagImage (flagURL);
	}


	public void ShowEmailSaveButton() {
		//saveEmailButton.SetActive (true);
	}

	public void UpdateProfile() {
		txt_result.gameObject.SetActive (false);
		string email = inpt_email.text;
		string username = inpt_name.text;
		int countryId = dropdown.value;
		if (email == "")
			email = PlayerPrefs.GetString ("Email");
		if (username == "")
			username = PlayerPrefs.GetString ("userName");
		
		ApiHandler.Instance ().PostUpdateProfileData (username, email, countryId);
	}

	public void RestorePurchaseItems() {
		CoinPurchaseScript.Instance ().RestorePurchases ();
	}


	public void HandleUpdateProfile(int nResult) {
		switch (nResult) {
		case 0:
			txt_result.gameObject.SetActive (true);
			break;
		case 1:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (6);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			//popupSignError.SetActive (true);
			break;
		case 2:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (7);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			break;
		case 3:
			popupSignError = Instantiate (prefSignError) as GameObject;
			popupSignError.transform.parent = mCanvas;
			popupSignError.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (28f, -206f);
			popupSignError.GetComponent<PopupSignError> ().ShowPopUp (3);
			inpt_email.text = PlayerPrefs.GetString ("Email");
			if (inpt_email.text.Contains ("@fbId.com"))
				inpt_email.text = "";
			break;
		}
	}

	IEnumerator UploadImage(Texture2D tex) {
		string formInput = "op=UpdateImage&user_id=" + PlayerPrefs.GetInt ("user_id");
		Debug.Log (formInput);
		byte[] bytes = tex.EncodeToPNG ();
		List<IMultipartFormSection> formData = new List<IMultipartFormSection> ();
		formData.Add (new MultipartFormDataSection(formInput));
		formData.Add (new MultipartFormFileSection("file", bytes, "avatar.png", "image/png"));

		UnityWebRequest www = UnityWebRequest.Post ("http://kometdigital.com/games/chef/backend/user/index.php", formData);
		yield return www.Send ();
		if (www.isError) {
			Debug.Log (www.error);
		} else {
			Debug.Log (www.downloadHandler.text);
		}
	}


//	void OnGUI()
//	{
//		GUILayout.Label (log);
//
//		if(GUI.Button(new Rect(10,10,120,35),"Browse Image"))
//		 {
//			#if UNITY_ANDROID
//			AndroidPicker.BrowseImage(false);
//			#elif UNITY_IPHONE
//			IOSPicker.BrowseImage(false); // true for pick and crop
//			#endif
//		}
//
//		if(GUI.Button(new Rect(140,10,150,35),"Browse & Crop Image"))
//		{
//			#if UNITY_ANDROID
//			AndroidPicker.BrowseImage(true);
//			#elif UNITY_IPHONE
//			IOSPicker.BrowseImage(true); // true for pick and crop
//			#endif
//		}
//
//		if(GUI.Button(new Rect(300,10,120,35),"Browse Video"))
//		{
//			#if UNITY_ANDROID
//			AndroidPicker.BrowseVideo();
//			#elif UNITY_IPHONE
//			IOSPicker.BrowseVideo();
//			#endif
//		}
//
//		if (texture != null){
//			
//			GUI.DrawTexture(new Rect(20,50,Screen.width - 40,Screen.height - 60), texture, ScaleMode.ScaleToFit, true);
//		}
//	}

}
