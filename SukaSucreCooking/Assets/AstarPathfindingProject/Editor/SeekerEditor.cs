using UnityEngine;
using UnityEditor;

namespace Pathfinding {
	[CustomEditor(typeof(Seeker))]
	[RequireComponent(typeof(CharacterController))]
	[RequireComponent(typeof(Seeker))]
	public class SeekerEditor : EditorBase {
		static bool tagPenaltiesOpen;

		protected override void Inspector () {
			base.Inspector();

			var script = target as Seeker;
			Undo.RecordObject(script, "Modify settings on Seeker");

			tagPenaltiesOpen = EditorGUILayout.Foldout(tagPenaltiesOpen, new GUIContent("Tags", "Settings for each tag"));
			if (tagPenaltiesOpen) {
				EditorGUI.indentLevel++;
				string[] tagNames = AstarPath.FindTagNames();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.BeginVertical();
				EditorGUILayout.LabelField("Tag", EditorStyles.boldLabel, GUILayout.MaxWidth(120));
				for (int i = 0; i < script.tagPenalties.Length; i++) {
					EditorGUILayout.LabelField(i < tagNames.Length ? tagNames[i] : "Tag "+i, GUILayout.MaxWidth(120));
				}
				if (GUILayout.Button("Edit names", EditorStyles.miniButton)) {
					AstarPathEditor.EditTags();
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical();
				EditorGUILayout.LabelField("Penalty", EditorStyles.boldLabel, GUILayout.MaxWidth(100));
				for (int i = 0; i < script.tagPenalties.Length; i++) {
					var newPenalty = EditorGUILayout.IntField(script.tagPenalties[i], GUILayout.MinWidth(100));
					if (newPenalty < 0) newPenalty = 0;

					// If the new value is different than the old one
					// Update the value and mark the script as dirty
					if (script.tagPenalties[i] != newPenalty) {
						script.tagPenalties[i] = newPenalty;
					}
				}
				if (GUILayout.Button("Reset all", EditorStyles.miniButton)) {
					for (int i = 0; i < script.tagPenalties.Length; i++) script.tagPenalties[i] = 0;
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical();
				EditorGUILayout.LabelField("Traversable", EditorStyles.boldLabel, GUILayout.MaxWidth(100));
				for (int i = 0; i < script.tagPenalties.Length; i++) {
					var prevTraversable = ((script.traversableTags >> i) & 0x1) != 0;
					var newTraversable = EditorGUILayout.Toggle(prevTraversable);
					if (prevTraversable != newTraversable) {
						script.traversableTags = (script.traversableTags & ~(1 << i)) | ((newTraversable ? 1 : 0) << i);
					}
				}
				if (GUILayout.Button("Set all/none", EditorStyles.miniButton)) {
					script.traversableTags = (script.traversableTags & 0x1) == 0 ? -1 : 0;
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.EndHorizontal();
			}
		}
	}
}
