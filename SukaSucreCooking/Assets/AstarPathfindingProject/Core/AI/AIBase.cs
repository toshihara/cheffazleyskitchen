using UnityEngine;
using System.Collections;

namespace Pathfinding {
	using Pathfinding.RVO;
	using Pathfinding.Util;

	/** Base class for AIPath and RichAI.
	 * This class holds various methods and fields that are common to both AIPath and RichAI.
	 *
	 * \see #Pathfinding.AIPath
	 * \see #Pathfinding.RichAI
	 * \see #Pathfinding.IAstarAI (all movement scripts implement this interface)
	 */
	[RequireComponent(typeof(Seeker))]
	public abstract class AIBase : VersionedMonoBehaviour {
		/** Determines how often the agent will search for new paths (in seconds).
		 * The agent will plan a new path to the target every N seconds.
		 *
		 * If you have fast moving targets or AIs, you might want to set it to a lower value.
		 *
		 * \see #RepeatTrySearchPath
		 */
		public float repathRate = 0.5f;

		/** \copydoc Pathfinding::IAstarAI::canSearch */
		[UnityEngine.Serialization.FormerlySerializedAs("repeatedlySearchPaths")]
		public bool canSearch = true;

		/** \copydoc Pathfinding::IAstarAI::canMove */
		public bool canMove = true;

		/** Max speed in world units per second */
		[UnityEngine.Serialization.FormerlySerializedAs("speed")]
		public float maxSpeed = 1;

		/** Gravity to use.
		 * If set to (NaN,NaN,NaN) then Physics.Gravity (configured in the Unity project settings) will be used.
		 * If set to (0,0,0) then no gravity will be used and no raycast to check for ground penetration will be performed.
		 */
		public Vector3 gravity = new Vector3(float.NaN, float.NaN, float.NaN);

		/** Layer mask to use for ground placement.
		 * Make sure this does not include the layer of any colliders attached to this gameobject.
		 *
		 * \see #gravity
		 * \see https://docs.unity3d.com/Manual/Layers.html
		 */
		public LayerMask groundMask = -1;

		/** Offset along the Y coordinate for the ground raycast start position.
		 * Normally the pivot of the character is at the character's feet, but you usually want to fire the raycast
		 * from the character's center, so this value should be half of the character's height.
		 *
		 * A green gizmo line will be drawn upwards from the pivot point of the character to indicate where the raycast will start.
		 *
		 * \see #gravity
		 */
		public float centerOffset = 1;

		/** If true, the forward axis of the character will be along the Y axis instead of the Z axis.
		 *
		 * For 3D games you most likely want to leave this the default value which is false.
		 * For 2D games you most likely want to change this to true as in 2D games you usually
		 * want the Y axis to be the forwards direction of the character.
		 *
		 * \shadowimage{aibase_forward_axis.png}
		 */
		public bool rotationIn2D = false;

		/** Current desired velocity of the agent (does not include local avoidance and physics).
		 * Lies in the movement plane.
		 */
		protected Vector2 velocity2D;

		/** Velocity due to gravity.
		 * Perpendicular to the movement plane.
		 *
		 * When the agent is grounded this may not accurately reflect the velocity of the agent.
		 * It may be non-zero even though the agent is not moving.
		 */
		protected float verticalVelocity;

		/** Cached Seeker component */
		protected Seeker seeker;

		/** Cached Transform component */
		protected Transform tr;

		/** Cached Rigidbody component */
		protected Rigidbody rigid;

		/** Cached Rigidbody component */
		protected Rigidbody2D rigid2D;

		/** Cached CharacterController component */
		protected CharacterController controller;


		/** Plane which this agent is moving in.
		 * This is used to convert between world space and a movement plane to make it possible to use this script in
		 * both 2D games and 3D games.
		 */
		public IMovementPlane movementPlane = GraphTransform.identityTransform;

		/** Indicates if gravity is used during this frame */
		protected bool usingGravity { get; private set; }

		/** Delta time used for movement during the last frame */
		protected float lastDeltaTime;

		/** Position of the character at the end of the last frame */
		protected Vector3 prevPosition1;

		/** Position of the character at the end of the frame before the last frame */
		protected Vector3 prevPosition2;

		/** Amount which the character wants or tried to move with during the last frame */
		protected Vector3 lastDeltaPosition;

		/** Only when the previous path has been calculated should the script consider searching for a new path */
		protected bool waitingForPathCalculation = false;

		/** Time when the last path request was started */
		protected float lastRepath = float.NegativeInfinity;

		/** Determines if the character should move or if just the desired velocities should be calculated.
		 * If false then all movement calculations will happen as usual, only the last step of actually
		 * moving the character will be left out.
		 *
		 * This is useful if you want to control the movement of the character using some other means such
		 * as for example root motion.
		 *
		 * \see #canMove which in contrast to this field will disable all movement calculations.
		 */
		[System.NonSerialized]
		public bool updatePosition = true;

		/** Determines if the character should rotate automatically.
		 * If false then all movement calculations will happen as usual, only the step of actually
		 * rotating the character will be left out.
		 */
		[System.NonSerialized]
		public bool updateRotation = true;

		[UnityEngine.Serialization.FormerlySerializedAs("target")][SerializeField][HideInInspector]
		Transform targetCompatibility;

		/** True if the Start method has been executed.
		 * Used to test if coroutines should be started in OnEnable to prevent calculating paths
		 * in the awake stage (or rather before start on frame 0).
		 */
		bool startHasRun = false;

		/** Target to move towards.
		 * The AI will try to follow/move towards this target.
		 * It can be a point on the ground where the player has clicked in an RTS for example, or it can be the player object in a zombie game.
		 *
		 * \deprecated In 4.0 this will automatically add a \link Pathfinding.AIDestinationSetter AIDestinationSetter\endlink component and set the target on that component.
		 * Try instead to use the #destination property which does not require a transform to be created as the target or use
		 * the AIDestinationSetter component directly.
		 */
		public Transform target {
			get {
				var setter = GetComponent<AIDestinationSetter>();
				return setter != null ? setter.target : null;
			}
			set {
				targetCompatibility = null;
				var setter = GetComponent<AIDestinationSetter>();
				if (setter == null) setter = gameObject.AddComponent<AIDestinationSetter>();
				setter.target = value;
				destination = target.position;
			}
		}

		/** \copydoc Pathfinding::IAstarAI::destination */
		public Vector3 destination { get; set; }

		/** \copydoc Pathfinding::IAstarAI::velocity */
		public Vector3 velocity {
			get {
				return lastDeltaTime > 0.000001f ? (prevPosition1 - prevPosition2) / lastDeltaTime : Vector3.zero;
			}
		}

		/** Velocity that this agent wants to move with.
		 * Includes gravity and local avoidance if applicable.
		 */
		public Vector3 desiredVelocity { get { return lastDeltaTime > 0.000001f ? lastDeltaPosition / lastDeltaTime : Vector3.zero; } }

		/** \copydoc Pathfinding::IAstarAI::isStopped */
		public bool isStopped { get; set; }

		public System.Action onSearchPath { get; set; }

		/** True if the path should be automatically recalculated as soon as possible */
		protected virtual bool shouldRecalculatePath {
			get {
				return Time.time - lastRepath >= repathRate && !waitingForPathCalculation && canSearch && !float.IsPositiveInfinity(destination.x);
			}
		}

		protected AIBase () {
			// Note that this needs to be set here in the constructor and not in e.g Awake
			// because it is possible that other code runs and sets the destination property
			// before the Awake method on this script runs.
			destination = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
		}

		/** Initializes reference variables.
		 * If you override this method you should in most cases call base.Awake () at the start of it.
		 */
		protected override void Awake () {
			base.Awake();
			seeker = GetComponent<Seeker>();
			controller = GetComponent<CharacterController>();
			rigid = GetComponent<Rigidbody>();
			rigid2D = GetComponent<Rigidbody2D>();
			tr = transform;
		}

		/** Starts searching for paths.
		 * If you override this method you should in most cases call base.Start () at the start of it.
		 * \see #Init
		 */
		protected virtual void Start () {
			startHasRun = true;
			Init();
		}

		/** Called when the component is enabled */
		protected virtual void OnEnable () {
			// Make sure we receive callbacks when paths are calculated
			seeker.pathCallback += OnPathComplete;
			Init();
		}

		void Init () {
			if (startHasRun) {
				// Clamp the agent to the navmesh (which is what the Teleport call will do essentially. Though only some movement scripts require this, like RichAI).
				// The Teleport call will also make sure some variables are properly initialized (like #prevPosition1 and #prevPosition2)
				Teleport(tr.position, false);
				lastRepath = float.NegativeInfinity;
				StartCoroutine(RepeatTrySearchPath());
			}
		}

		/** \copydoc Pathfinding::IAstarAI::Teleport */
		public virtual void Teleport (Vector3 newPosition, bool clearPath = true) {
			if (clearPath) CancelCurrentPathRequest();
			prevPosition1 = prevPosition2 = tr.position = newPosition;
			if (clearPath) SearchPath();
		}

		protected void CancelCurrentPathRequest () {
			waitingForPathCalculation = false;
			// Abort calculation of the current path
			if (seeker != null) seeker.CancelCurrentPathRequest();
		}

		protected virtual void OnDisable () {
			CancelCurrentPathRequest();

			// Make sure we no longer receive callbacks when paths complete
			seeker.pathCallback -= OnPathComplete;

			velocity2D = Vector3.zero;
			verticalVelocity = 0f;
			lastDeltaTime = 0;

			// Stop the #RepeatTrySearchPath coroutine
			StopAllCoroutines();
		}

		/** Called every frame.
		 * If no rigidbodies are used then all movement happens here.
		 */
		protected virtual void Update () {
			if (rigid == null && rigid2D == null && canMove) {
				MovementUpdate(Time.deltaTime, !(gravity == Vector3.zero));
			}
		}

		/** Called every physics update.
		 * If rigidbodies are used then all movement happens here.
		 */
		protected virtual void FixedUpdate () {
			if (!(rigid == null && rigid2D == null) && canMove) {
				MovementUpdate(Time.fixedDeltaTime, !(gravity == Vector3.zero) && (rigid == null || rigid.isKinematic) && (rigid2D == null || rigid2D.isKinematic));
			}
		}

		/** Calculate all movement for a single movement step and move the character.
		 * This is called automatically unless #canMove is false.
		 */
		public void MovementUpdate (float deltaTime, bool useGravity) {
			this.usingGravity = useGravity;
			lastDeltaTime = deltaTime;
			MovementUpdateInternal(deltaTime);
		}

		/** Called during either Update or FixedUpdate depending on if rigidbodies are used for movement or not */
		protected abstract void MovementUpdateInternal (float deltaTime);

		/** Tries to search for a path every #repathRate seconds.
		 * \see #SearchPath
		 */
		protected IEnumerator RepeatTrySearchPath () {
			while (true) {
				if (shouldRecalculatePath) SearchPath();
				yield return null;
			}
		}

		/** Outputs the start point and end point of the next automatic path request.
		 * This is a separate method to make it easy for subclasses to swap out the endpoints
		 * of path requests. For example the #LocalSpaceRichAI script which requires the endpoints
		 * to be transformed to graph space first.
		 */
		protected virtual void CalculatePathRequestEndpoints (out Vector3 start, out Vector3 end) {
			start = GetFeetPosition();
			end = destination;
		}

		/** \copydoc Pathfinding::IAstarAI::SearchPath */
		public virtual void SearchPath () {
			if (float.IsPositiveInfinity(destination.x)) return;
			if (onSearchPath != null) onSearchPath();

			lastRepath = Time.time;
			waitingForPathCalculation = true;

			seeker.CancelCurrentPathRequest();

			Vector3 start, end;
			CalculatePathRequestEndpoints(out start, out end);

			// Alternative way of requesting the path
			//ABPath p = ABPath.Construct(start, end, null);
			//seeker.StartPath(p);

			// This is where we should search to
			// Request a path to be calculated from our current position to the destination
			seeker.StartPath(start, end);
		}

		/** Position of the base of the character.
		 * This is used for pathfinding as the character's pivot point is sometimes placed
		 * at the center of the character instead of near the feet. In a building with multiple floors
		 * the center of a character may in some scenarios be closer to the navmesh on the floor above
		 * than to the floor below which could cause an incorrect path to be calculated.
		 * To solve this the start point of the requested paths is always at the base of the character.
		 */
		public virtual Vector3 GetFeetPosition () {
			if (controller != null && controller.enabled) {
				return tr.TransformPoint(controller.center) - Vector3.up*controller.height*0.5F;
			}

			return tr.position;
		}

		/** Called when a requested path has been calculated */
		public abstract void OnPathComplete (Path newPath);

		/** Accelerates the agent downwards.
		 * \see #verticalVelocity
		 * \see #gravity
		 */
		protected void ApplyGravity (float deltaTime) {
			// Apply gravity
			if (usingGravity) {
				float verticalGravity;
				velocity2D += movementPlane.ToPlane(deltaTime * (float.IsNaN(gravity.x) ? Physics.gravity : gravity), out verticalGravity);
				verticalVelocity += verticalGravity;
			} else {
				verticalVelocity = 0;
			}
		}

		/** Calculates how far to move during a single frame */
		protected Vector2 CalculateDeltaToMoveThisFrame (Vector2 position, float distanceToEndOfPath, float deltaTime) {
			// Direction and distance to move during this frame
			return Vector2.ClampMagnitude(velocity2D * deltaTime, distanceToEndOfPath);
		}

		/** Rotates towards the specified direction.
		 * \param direction Direction in the movement plane to rotate towards.
		 * \param maxDegrees Maximum number of degrees to rotate this frame.
		 *
		 * \see #rotationIn2D
		 * \see #movementPlane
		 */
		protected virtual void RotateTowards (Vector2 direction, float maxDegrees) {
			if (direction != Vector2.zero) {
				Quaternion targetRotation = Quaternion.LookRotation(movementPlane.ToWorld(direction, 0), movementPlane.ToWorld(Vector2.zero, 1));
				// This causes the character to only rotate around the Z axis
				if (rotationIn2D) targetRotation *= Quaternion.Euler(-90, 0, 180);
				tr.rotation = Quaternion.RotateTowards(tr.rotation, targetRotation, maxDegrees);
			}
		}

		/** Moves the agent using an offset.
		 * \param position3D Position of the agent in the world. Reading transform.position is slow, so for performance the parameter is passed in here.
		 * \param deltaPosition How much to move the agent.
		 *
		 * This will use a CharacterController, Rigidbody, Rigidbody2D or the Transform component depending on what options
		 * are available.
		 *
		 * The agent will be clamped to the navmesh after the movement (if such information is available, generally this is only done by the RichAI component).
		 *
		 * If the agent collides with the ground, then the #verticalVelocity will be set to zero.
		 *
		 * Sets #prevPosition1, #prevPosition2.
		 *
		 * \see #controller, #rigid, #rigid2D
		 * \see #ClampToNavmesh
		 * \see #RaycastPosition
		 */
		protected virtual void Move (Vector3 position3D, Vector3 deltaPosition) {
			bool positionDirty1 = false;

			if (controller != null && controller.enabled) {
				// Use CharacterController
				tr.position = position3D;
				controller.Move(deltaPosition);
				// Grab the position after the movement to be able to take physics into account
				// TODO: Add this into the clampedPosition calculation below to make RVO better respond to physics
				position3D = tr.position;
				if (controller.isGrounded) verticalVelocity = 0;
			} else {
				// Use Transform, Rigidbody or Rigidbody2D
				float lastElevation;
				movementPlane.ToPlane(position3D, out lastElevation);
				position3D += deltaPosition;

				// Position the character on the ground
				if (usingGravity) position3D = RaycastPosition(position3D, lastElevation);
				positionDirty1 = true;
			}

			// Clamp the position to the navmesh after movement is done
			bool positionDirty2 = false;
			position3D = ClampToNavmesh(position3D, out positionDirty2);

			// Assign the final position to the character if we haven't already set it (mostly for performance, setting the position can be slow)
			if (positionDirty1 || positionDirty2) {
				// Note that rigid.MovePosition may or may not move the character immediately.
				// Check the Unity documentation for the special cases.
				if (rigid != null) rigid.MovePosition(position3D);
				else if (rigid2D != null) rigid2D.MovePosition(position3D);
				else tr.position = position3D;
			}

			UpdateVelocity(position3D);
		}

		protected void UpdateVelocity (Vector3 position) {
			prevPosition2 = prevPosition1;
			prevPosition1 = position;
		}

		/** Constrains the character's position to lie on the navmesh.
		 * Not all movement scripts have support for this.
		 *
		 * \param position Current position of the character.
		 * \param positionChanged True if the character's position was modified by this method.
		 *
		 * \returns New position of the character that has been clamped to the navmesh.
		 */
		protected virtual Vector3 ClampToNavmesh (Vector3 position, out bool positionChanged) {
			positionChanged = false;
			return position;
		}

		/** Checks if the character is grounded and prevents ground penetration.
		 * \param position Position of the character in the world.
		 * \param lastElevation Elevation coordinate before the agent was moved. This is along the 'up' axis of the #movementPlane.
		 *
		 * Sets #verticalVelocity to zero if the character is grounded.
		 *
		 * \returns The new position of the character.
		 */
		protected Vector3 RaycastPosition (Vector3 position, float lastElevation) {
			RaycastHit hit;
			float elevation;

			movementPlane.ToPlane(position, out elevation);
			float rayLength = centerOffset + Mathf.Max(0, lastElevation-elevation);
			Vector3 rayOffset = movementPlane.ToWorld(Vector2.zero, rayLength);

			if (Physics.Raycast(position + rayOffset, -rayOffset, out hit, rayLength, groundMask, QueryTriggerInteraction.Ignore)) {
				// Grounded
				// Make the vertical velocity fall off exponentially. This is reasonable from a physical standpoint as characters
				// are not completely stiff and touching the ground will not immediately negate all velocity downwards. The AI will
				// stop moving completely due to the raycast penetration test but it will still *try* to move downwards. This helps
				// significantly when moving down along slopes as if the vertical velocity would be set to zero when the character
				// was grounded it would lead to a kind of 'jumping' behavior (try it, it's hard to explain). Ideally this should
				// use a more physically correct formula but this is a good approximation and is much more performant. The constant
				// '5' in the expression below determines how quickly it converges but high values can lead to too much noise.
				verticalVelocity *= System.Math.Max(0, 1 - 5 * lastDeltaTime);
				return hit.point;
			}
			return position;
		}

		protected static readonly Color GizmoColorRaycast = new Color(118.0f/255, 206.0f/255, 112.0f/255);

		protected virtual void OnDrawGizmos () {
			if (!Application.isPlaying) {
				tr = transform;
				controller = controller != null ? controller : GetComponent<CharacterController>();
				rigid = rigid != null ? rigid : GetComponent<Rigidbody>();
				rigid2D = rigid2D != null ? rigid2D : GetComponent<Rigidbody2D>();
			}

			var usingGravity = (rigid == null || rigid.isKinematic) && (rigid2D == null || rigid2D.isKinematic) && !(gravity == Vector3.zero);
			if (usingGravity && (controller == null || !controller.enabled)) {
				Gizmos.color = GizmoColorRaycast;
				Gizmos.DrawLine(transform.position, transform.position + transform.up*centerOffset);
				Gizmos.DrawLine(transform.position - transform.right*0.1f, transform.position + transform.right*0.1f);
				Gizmos.DrawLine(transform.position - transform.forward*0.1f, transform.position + transform.forward*0.1f);
			}

			if (!float.IsPositiveInfinity(destination.x) && Application.isPlaying) Draw.Gizmos.CircleXZ(destination, 0.2f, Color.blue);
		}

		protected override int OnUpgradeSerializedData (int version, bool unityThread) {
			if (unityThread && targetCompatibility != null) target = targetCompatibility;
			return 1;
		}
	}
}
