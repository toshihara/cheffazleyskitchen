using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using System.Collections.Generic;

namespace Pathfinding {
	/** Moves the target in example scenes.
	 * This is a simple script which has the sole purpose
	 * of moving the target point of agents in the example
	 * scenes for the A* Pathfinding Project.
	 *
	 * It is not meant to be pretty, but it does the job.
	 */
	[HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_target_mover.php")]
	public class TargetMover : MonoBehaviour {
		/** Mask for the raycast placement */
		public LayerMask mask;

		public Transform target;
		IAstarAI[] ais;

		public List<Vector3> targetPositionList;
		/** Determines if the target position should be updated every frame or only on double-click */
		public bool onlyOnDoubleClick;
		public bool use2D;

		Camera cam;

		public void Start () {
			//Cache the Main Camera
			cam = Camera.main;
			// Slightly inefficient way of finding all AIs, but this is just an example script, so it doesn't matter much.
			// FindObjectsOfType does not support interfaces unfortunately.
			ais = FindObjectsOfType<MonoBehaviour>().OfType<IAstarAI>().ToArray();
			Debug.Log(ais.Length);
			useGUILayout = false;
		}

		public void OnGUI () {
//			if (onlyOnDoubleClick && cam != null && Event.current.type == EventType.MouseDown) // && Event.current.clickCount == 2)
//			{
////				Vector2 mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
////				if (!IsPinterOverUIObject(mousePosition))
////					UpdateTargetPosition();
//			}
		}

		/** Update is called once per frame */
		void Update () {
//			if (!onlyOnDoubleClick && cam != null) {
//				UpdateTargetPosition();
//			}
		}

		public void UpdateTargetPosition () {
			Vector3 newPosition = Vector3.zero;
			bool positionFound = false;

			if (use2D) {
				newPosition = cam.ScreenToWorldPoint(Input.mousePosition);
				newPosition.z = 0;
				positionFound = true;
				Debug.Log ("Update Target Position");
			} else {
				// Fire a ray through the scene at the mouse position and place the target where it hits
				RaycastHit hit;
				if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, mask)) {
					newPosition = hit.point;
					newPosition.z = 0;
					positionFound = true;
				}
			}

			if (positionFound && newPosition != target.position) {
				target.position = newPosition;

				if (onlyOnDoubleClick) {
					for (int i = 0; i < ais.Length; i++) {
						if (ais[i] != null) ais[i].SearchPath();
					}
				}
			}
		}

		private bool IsPinterOverUIObject(Vector2 touchPosition) {
			PointerEventData eventDataCurrentPosition = new PointerEventData (EventSystem.current);
			eventDataCurrentPosition.position = touchPosition;
			List<RaycastResult> results = new List<RaycastResult> ();
			EventSystem.current.RaycastAll (eventDataCurrentPosition, results);
			return results.Count > 0;
		}

		public void InitTargetList() {
			targetPositionList.Clear ();
		}

		public void SetTargetPosition(Vector3 targetPos) {
			Vector3 newPosition = Vector3.zero;
			bool positionFound = false;

			newPosition = targetPos;
			newPosition.z = 0;
			positionFound = true;

			if (positionFound) {
				target.position = newPosition;
				Debug.Log ("Set Target Position");
				if (onlyOnDoubleClick) {
					for (int i = 0; i < ais.Length; i++) {
						if (ais[i] != null) ais[i].SearchPath();
					}
				}
			}
		}

		public void PushTargetPosition2List(Vector3 targetPos) {
			if (targetPositionList.Count == 0)
				SetTargetPosition (targetPos);
			
			targetPositionList.Add (targetPos);
		}

		public void PopTargetPositionFromList() {
			if (targetPositionList.Count > 0) {
				targetPositionList.RemoveAt (0);
				if (targetPositionList.Count > 0) {
					SetTargetPosition (targetPositionList [0]);
					CharacterAniController.Instance ().bReachDestination = false;
				}

			}
				
		}
	}
}
