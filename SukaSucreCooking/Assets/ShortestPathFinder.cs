﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortestPathFinder : MonoBehaviour {
	public int startNodeId = 0;
	public int endNodeId = 14;
	public List<Node> arrayNodes = new List<Node>();
	public List<Graph> listGraph = new List<Graph>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void InitNodes() {
		Node node0 = new Node (0,1);
		Node node1 = new Node (1,1);
		Node node2 = new Node (2,1);
		Node node3 = new Node (3,1);
		Node node4 = new Node (4,1);
		Node node5 = new Node (5,1);
		Node node6 = new Node (6,1);
		Node node7 = new Node (7,1);
		Node node8 = new Node (8,1);
		Node node9 = new Node (9,1);
		Node node10 = new Node (10,1);
		Node node11 = new Node (11,1);
		Node node12 = new Node (12,1);
		Node node13 = new Node (13,1);
		Node node14 = new Node (14,1);

		node0.connectedNodeIds.Add (1);
		node0.connectedNodeIds.Add (5);

		node1.connectedNodeIds.Add (0);
		node1.connectedNodeIds.Add (2);
		node1.connectedNodeIds.Add (3);

		node2.connectedNodeIds.Add (2);
		node2.connectedNodeIds.Add (5);
		node2.connectedNodeIds.Add (8);

		node3.connectedNodeIds.Add (1);
		node3.connectedNodeIds.Add (6);

		node4.connectedNodeIds.Add (2);
		node4.connectedNodeIds.Add (5);
		node4.connectedNodeIds.Add (8);

		node5.connectedNodeIds.Add (0);
		node5.connectedNodeIds.Add (4);

		node6.connectedNodeIds.Add (3);

		node7.connectedNodeIds.Add (2);
		node7.connectedNodeIds.Add (8);
		node7.connectedNodeIds.Add (13);

		node8.connectedNodeIds.Add (4);
		node8.connectedNodeIds.Add (7);
		node8.connectedNodeIds.Add (9);
		node8.connectedNodeIds.Add (11);

		node9.connectedNodeIds.Add (8);

		node10.connectedNodeIds.Add (11);

		node11.connectedNodeIds.Add (10);
		node11.connectedNodeIds.Add (8);
		node11.connectedNodeIds.Add (13);

		node12.connectedNodeIds.Add (13);

		node13.connectedNodeIds.Add (11);
		node13.connectedNodeIds.Add (12);
		node13.connectedNodeIds.Add (14);

		node14.connectedNodeIds.Add (13);

		arrayNodes.Add (node0);
		arrayNodes.Add (node1);
		arrayNodes.Add (node2);
		arrayNodes.Add (node3);
		arrayNodes.Add (node4);
		arrayNodes.Add (node5);
		arrayNodes.Add (node6);
		arrayNodes.Add (node7);
		arrayNodes.Add (node8);
		arrayNodes.Add (node9);
		arrayNodes.Add (node10);
		arrayNodes.Add (node11);
		arrayNodes.Add (node12);
		arrayNodes.Add (node13);
		arrayNodes.Add (node14);

	}

	void GenerateGraph(int graphIndex, List<int> graphElement) {

	}

	void AddNewNode2Graph(int graphIndex, int graphId) {

	}

	void CalculateShortestPath(int nStartIndex, int nEndIndex) {
		for (int i = 0; i < arrayNodes[nStartIndex].connectedNodeIds.Count; i++) {
			//GenerateGraph
		}
	}
}

public struct Node {
	public int nodeId;
	public int nodeWeight;
	public List<int> connectedNodeIds;
	public Node(int id, int weight) {
		nodeId = id;
		nodeWeight = weight;
		connectedNodeIds = new List<int> ();
	}
}

public struct Graph {
	public int graphId;
	//public int graphWeight;
	public List<int> graphNode;
	public Graph (int id) {
		graphId = id;
		graphNode = new List<int> ();
	}
}