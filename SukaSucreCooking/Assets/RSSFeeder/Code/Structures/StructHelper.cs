﻿using System;

/// <summary>
/// Holds all potential items we will need for RSS content.
/// </summary>
public struct RSSContentItem {
	public string Title;
	public string Description;
	public string Link;
	public string Image;
	public string Video;
	public string ButtonLabel;
}
