﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Ricimi {
	public class PopupWin : Popup {
		public Text txt_score;
		public Text txt_round;
		public Image[] objStars;
		public Sprite[] goldStars;
		public Sprite[] greyStars;
		public Button btnReplay;
		public Button btnNext;
		public GameObject btnFbShare;

		public void InitWinPopup(int nScore, int nRound, int nStar) {
			GoogleMobileAdsDemoScript.Instance ().RequestInterstitial ();
			Open ();
			txt_score.text = "" + nScore;
			//txt_round.text = "Round " + nRound;
			GameFlags.nStars = nStar;
			for (int i = 0; i < 3; i++) {
				if (i < nStar)
					objStars [i].sprite = goldStars[i];
				else 
					objStars [i].sprite = greyStars[i];
			}

			if (PlayerPrefs.GetInt ("CurrentRound") + 1 > PlayerPrefs.GetInt ("AvailRound")) {
				PlayerPrefs.SetInt ("AvailRound", PlayerPrefs.GetInt ("CurrentRound") + 1);
				if (PlayerPrefs.HasKey("encPass"))
					ApiHandler.Instance ().SendStageAchievement (1, PlayerPrefs.GetInt("AvailRound"));
			}
//			if (PlayerPrefs.HasKey("encPass"))
//				ApiHandler.Instance ().SendStageAchievement (1, PlayerPrefs.GetInt("AvailRound"));
			string strDiaryCount = "Round_Dialy_Play_1_" + PlayerPrefs.GetInt ("CurrentRound");
			if (PlayerPrefs.GetInt (strDiaryCount, 0) >= GameFlags.MAX_PLAYING_COUNT)
				btnReplay.gameObject.SetActive (false);
//			if (PlayerPrefs.GetInt ("CurrentRound") == 100)
//				btnNext.gameObject.SetActive (false);
			
			string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
			if (currentDate == PlayerPrefs.GetString ("FbShare", "")) {
				btnFbShare.SetActive (false);
			}
		}

		public void RestartGame() {
			if (PlayerPrefs.GetInt("Ads") == 0)
				GoogleMobileAdsDemoScript.Instance ().ShowInterstitial ();
			RoundManager.Instance ().RestartGame ();
		}

		public void Go2LevelScene() {
			if (PlayerPrefs.GetInt("Ads") == 0)
				GoogleMobileAdsDemoScript.Instance ().ShowInterstitial ();
			RoundManager.Instance ().Go2Main ();
		}

		public void PlayNextRound() {
			if (PlayerPrefs.GetInt("Ads") == 0)
				GoogleMobileAdsDemoScript.Instance ().ShowInterstitial ();
			if (PlayerPrefs.GetInt ("CurrentRound") < 100) {
				PlayerPrefs.SetInt ("CurrentRound", PlayerPrefs.GetInt ("CurrentRound") + 1);
				RoundManager.Instance ().PlayNextRound ();
			} else {
				GameManager.Instance ().LoadCreditsScene ();
			}
		}

		public void DisableShareButton() {
			int nCoin = PlayerPrefs.GetInt ("Coin");
			PlayerPrefs.SetInt ("Coin", nCoin + 100);
	
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip ((long)(nCoin + 100));
				ApiHandler.Instance ().SendGameLog ((long)(100), 0, 0);
			}
			string currentDate = System.DateTime.Now.ToString ("yyyy/MM/dd");
			PlayerPrefs.SetString ("FbShare", currentDate);
			btnFbShare.SetActive (false);
		}

	}
}
