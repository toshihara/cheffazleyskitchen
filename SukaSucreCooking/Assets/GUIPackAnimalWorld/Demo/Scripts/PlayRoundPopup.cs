﻿using UnityEngine;
using UnityEngine.UI;

namespace Ricimi
{
	public class PlayRoundPopup : PlayPopup
	{
		public int nRoundId;
		public Text txt_round_label;
		public Text txt_score;
		public Text txt_time;
		public Text txt_bonus;
		public GameObject[] goldStars;
		public GameObject[] grayStars;
		public Text txt_star_1;
		public Text txt_star_2;
		public Text txt_star_3;

		public void InitRound(int nRound, int nStarObtained) {
			this.nRoundId = nRound;
			txt_round_label.text = "" + nRoundId;

			for (int i = 0; i < 3; i++) {
				if (i < nStarObtained) {
					goldStars [i].SetActive (true);
					grayStars [i].SetActive (false);
				} else {
					goldStars [i].SetActive (false);
					grayStars [i].SetActive (true);
				}
			}
//			txt_score.text = "" + GameFlags.roundInfoList [nRound - 1].nTargetScore;
//			txt_bonus.text = "" + GameFlags.roundInfoList [nRound - 1].nBonusPoint;
//			int nTime = (int)GameFlags.roundInfoList [nRound - 1].fTimeDuration;
//			int nMinute = nTime / 60;
//			int nSec = nTime % 60;
//			txt_time.text = "" + nMinute + ":" + nSec;
			int nStar0 = GameFlags.roundInfoList[nRound - 1].nTargetScore;
			int nStar1 = GameFlags.roundInfoList[nRound - 1].nBonusPoint + nStar0;
			int nStar2 = GameFlags.roundInfoList [nRound - 1].nBonusPoint * 2 + nStar0;
			txt_star_1.text = nStar0 + " COINS = 1 STAR"; 
			txt_star_2.text = nStar1 + " COINS = 2 STARS"; 
			txt_star_3.text = nStar2 + " COINS = 3 STARS"; 
		}

		public  void OnClickPlayButton() {
			PlayerPrefs.SetInt ("CurrentRound", nRoundId);
			GameFlags.nCurrentRound = nRoundId;
			GameManager.Instance ().PlayGame ();

		}

		public void OnClickPlayNextStage() {
			Close ();
			PlayerPrefs.SetInt ("CurrentRound", nRoundId);
			GameFlags.nCurrentRound = nRoundId;
			RoundManager.Instance ().ResumeNewRound ();
		}

	}
		
}