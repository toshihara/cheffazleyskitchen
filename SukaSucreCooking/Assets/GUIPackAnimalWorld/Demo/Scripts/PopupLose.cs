﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Ricimi {
	public class PopupLose : Popup {
		public Text txt_score;

		public void SetScore(int nScore) {
			GoogleMobileAdsDemoScript.Instance ().RequestInterstitial ();
			txt_score.text = "" + nScore;
			Open ();
		}

		public void OnClickMenu() {
			if (PlayerPrefs.GetInt("Ads") == 0)
				GoogleMobileAdsDemoScript.Instance ().ShowInterstitial ();
			RoundManager.Instance ().Go2Main ();
		}

		public void OnClickRetry() {
			if (PlayerPrefs.GetInt("Ads") == 0)
				GoogleMobileAdsDemoScript.Instance ().ShowInterstitial ();
			RoundManager.Instance ().RestartGame ();
		}
	}
}
