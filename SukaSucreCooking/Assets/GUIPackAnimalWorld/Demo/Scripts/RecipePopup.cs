﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ricimi;
public class RecipePopup : Popup {

	public void OnClose() {
		RoundManager.Instance ().ResumeNewRound ();
		Close ();
	}
}
