﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ricimi;
public class PopupSignError: Popup {
	
	public Text txt_error;

	public void ShowPopUp(int nErrorType) {
		switch (nErrorType) {
		case 0:
			txt_error.text = "User name should not be empty.";
			break;
		case 1:
			txt_error.text = "User Name already exist, please choose other name.";
			break;
		case 2:
			txt_error.text = "Email should not be empty.";
			break;
		case 3:
			txt_error.text = "Email already exist, please choose other email.";
			break;
		case 4:
			txt_error.text = "Password should not be empty.";
			break;
		case 5:
			txt_error.text = "Password length should be longer than 5.";
			break;
		case 6:
			txt_error.text = "Server seems to be down, please try a little later.";
			break;
		case 7:
			txt_error.text = "Invalid Email format, Please retype the email.";
			break;
		case 8:
			txt_error.text = "Invalid login information! Please retype email and password.";
			break;
		case 9:
			txt_error.text = "Your purchases and upgrades have been restored.";
			break;
		case 10:
			txt_error.text = "Restore completed.";
			break;
		}
		this.Open ();
	}
		
}
