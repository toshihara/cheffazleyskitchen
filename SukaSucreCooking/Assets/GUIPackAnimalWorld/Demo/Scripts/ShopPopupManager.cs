﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ricimi;
public class ShopPopupManager : Popup {
	public Text txt_coin;

	public GameObject item_oven;
	public Text txt_buy_oven;
	public Text txt_stick_oven;
	public GameObject btn_oven;
	public GameObject mask_oven;

	public GameObject item_stove;
	public Text txt_buy_stove;
	public Text txt_stick_stove;
	public GameObject btn_stove;
	public GameObject mask_stove;

	public GameObject item_warm;
	public Text txt_buy_warm;
	public Text txt_stick_warm;
	public GameObject btn_warm;
	public GameObject mask_warm;

	public GameObject item_coffee1;
	public Text txt_buy_coffee1;
	public Text txt_stick_coffee1;
	//public Button btn_coffee1;

	public GameObject item_coffee2;
	public Text txt_buy_coffee2;
	public Text txt_stick_coffee2;
	//public Button btn_coffee2;

	public GameObject item_cold1;
	public Text txt_buy_cold1;
	public Text txt_stick_cold1;


	public GameObject item_cold2;
	public Text txt_buy_cold2;
	public Text txt_stick_col2;

	public GameObject item_speedUp;
	public GameObject btn_speedup;
	public GameObject mask_speedup;
	public GameObject prefPopupConfirm;

	public Transform mCanvas;
	int nOven;
	int nStove;
	int nWarmStation;
	int nColdMachine;
	int nCoffeeMachine;

	public static ShopPopupManager instance;

	public static ShopPopupManager Instance() {
		if (!instance) {
			instance = FindObjectOfType (typeof(ShopPopupManager)) as ShopPopupManager;
		}
		return instance;
	}

	public void OnPressPurchaseButton(int nId) {
		GameObject popupConfirm = Instantiate (prefPopupConfirm) as GameObject;
		popupConfirm.transform.parent = mCanvas;
		popupConfirm.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (15f, -238f);
		popupConfirm.GetComponent<Popup> ().Open ();

		switch (nId) {
		case 0:
			nOven = PlayerPrefs.HasKey ("Oven") ? PlayerPrefs.GetInt ("Oven") : 2;
			int nRequiredCoin = (nOven == 2) ? 4000 : 10000;
			if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (0);
			} else {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (1);
			}
			break;
		case 1:
			nStove = PlayerPrefs.HasKey ("Stove") ? PlayerPrefs.GetInt ("Stove") : 2;
			nRequiredCoin = (nStove == 2) ? 4000 : 10000;
			if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (2);
			} else {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (3);
			}
			break;
		case 2:
			nRequiredCoin = (nWarmStation == 2) ? 4000 : 10000;
			if (nWarmStation < 2)
				nWarmStation = 2;
			if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (4);
			} else {
				popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (5);
			}
			break;
		case 3:
			popupConfirm.GetComponent<UpgradePopupController> ().InitUpgradePopup (6);
			break;

		}
	}

	public void InitShopSetting() {
		mCanvas = GameObject.Find ("Canvas").transform;
		Open ();
		if (!PlayerPrefs.HasKey ("Coin"))
			PlayerPrefs.SetInt ("Coin", 0);

		int nOven = PlayerPrefs.HasKey ("Oven") ? PlayerPrefs.GetInt ("Oven") : 2;
		int nStove = PlayerPrefs.HasKey ("Stove") ? PlayerPrefs.GetInt ("Stove") : 2;
		int nWarmStation = PlayerPrefs.HasKey ("WarmStation") ? PlayerPrefs.GetInt ("WarmStation") : 2;
		if (nWarmStation < 2)
			nWarmStation = 2;
		int nColdMachine = PlayerPrefs.HasKey ("ColdMachine") ? PlayerPrefs.GetInt ("ColdMachine") : 1;
		int nCoffeeMachine = PlayerPrefs.HasKey ("CoffeeMachine") ? PlayerPrefs.GetInt ("CoffeeMachine") : 1;

		// check if the user purchase the speedup item with real cash -----
		if (!PlayerPrefs.HasKey ("SpeedUp") || PlayerPrefs.GetInt("SpeedUp", 0) != 1) {
			mask_speedup.SetActive(false);
			btn_speedup.SetActive(true);
		} else {
			//item_speedUp.SetActive (false);
			btn_speedup.SetActive(false);
			mask_speedup.SetActive (true);
		}
		//-----------------------------------------------------------------

		txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");

		if (nOven == 4) {
			btn_oven.SetActive (false);
			txt_stick_oven.text = "4/4";
			mask_oven.SetActive (true);
		} else {
			item_oven.SetActive (true);
			txt_stick_oven.text = "" + nOven + "/4";
			if (nOven == 2) {
				txt_buy_oven.text = "4,000";
			} else if (nOven == 3) {
				txt_buy_oven.text = "10,000";
			}
			mask_oven.SetActive (false);
		}

		if (nStove == 4) {
			//item_stove.SetActive (false);
			btn_stove.SetActive(false);
			txt_stick_stove.text = "4/4";
			mask_stove.SetActive (true);
		} else {
			item_stove.SetActive (true);
			txt_stick_stove.text = "" + nStove + "/4";
			if (nStove == 2) {
				txt_buy_stove.text = "4,000";
			} else if (nStove == 3) {
				txt_buy_stove.text = "10,000";
			}
			mask_stove.SetActive (false);
		}

		if (nWarmStation == 4) {
			btn_warm.SetActive(false);
			txt_stick_warm.text = "" + nWarmStation + "/4";
			mask_warm.SetActive (true);
		} else {
			item_warm.SetActive (true);
			txt_stick_warm.text = "" + nWarmStation + "/4";
			if (nWarmStation == 2) {
				txt_buy_warm.text = "4,000";
			} else if (nWarmStation == 3) {
				txt_buy_warm.text = "10,000";
			}
			mask_warm.SetActive (false);
//			else if (nWarmStation == 2) {
//				txt_buy_warm.text = "900";
//			}
//			else if (nWarmStation == 3) {
//				txt_buy_warm.text = "1200";
//			}
		}

		if (PlayerPrefs.GetInt ("SpeedUp", 0) == 1) {
			btn_speedup.SetActive (false);
			mask_speedup.SetActive (true);
		} else {
			mask_speedup.SetActive (false);
		}

		item_cold2.SetActive (false);
		item_cold1.SetActive (false);
		item_coffee1.SetActive (false);
		item_coffee2.SetActive (false);

//		if (nColdMachine == 3) {
//			item_cold2.SetActive (false);
//			item_cold1.SetActive (false);
//		} else {
//			if (nColdMachine == 1) {
//				txt_buy_cold1.text = "4,000";
//				item_cold2.SetActive (false);
//				item_cold1.SetActive (true);
//			} else if (nColdMachine == 2) {
//				txt_buy_cold2.text = "10,000";
//				item_cold2.SetActive (true);
//				item_cold1.SetActive (false);
//			}
//		}
			
//		if (nCoffeeMachine == 3) {
//			item_coffee1.SetActive (false);
//			item_coffee2.SetActive (false);
//		} else {
//			if (nCoffeeMachine == 1) {
//				txt_buy_coffee1.text = "4,000";
//				item_coffee2.SetActive (false);
//				item_coffee1.SetActive (true);
//			} else if (nCoffeeMachine == 2) {
//				txt_buy_coffee2.text = "10,000";
//				item_coffee2.SetActive (true);
//				item_coffee1.SetActive (false);
//			}
//		}
	}

	public void OnPressOvenBtn() {
		nOven =  PlayerPrefs.HasKey ("Oven") ? PlayerPrefs.GetInt ("Oven") : 2;
		int nRequiredCoin = (nOven == 2) ? 4000 : 10000;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nOven++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
				ApiHandler.Instance ().LogPurchaseResult ("oven", nOven - 2);
				LevelSceneManager.Instance ().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			}
			PlayerPrefs.SetInt ("Oven", nOven);
			if (nOven == 4) {
				//item_oven.SetActive (false);
				btn_oven.SetActive(false);
				mask_oven.SetActive (true);
				txt_stick_oven.text = "4/4";
			} else {
				item_oven.SetActive (true);
				txt_stick_oven.text = "" + nOven + "/4";
				if (nOven == 2) {
					txt_buy_oven.text = "4,000";
				} else if (nOven == 3) {
					txt_buy_oven.text = "10,000";
				}
			}
			LevelSceneManager.Instance ().DisplayCoin ();
		}
			
	}

	public void OnPressStoveBtn() {
		nStove = PlayerPrefs.HasKey ("Stove") ? PlayerPrefs.GetInt ("Stove") : 2;
		int nRequiredCoin = (nStove == 2) ? 4000 : 10000;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nStove++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
				ApiHandler.Instance ().LogPurchaseResult ("stove", nStove - 2);
			}
			PlayerPrefs.SetInt ("Stove", nStove);
			if (nStove == 4) {
				//item_stove.SetActive (false);
				btn_stove.SetActive(false);
				txt_stick_stove.text = "4/4";
				mask_stove.SetActive (true);
			} else {
				item_stove.SetActive (true);
				txt_stick_stove.text = "" + nStove + "/4";
				if (nStove == 2) {
					txt_buy_stove.text = "4,000";
				} else if (nStove == 3) {
					txt_buy_stove.text = "10,000";
				}
			}

			LevelSceneManager.Instance ().DisplayCoin ();
		}
	}

	public void OnPressWarmStation() {
		if (nWarmStation < 2)
			nWarmStation = 2;
		int nRequiredCoin = (nWarmStation == 2) ? 4000 : 10000;

		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nWarmStation++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			PlayerPrefs.SetInt ("WarmStation", nWarmStation);
			txt_coin.text = "" +PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
				ApiHandler.Instance ().LogPurchaseResult ("warmstation", nWarmStation - 2);
			}
			if (nWarmStation == 4) {
				//item_warm.SetActive (false);
				btn_warm.SetActive(false);
				txt_stick_warm.text = "4/4";
				mask_warm.SetActive (true);
			} else {
				item_warm.SetActive (true);
				txt_stick_warm.text = "" + nWarmStation + "/4";
				if (nWarmStation == 0) {
					txt_buy_warm.text = "300";
				} else if (nWarmStation == 1) {
					txt_buy_warm.text = "600";
				}
				else if (nWarmStation == 2) {
					txt_buy_warm.text = "4,000";
				}
				else if (nWarmStation == 3) {
					txt_buy_warm.text = "10,000";
				}
			}

			LevelSceneManager.Instance ().DisplayCoin ();
		}
	}

	public void OnPressColdMachine1() {
		int nRequiredCoin = 4000;
		nColdMachine = PlayerPrefs.HasKey ("ColdMachine") ? PlayerPrefs.GetInt ("ColdMachine") : 1;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nColdMachine++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");

			PlayerPrefs.SetInt ("ColdMachine", nColdMachine);
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
			}
			txt_buy_cold2.text = "10,000";
			item_cold2.SetActive (true);
			item_cold1.SetActive (false);

		}
	}

	public void OnPressColdMachine2() {
		int nRequiredCoin = 10000;
		nColdMachine = PlayerPrefs.HasKey ("ColdMachine") ? PlayerPrefs.GetInt ("ColdMachine") : 1;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nColdMachine++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			PlayerPrefs.SetInt ("ColdMachine", nColdMachine);
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
			}
			//txt_buy_cold2.text = "600";
			item_cold2.SetActive (false);
			item_cold1.SetActive (false);

		}
	}

	public void OnPressCoffeeMachine1() {
		int nRequiredCoin = 4000;
		nCoffeeMachine = PlayerPrefs.HasKey ("CoffeeMachine") ? PlayerPrefs.GetInt ("CoffeeMachine") : 1;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nCoffeeMachine++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			PlayerPrefs.SetInt ("CoffeeMachine", nCoffeeMachine);
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
			}
			txt_buy_coffee2.text = "10,000";
			item_coffee2.SetActive (true);
			item_coffee1.SetActive (false);

		}
	}

	public void OnPressCoffeeMachine2() {
		int nRequiredCoin = 10000;
		nCoffeeMachine = PlayerPrefs.HasKey ("CoffeeMachine") ? PlayerPrefs.GetInt ("CoffeeMachine") : 1;
		if (nRequiredCoin <= PlayerPrefs.GetInt ("Coin")) {
			nCoffeeMachine++;
			PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt ("Coin") - nRequiredCoin);
			txt_coin.text = "" + PlayerPrefs.GetInt ("Coin");
			LevelSceneManager.Instance().totalCoin.text = "" + PlayerPrefs.GetInt ("Coin");
			PlayerPrefs.SetInt ("CoffeeMachine", nCoffeeMachine);
			txt_buy_coffee2.text = "10,000";
			long lChip = (long)PlayerPrefs.GetInt ("Coin");
			if (PlayerPrefs.HasKey ("encPass")) {
				ApiHandler.Instance ().UpdateChip (lChip);
				ApiHandler.Instance ().SendGameLog (-(long)nRequiredCoin, 0, 0);
			}
			item_coffee2.SetActive (false);
			item_coffee1.SetActive (false);

		}
	}

	public void OnPressChefSpeedUp() {
		//----- should worked by IAP controller 
//		PlayerPrefs.SetInt ("SpeedUp", 1);
//		item_speedUp.SetActive (false);
		//PurchaseManager.Instance().BuySuperChips();
		CoinPurchaseScript.Instance().BuySpeedBooster();
	}

	public void HidePurchaseButton() {
		btn_speedup.SetActive (false);
		mask_speedup.SetActive (true);
	}
}
