﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Ricimi {
	public class PopupPause : Popup {
		
		public GameObject[] imgDisables;
		public Text txt_stage;
		public void SetGamePaused() {
			GameFlags.bPauseGame = true;
			CheckSettings ();
			txt_stage.text = "Stage " + PlayerPrefs.GetInt ("CurrentRound");
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -238f);
			Open ();
		}

		public void ResumeGame() {
			GameFlags.bPauseGame = false;
			Close ();
		}

		public void RestartGame() {
			RoundManager.Instance ().RestartGame ();
		}

		public void Go2LevelScene() {
			RoundManager.Instance ().Go2Main ();
		}

		void Start () {
			CheckSettings ();
		}

		void CheckSettings() {
			if (!PlayerPrefs.HasKey ("SoundOff")) {
				imgDisables [0].SetActive (false);
			} else {
				if (PlayerPrefs.GetInt("SoundOff", 0) == 1)
					imgDisables [0].SetActive (true);
				else
					imgDisables [0].SetActive (false);
			}

			if (!PlayerPrefs.HasKey ("MusicOff")) {
				imgDisables [1].SetActive (false);
			} else {
				if (PlayerPrefs.GetInt ("MusicOff", 0) == 1)
					imgDisables [1].SetActive (true);
				else 
					imgDisables [1].SetActive (false);
			}

			BGSoundManager.Instance ().CheckMuteSounds ();
				
		}

		public void OnClickSoundButton () {
			bool bDisabled = false;

			if (PlayerPrefs.GetInt ("SoundOff", 0) == 1) {
				PlayerPrefs.SetInt ("SoundOff", 0);
				imgDisables [0].SetActive (false);
			} else {
				PlayerPrefs.SetInt ("SoundOff", 1);
				imgDisables [0].SetActive (true);
			}
		}

		public void OnClickMusicButton () {

			if (PlayerPrefs.GetInt ("MusicOff") == 1) {
				PlayerPrefs.SetInt ("MusicOff", 0);
				imgDisables [1].SetActive (false);
			} else {
				PlayerPrefs.SetInt ("MusicOff", 1);
				imgDisables [1].SetActive (true);
			}

			BGSoundManager.Instance ().CheckMuteSounds ();

		}

	}
}
