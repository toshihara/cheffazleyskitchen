﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ricimi
{
	public class ShopPopupOpener : PopupOpener {
		public void OpenShopPopup() {
			var popup = Instantiate(popupPrefab) as GameObject;
			popup.SetActive(true);
			popup.transform.localScale = Vector3.zero;

			if (!m_canvas)
				m_canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
			popup.transform.SetParent(m_canvas.transform, false);
			popup.GetComponent<ShopPopupManager>().InitShopSetting();
		}
	}
}
