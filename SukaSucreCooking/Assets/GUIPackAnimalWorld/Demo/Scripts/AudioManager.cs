﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	public List<AudioSource> audioSources;

	// Use this for initialization
	void Start () {
		CheckMuteSounds ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void CheckMuteSounds() {
		bool bMute = false;
		if (!PlayerPrefs.HasKey ("SoundOff"))
			PlayerPrefs.SetInt ("SoundOff", 0);
		if (PlayerPrefs.GetInt ("SoundOff", 0) == 1) {
			bMute = true;
		}

		for (int i = 0; i < audioSources.Count; i++) {
			audioSources [i].mute = bMute;
		}
	}

	public void PlaySound(int nAudioIndex) {
		if (nAudioIndex < audioSources.Count) {
			audioSources [nAudioIndex].mute = false;
			if (PlayerPrefs.HasKey ("SoundOff")) {
				if (PlayerPrefs.GetInt ("SoundOff", 0) == 1) {
					audioSources [nAudioIndex].mute = true;
				}
			}
			if (PlayerPrefs.GetInt ("SoundOff", 0) == 0)
				audioSources [nAudioIndex].Play ();
		}
	}

	public void ResumeGame() {
		GameFlags.bPauseGame = false;
	}
}
