﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Ricimi {
	public class PopupRoundInfo : PlayPopup {
		public Text txt_round_label;
		public Text txt_score;
		public Text txt_time;
		public Text txt_bonus;

		public void InitRound(int nRound) {
			txt_round_label.text = "Stage " + GameFlags.nCurrentRound;
			txt_score.text = "" + GameFlags.roundInfoList [nRound - 1].nTargetScore;
			txt_bonus.text = "" + GameFlags.roundInfoList [nRound - 1].nBonusPoint;
			int nTime = (int)GameFlags.roundInfoList [nRound - 1].fTimeDuration;
			int nMinute = nTime / 60;
			int nSec = nTime % 60;
			txt_time.text = "" + nMinute + ":" + nSec;
		}

		public  void OnClickPlayButton() {
			Close ();
			RoundManager.Instance ().ResumeNewRound ();

		}
	}
}
